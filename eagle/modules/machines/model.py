from sqlalchemy import Column, String, DateTime, Boolean
from flask_appbuilder.models.mixins import BaseMixin
from flask_appbuilder.models.sqla import Model
from sqlalchemy_utils import generic_repr
import datetime, json
from dateutil import parser
from blinker import signal
import flask
from eagle.models import insert_db, db

@generic_repr
class Machine(BaseMixin,  Model):

    __bind_key__ = 'machines'
    id = Column(String, primary_key=True)
    name = Column(String(50), nullable=False)
    address = Column(String(50), nullable=False)
    properties = Column(String,nullable=True)
    created_at = Column(DateTime(timezone=True), default=datetime.datetime.utcnow)
    updated_at = Column(DateTime(timezone=True), onupdate=datetime.datetime.utcnow)
    available = Column(Boolean, nullable=False, default=True)

    @property
    def role(self):
        if self.properties:
            return json.loads(self.properties)['role']
        return 'NONE'

    @property
    def hostname(self):
        if self.properties:
            return json.loads(self.properties)['hostname']
        return 'NONE'

    def __repr__(self):
        return f"Machine(id='{self.id}',name='{self.name}',role='{self.role}',address='{self.address}')"

    def __html__(self):
        return f"{self.name} ({self.role})"
    
    @classmethod
    def from_dict(cls,machine):
        machine_dict = machine
        for key in ['created_at','updated_at']:
            if machine_dict.get(key):
                machine_dict[key] = parser.parse(machine_dict[key])
        machine = cls(**machine_dict)
        return machine

machine_added = signal('machine-added',doc='A Machine was added to the database')
machine_removed = signal('machine-removed',doc='A Machine was removed from the database')

def add_machine(app,**service):
    name = service['server'].rstrip('.')
    type_ = service['type_']
    id = service['name'].split('.')[0]
    addresses = service['addresses']
    properties = json.dumps(service['properties'])
    address = addresses[0]
    address =  address+':'+ str(service['port'])
    m = db.session.query(Machine).filter(Machine.id==id).first() 
    if not m:
        m = Machine(id=id,name=name,address=address,properties=properties,available=True)
        if m.hostname:
            m.name = m.hostname
    else:
        m.properties = properties
        m.available = True
        m.address = address

    insert_db(m)
    machine_added.send(app,machine=m.to_json())

def remove_machine(app,**service):
    id = service['name'].split('.')[0]
    m = db.session.query(Machine).filter(Machine.id==id).first()
    if not m:
        return
    #m.available=False
    #insert_db(m)
    machine_removed.send(app,machine=m.to_json())

def init_app(app:flask.Flask):
    
    app.context_processor(lambda: dict(Machine=Machine))