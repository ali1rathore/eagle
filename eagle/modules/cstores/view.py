import flask
from .model import CStore
from flask_appbuilder.models.sqla.interface import SQLAInterface
from eagle.extensions import litestream
from eagle.extensions.appbuilder.baseviews import utc_to_local, ModelView
from wtforms import SelectField, HiddenField
from flask_babel import gettext
from flask_appbuilder import action, expose
from wtforms.validators import DataRequired
import json
def google_map_link(addr):
    link = f"<a href='https://www.google.com/maps/place/{addr}'>{addr}</a>"
    return flask.Markup(link)


def link_to_pos(host_id):
    link = ""
    if host_id:
        url = flask.url_for("ReplicaView.set", name=host_id) + flask.url_for(
            "ReportsView.summary"
        )
        link = f"<a target='_blank' href='{url}'>{host_id}</a>"
    return flask.Markup(link)

def list_pos():
    dbs = litestream.fetch_databases()
    return dbs

class DynamicSelectField(SelectField):

    def __init__(self, label=None, validators=None, coerce=str, choices=None, validate_choice=True, **kwargs):
        super(SelectField, self).__init__(label, validators, **kwargs)
        self.coerce = coerce
        if not callable(choices):
            self.choices = list(choices) if choices is not None else None
        else:
            self.choices = choices
        self.validate_choice = validate_choice

    def iter_choices(self):
        if callable(self.choices):
            self.choices = self.choices()
        return super().iter_choices()

class CStoresView(ModelView):
    route_base = "/cstores"
    list_title = "CStores"
    list_columns = ["name", "address", "host_id"]
    show_columns = list_columns + [
        "properties",
        "host_id",
        "id",
        "created_at",
        "updated_at",
    ]
    add_columns = ["name", "address",'properties',"POS"]
    edit_columns = add_columns

    base_order = ("updated_at", "desc")
    datamodel = SQLAInterface(CStore)
    formatters_columns = {
        "address": google_map_link,
        "created_at": utc_to_local,
        "updated_at": utc_to_local,
        "host_id": link_to_pos,
    }
    base_permissions = ["can_add", "can_list", "can_delete", "can_edit", "can_show"]

    add_form_extra_fields = {
        "POS": DynamicSelectField("POS", choices=list_pos,validators=[DataRequired()]),
        "properties": HiddenField("Properties")
    }
    edit_form_extra_fields = add_form_extra_fields

    def process_form(self, form, is_created):
        form.properties.data = json.dumps(dict(HOST_ID=form.POS.data))
        return super().process_form(form, is_created)

def init_app(app):

    if not app.config.get("CSTORES_ENABLED", True):
        return

    appbuilder = app.extensions.get("appbuilder")
    if not appbuilder:
        return

    appbuilder.add_view(
        CStoresView,
        "Stores",
        icon="fa-building",
    )
