#ifndef crc_software
#define crc_software

#include <stdint.h>

#define 	id003	0
#define 	id024	1

typedef union {
	uint16_t result;
	struct {
		uint8_t byte1;
		uint8_t byte2;
	};
} _checksum_;


uint16_t crc16(uint8_t byte, uint16_t fcs);
uint16_t crc_kermit(uint8_t* data, uint8_t number_of_bytes);
uint8_t ckecksum_comparator(uint8_t protocol, uint8_t *DATA);

#endif

