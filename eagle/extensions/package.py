import shutil
import subprocess
import sys
import flask
import click
import os
from pathlib import Path
import tarfile
import tempfile

def create_package(target_dir,project='.'):
    proc = subprocess.run(f"{sys.executable} -m pip wheel --wheel-dir {target_dir} {project}".split())

def init_app(app:flask.Flask):

    @app.cli.group('package')
    def package():
        """Create and install packages"""
        pass

    @package.command('create')
    @click.argument('target',default=f'build/{app.name}')
    @click.option('--tar',default=Path(f'{app.name}.tar.gz'),help='Name of tar file to creat')
    @click.option('--project',default=Path('.'),help='Location of python project')
    def create(target,tar:Path,project:Path):
        """Create a installable package"""
        create_package(target,project=project)
        create_tar(target,tar_name=tar)

    @package.command('install')
    @click.argument('target')
    def install(target):
        """Install a package created by the 'package create' command"""
        install_tar(target)

def install_tar(tar_name):
    with tempfile.TemporaryDirectory() as tmpdir:
        with tarfile.open(tar_name) as t:
            t.extractall(tmpdir) # specify which folder to extract to
        all_files = []
        for root, dirs, files in os.walk(tmpdir):
            for file in files:
                all_files.append(os.path.join(root,file))
        proc = subprocess.run(f"{sys.executable} -m pip install --no-deps {' '.join(all_files)}".split())


def create_tar(source_dir,tar_name=None):

    if not tar_name:
        tar_name = Path(source_dir).name + '.tar.gz'

    with tarfile.open(tar_name, "w:gz") as tar_handle:
        for root, dirs, files in os.walk(source_dir):
            for file in files:
                tar_handle.add(os.path.join(root, file),arcname=file)

if __name__ == '__main__':
    #create_package('build/eagle')
    #create_tar('build/eagle')
    install_tar('eagle.tar.gz')
