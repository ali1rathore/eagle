/*
 * By SydBernard
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <fcntl.h>

#include <sys/time.h>
#include <time.h>
#include <wiringSerial.h>

#include "id003.h" // JCM UBA-10-SS
#include <database.h>
#include "gpio.h"

int s;
int sout;
unsigned char RxData;

void delay_ms(unsigned int howLong);

//#define DEVICE "/dev/ttyUSB0"

/* thread */
void *read_serialport()
{

	while (1)
	{

		if (serialDataAvail(s) >= 1)
		{
			RxData = (unsigned char)serialGetchar(s);
			rx_stream_uba(RxData);
		}
		usleep(5000);
	}
	return NULL;
}
int parse_stdin_args(char *argv[])
{
	return atoi(argv[2]);
}
/* thread */
void *read_stdin()
{
	while (1)
	{
		if (serialDataAvail(s) >= 1)
		{
			RxData = (unsigned char)serialGetchar(s);
			rx_stream_uba(RxData);
		}
		usleep(5000);
	}
	return NULL;
}

int calculate_pulses(int count)
{
	return count;
}

void process_cashin(int count, int mode)
{

	printf("%d\n", count * 100);
	fflush(stdout);
	append_ledger(count * 100);
	if (mode == 0)
	{
		int pulses = calculate_pulses(count);
		write_pulses(pulses);
	}
}

int main(int argc, char *argv[])
{
	// 0. Prase Arguments
	// DATABASE_NAME , mode(0, 1, 2, 3)
	//    0 = uba_pulse
	//    1 = uba_sense
	//    2 = test
	//    3 = POG
	// 1. Initialize Database
	// 2. Create DB Tables
	// 3. Prepare intput device/stdinout (set delay)
	// 4.
	char *device = "stdin";
	if (argc < 3)
	{
		fprintf(stderr,
				"Usage: %s DATABASE mode\nwhere mode is:\n\t(0=uba_pulse, 1=uba_sense, 2=uba_test, 3=POG)\n", argv[0]);
		exit(-1);
	}

	char *DATABASE_NAME = parse_db_args(argv);
	int rc = init_db(DATABASE_NAME);
	int mode = parse_stdin_args(argv);

	fprintf(stderr, "database=%s, mode=%d\n", DATABASE_NAME, mode);

	int delay = 200; // TODO: This is a hack because of race condition or timing
					 //  We need to change delay for stin to work (experimently determined)
	if (mode == 2)
	{
		delay = 1;
	}
	if (mode == 3)
	{
		delay = 10;
	}

	pthread_t thread_id;

	/* Create a new thread. The new thread will run the id003 stream. */
	if (mode == 2)
	{
		s = 0;
		pthread_create(&thread_id, NULL, &read_stdin, NULL);
	}
	else
	{
		if (mode == 0)
		{
			device = "/dev/ttyUSB0";
		}
		else
		{
			device = "/dev/ttyAMA1";
		}
		while ((s = serialOpen(device, 9600)) == -1)
		{
			fprintf(stderr, "Cannot open %s. Waiting 10 seconds\n", device);
			delay_ms(10 * 1000);
		}
		sout = s;
		pthread_create(&thread_id, NULL, &read_serialport, NULL);
	}

	if (mode == 0)
	{
		sout = s;
	}
	else
	{
		sout = open("/dev/null", O_WRONLY);
	}

	fprintf(stderr, "Simple Driver JCM UBA-10-SS (ID003 Protocol) device: %s\n", device);
	setup_wipi();
	int val = -1;
	while (1)
	{

		val = loop_id003();
		if (val > 0)
		{
			process_cashin(val, mode);
		}
		delay_ms(delay);
	}
	return 0;
}

/*
 * delay:
 *	Wait for some number of milliseconds
 *********************************************************************************
 */

void delay_ms(unsigned int howLong)
{
	struct timespec sleeper, dummy;

	sleeper.tv_sec = (time_t)(howLong / 1000);
	sleeper.tv_nsec = (long)(howLong % 1000) * 1000000;

	nanosleep(&sleeper, &dummy);
}
