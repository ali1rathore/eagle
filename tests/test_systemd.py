from eagle import __version__
from eagle.extensions import systemd
import pathlib
import getpass
def test_create_linger_file(app,tmp_path):
    app.config['SYSTEMD_LINGER_DIR'] = tmp_path
    lf = pathlib.Path(systemd.write_linger_file(app))

    assert lf.exists()
    assert lf.name == getpass.getuser()
