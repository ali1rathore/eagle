from eagle.app import create_app
from eagle.extensions import sentry
import pathlib
import pytest

HERE = pathlib.Path(__file__).parent
PROV_FILE = HERE.parent / 'instance' / 'provision.py'

@pytest.mark.skipif(not PROV_FILE.exists(), reason=f"Test provisionning file {PROV_FILE} does not exist")
def test_sentry_endpoint():

    app = create_app(config_file=PROV_FILE)
    sentry.start_app(app)

    @app.route('/sentry-test')
    def sentry_test():
        return 1/0

    client = app.test_client()

    resp = client.get('/sentry-test')
    assert resp.status_code == 500
