#!/usr/bin/env python3
import time
import logging
import sys
import decimal


def read_until(fp, expected, size=None):
    """
    Taking this method from 'serial', since 'stdin' does not provide this method
    for a file descriptor, only 'serial' does.

    Read until an expected sequence is found (line feed by default), the size
    is exceeded or until timeout occurs.
    """
    lenterm = len(expected)
    line = bytearray()
    timeout = 10
    while True:
        c = fp.read(1)
        if c:
            line += c
            if line[-lenterm:] == expected:
                break
            if size is not None and len(line) >= size:
                break
        else:
            break
    return bytes(line)


def run():
    use_stdin = False
    if len(sys.argv) < 2:
        print(f"Usage: {sys.argv[0]} DATABASE", file=sys.stderr)
        exit(1)
    db_path = sys.argv[1]
    import sqlite3

    if len(sys.argv) > 2 and sys.argv[2] == "-":
        use_stdin = True
        print("will use stdin", file=sys.stderr)

    if not use_stdin:
        import serial

        ser = serial.Serial("/dev/ttyS0")
        ser.baudrate = 2400
    else:
        ser = sys.stdin.buffer

    conn = sqlite3.connect(db_path)
    c = conn.cursor()

    while 1:
        my_line = read_until(ser, (b"\x0d"))
        num = my_line.find(b"CRED:")
        if num != -1:
            printed_amount = my_line.decode("ascii")[num + 14 : num + 21]
            amount = decimal.Decimal(printed_amount)
            cash_out = int(amount * -100)
            logging.debug("cash out = " + str(cash_out))
            print(str(cash_out))
            sys.stdout.flush()
            try:
                c.execute(
                    "insert into ledger (amount,timestamp) VALUES (%s,%s)"
                    % (str(cash_out), time.time())
                )
            except sqlite3.OperationalError:
                print("exception  sqlite3.OperationalError", file=sys.stderr)
            conn.commit()


if __name__ == "__main__":
    run()
