import flask
from .model import Machine
from flask_appbuilder.models.sqla.interface import SQLAInterface
from eagle.extensions.appbuilder.baseviews import utc_to_local, ModelView
from flask_appbuilder import action, expose
def address_to_link(addr):
    link =  f"<a href='http://{addr}'>{addr}</a>"
    return flask.Markup(link)

class MachineGeneralView(ModelView):
    route_base = '/machines'
    list_title = "Machines"
    list_columns = ["id", "name", "address","created_at","updated_at","role","available"]
    show_columns = list_columns
    edit_columns = [ "name","available"]
    base_order = ('updated_at','desc')
    list_template = "appbuilder/general/model/list_no_search.html"
    default_view = "display"

    datamodel = SQLAInterface(Machine)
    formatters_columns={'address':address_to_link,'created_at':utc_to_local,'updated_at':utc_to_local}
    base_permissions = ['can_list',"can_delete",'can_edit']

    @action("delete","Delete Machines","Delete these Machine(s) from the UI? You can add them back later.","fa-trash",single=True)
    def delete_machine(self, item):
        """Delete the selected machines(s)"""
        if not isinstance(item,list):
            item = [item]

        for i in item:
            try:
                self._delete(i.id)
            except Exception as e:
                flask.flash(f"Unable to delete: {e}",category="danger")
                
        return flask.redirect(self.get_redirect())


    @expose('/display',methods=['GET'])
    def display(self):
        self.update_redirect()
        return flask.render_template('games.html',title=f'Games')


def init_app(app):
    appbuilder = app.extensions.get('appbuilder')
    if not appbuilder:
        return
    appbuilder.add_view(
        MachineGeneralView, "Machines", icon="fa-microchip",
    )