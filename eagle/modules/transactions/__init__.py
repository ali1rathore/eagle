from . import model, view
from eagle.extensions.appbuilder import db
import flask

def get_last_transaction():
    return db.session.query(model.Transaction).order_by(model.Transaction.created_at.desc()).first()

def most_recent_transactions(app=None):
    """Returns the most recent transaction time for each machine

    Args:
        app (Flask, optional): The flask application. Defaults to None.

    Returns:
        dict: mapping for each Machine.id to its most recent Transaction.created_at
    """
    from sqlalchemy import func, and_
    if not app:
        app = flask.current_app

    subq = db.session.query(
        model.Transaction.id,
        func.max(model.Transaction.created_at).label('maxdate')
    ).group_by(model.Transaction.machine_id).subquery('t2')

    query = db.session.query(model.Transaction.machine_id,model.Transaction.created_at).join(
        subq,
        and_(
            model.Transaction.id == subq.c.id,
            model.Transaction.created_at == subq.c.maxdate
        )
    )
    return dict(query.all())    

def init_app(app):
    model.init_app(app)
    view.init_app(app)