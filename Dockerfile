# For more information, please refer to https://aka.ms/vscode-docker-python
FROM python:3.9-bullseye

RUN apt update --yes \
    && apt install --yes make gcc wget git

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

RUN mkdir /code
WORKDIR /code

# Creates a non-root user with an explicit UID and adds permission to access the /app folder
# For more info, please refer to https://aka.ms/vscode-docker-python-configure-containers
RUN adduser -u 5678 --disabled-password --gecos "" developer && chown -R developer /code
USER developer

RUN python3 -m pip install --upgrade pip

COPY eagle /code/eagle
COPY setup.py /code/setup.py
RUN python3 -m pip install -e "/code[dev]"

# During debugging, this entry point will be overridden. For more information, please refer to https://aka.ms/vscode-docker-python-debug
CMD ["python","-m","gunicorn","eagle.server:create_server",'--bind','0.0.0.0:5000']
