import logging
from tb_rest_client.rest import ApiException
from tb_rest_client import rest_client_pe as pe

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(levelname)s - %(module)s - %(lineno)d - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')


def create_package(title,version,url,tag,api_url,tenant_id,device_profile_id,username,password,type='FIRMWARE'):
    saved_package = None
    # Creating the REST client object with context manager to get auto token refresh
    with pe.RestClientPE(base_url=api_url) as rest_client:
        try:
            # Auth with credentials
            rest_client.login(username=username, password=password)

            # Create a Package
            package = pe.OtaPackageInfo(
                url=url,
                tenant_id=pe.TenantId(tenant_id,entity_type="TENANT"),
                device_profile_id=pe.DeviceProfileId(device_profile_id,entity_type='DEVICE_PROFILE'),
                type=type,version=version,tag=tag,title=title)
            saved_package = rest_client.save_ota_package_info(package)
        except ApiException as e:
            logging.exception(e)

    return saved_package


if __name__ == '__main__':
    from flask import Config
    import sys
    
    if len(sys.argv) > 1:
        url = sys.argv[1]
    else:
        url =  'https://google.com'


    if len(sys.argv) > 2:
        commit = sys.argv[2]
    else:
        commit =  'dev'

    config = Config('instance')
    if not config.from_envvar('EAGLE_CONFIG',silent=True):
        if not config.from_pyfile('globals.py',silent=True):
            print("EAGLE_CONFIG not set")
            exit()

    title = 'eagle'
    version = commit
    create_package(
        title=title,
        version=version,
        url=url,
        tag=f"{title}-{version}",
        api_url=config['TB_API_URL'],
        tenant_id=config['TB_API_TENANT_ID'],
        device_profile_id=config['TB_API_DEVICE_PROFILE_ID'],
        username=config['TB_API_USERNAME'],
        password=config['TB_API_PASSWORD']
    )
