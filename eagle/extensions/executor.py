from flask_executor import Executor
from flask_executor.executor import push_app_context, copy_current_request_context, FutureProxy
import concurrent
import flask

def _prepare_fn(self, fn, force_copy=False):
    """
    Overwrite this function to not require request context so we can use
    executor outside of requests
    """
    if isinstance(self._self, concurrent.futures.ThreadPoolExecutor) or force_copy:
        if flask.has_request_context():
            fn = copy_current_request_context(fn)
        if flask.current_app.config[self.EXECUTOR_PUSH_APP_CONTEXT]:
            fn = push_app_context(fn)
    return fn
Executor._prepare_fn = _prepare_fn

def _submit(self, fn, *args, **kwargs):
    fn = self._prepare_fn(fn)
    if self._self._shutdown:
        return
    future = self._self.submit(fn, *args, **kwargs)
    for callback in self._default_done_callbacks:
        future.add_done_callback(callback)
    return FutureProxy(future, self)
Executor.submit = _submit

def start_app(app):
    pass

def stop_app(app):
    
    executor = app.extensions.get('executor')
    if executor:
        executor.shutdown(wait=False,cancel_futures=True)

def init_app(app):
    executor = Executor()

    app.config['EXECUTOR_PROPAGATE_EXCEPTIONS'] = True
    app.config['EXECUTOR_TYPE'] = 'thread'
    app.config['EXECUTOR_MAX_WORKERS'] = 50

    executor.init_app(app)