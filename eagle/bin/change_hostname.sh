#!/bin/bash

if [ ! -n "$1" ] ; then
	echo 'Missing argument: new_hostname'
	exit 1
fi

if [ "$(id -u)" != "0" ] ; then
	echo "Sorry, you are not root."
	exit 2
fi

CUR_HOSTNAME=$(cat /etc/hostname)
NEW_HOSTNAME=$1

# Change the hostname
hostnamectl set-hostname $NEW_HOSTNAME
if [ $? -ne 0 ] 
then
    exit 1
fi

hostname $NEW_HOSTNAME
if [ $? -ne 0 ] 
then
    exit 1
fi
# Change hostname in /etc/hosts & /etc/hostname
sed -i "s/$CUR_HOSTNAME/$NEW_HOSTNAME/g" /etc/hosts
if [ $? -ne 0 ] 
then
    exit 1
fi
