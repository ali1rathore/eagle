from flask.testing import FlaskClient



def test_home_exists(client:FlaskClient):
    resp =  client.get('/')
    assert 'role' in resp.data.decode()