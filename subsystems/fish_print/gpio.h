void loop_count_pulses(void);
int process_cashout(int);
static volatile int count = 0;

#ifdef __arm__
#include <wiringPi.h>

const unsigned int ipin = 25;
const unsigned int opin = 24;
const unsigned int period = 24;

void setup_wipi(void)
{
    wiringPiSetup();
    // Set pin
    pinMode(opin, OUTPUT);
    digitalWrite(opin, HIGH);

    pinMode(ipin, INPUT);
    pullUpDnControl(ipin, PUD_UP);
    fprintf(stderr, "All set, counting pulses\n");
}

void loop_count_pulses()
{
    while (1)
    {
        if (digitalRead(ipin) == LOW)
        {
            while (1)
            {
                digitalWrite(opin, LOW);
                delay(period);
                digitalWrite(opin, HIGH);
                delay(period);
                count++;
                if (digitalRead(ipin) == HIGH)
                {
                    fprintf(stderr, "count = %d\n", count);
                    process_cashout(count * -100);
                    count = 0;
                    break;
                }
            }
        }
        sleep(.5);
    }
}

#else

void setup_wipi(void)
{
    fprintf(stderr, "All set, counting fake pulses\n");

}
void write_pulses(int count)
{
}

void loop_count_pulses(void)
{
    while (1)
    {
        sleep(1);
    }
}
#endif
