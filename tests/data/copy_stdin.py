#!/usr/bin/python3
import sys
import fileinput

for line in fileinput.input():
    print(line.rstrip())
    sys.stdout.flush()
    