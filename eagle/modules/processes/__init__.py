from eagle import models
from eagle.extensions import thingsboard, services
from . import views, signals

def handle_stdout_event(service,**data):
    lines = data['line'].split('\n')
    for line in lines:
        amount = int(line)
        models.append_transaction_to_db(service.app,amount)

def publish_stdout_event(service,**data):
    lines = data['line'].split('\n')
    for line in lines:
        amount = int(line)
        if amount > 0:
            thingsboard.publish_to_thingsboard(app=service.app,cashin=amount)
        elif amount < 0:
            thingsboard.publish_to_thingsboard(app=service.app,cashout=amount)

def handle_stderr_event(service,**data):
    verbose = service.app.config.get('SERVICES_DEBUG',False) if service.app else True
    logit = print
    lines = data['line'].split('\n')
    for line in lines:
        if line.startswith('[DEBUG]'):
            if verbose:
                logit = service.app.logger.debug if service.app else print
            else:
                logit = lambda x:x
        elif line.startswith('[ERROR]'):
            logit = service.app.logger.error if service.app else print
        else:
            logit = service.app.logger.debug if service.app else print

        logit(f"[{service.app.name if service.app else ''}][{service.name}] {line}")
def init_services(app):
    """Read services from config app.config"""
    import sys
    sm = app.extensions.get('services')
    if sm:
        sm.kill()
    sm = services.ServiceManager()
    app.extensions['services'] = sm

    kwargs = dict(app=app,python=sys.executable,**app.config)

    for s in ['CASHIN','CASHOUT','HANDCOUNT']:
        if s in app.config and app.config[s] and app.config[s] in app.config['SERVICES_LIST']:
            cmd_name = app.config[s] 
            cmd = app.config['SERVICES_LIST'][cmd_name]
            formatted_command =  cmd.format(**kwargs)
            svs = services.Service(formatted_command,app=app,name=s.lower(),restart=False)
            svs.stdout_event.connect(handle_stdout_event)
            svs.stdout_event.connect(publish_stdout_event)
            svs.stderr_event.connect(handle_stderr_event)
            svs.debug = app.logger.debug
            sm[s.lower()] = svs
            app.logger.debug(f'Created "{s.lower()}" service: {formatted_command}')

def init_app(app):
    init_services(app)
    views.init_app(app)