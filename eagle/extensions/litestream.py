import flask
from time import sleep
from pathlib import Path
import shlex, subprocess
import functools
from eagle.config import get_my_ip

def litestream_bin(app:flask.Flask):
    return str(Path(app.root_path) / 'bin' / app.config['PLATFORM'] / 'litestream')

def litestream_cmd(app:flask.Flask):
    return litestream_bin(app) + ' replicate -config {LITESTREAM_CONFIG}'.format(**app.config)

def litestream_restore_cmd(app:flask.Flask):
    return litestream_bin(app) + ' restore -config {LITESTREAM_REPLICA_CONFIG}'.format(**app.config)

LITESTREAM_REPLICATE_TEMPLATE = """
access-key-id: {{config.AWS_ACCESS_KEY_ID}}
secret-access-key: {{config.AWS_SECRET_ACCESS_KEY}}

dbs:
{% for db in ['TRANSACTIONS_DB','MACHINES_DB','USERS_DB','REPORTS_DB']%}
    - path: {{config[db]}}
      replicas:
        - type: s3
          bucket: {{config.LITESTREAM_BUCKET}}
          path:   {{config.LITESTREAM_PREFIX}}{{config.HOST_ID}}/{{config[db].name}}
{% endfor %}
"""
LITESTREAM_RESTORE_TEMPLATE = """
access-key-id: {{config.AWS_ACCESS_KEY_ID}}
secret-access-key: {{config.AWS_SECRET_ACCESS_KEY}}

dbs:
    # existing replicas for other instances
    {% for r in replicas %}
{% for db in ['TRANSACTIONS_DB','MACHINES_DB','USERS_DB','REPORTS_DB']%}

    - path: {{config.INSTANCE_PATH}}/replicas/{{r}}/{{config[db].name}}
      replicas:
        - type: s3
          bucket: {{config.LITESTREAM_BUCKET}}
          path:   {{config.LITESTREAM_PREFIX}}{{r}}/{{config[db].name}}

{% endfor %}
    {% endfor%}
"""

LITESTREAM_BINARIES_VERSION = 'v0.3.9'
LITESTREAM_BINARIES = dict(
    x86 = 'linux-amd64.tar.gz',
    Darwin = 'darwin-amd64.zip',
    arm = 'linux-arm7.tar.gz'
)
LITESTREAM_BINARIES_URL = 'https://github.com/benbjohnson/litestream/releases/download/{version}/litestream-{version}-{platform}'

def create_litestream_config(app:flask.Flask):

    key_id = app.config.get('AWS_ACCESS_KEY_ID')
    secret_key = app.config.get('AWS_SECRET_ACCESS_KEY')
    bucket = app.config.get('LITESTREAM_BUCKET')

    if not all([key_id,secret_key,bucket]):
        return False

    content = flask.render_template_string(app.config['LITESTREAM_REPLICATE_TEMPLATE'])
    litestream_config = Path(app.config.setdefault('LITESTREAM_CONFIG',Path(app.instance_path)/'litestream.yml'))
    litestream_config.write_text(content)

    return True

def create_replicas_config(app,replicas):
    content = flask.render_template_string(app.config['LITESTREAM_RESTORE_TEMPLATE'],replicas=replicas)
    litestream_config = Path(app.config.setdefault('LITESTREAM_REPLICA_CONFIG',Path(app.instance_path)/'replicas.litestream.yml'))
    litestream_config.write_text(content)

    return True
    
def init_app(app:flask.Flask):
    prefix = app.config.setdefault('LITESTREAM_PREFIX',f'litestream-{app.name}-{app.env}/')

    app.config.from_object(__name__)
    disabled = ('LITESTREAM_ENABLED' in app.config and app.config['LITESTREAM_ENABLED'] == False)
    
    if not disabled:
        enabled = app.config['LITESTREAM_ENABLED'] = create_litestream_config(app)
        if not enabled:
            app.logger.debug(f'Litestream is not enabled')
            return
    try:
        download_binary(app)
    except Exception as e:
        app.logger.error(f'Failed downloading litestream binary: {e}')
        
class AWSDatabaseStorage():

    def __init__(self,bucket,prefix,**creds) -> None:
        import boto3
        self.prefix=prefix
        self.bucket=bucket
        self.s3_client = boto3.client('s3',**creds)

    def list_databases(self):
        return (f.removeprefix(self.prefix) for f in self.list_folders(self.s3_client,self.bucket,self.prefix))

    def list_database_files(self,db):
        return self.list_folders(self.s3_client,self.bucket,self.prefix+db)

    def list_folders(self,s3_client,bucket_name,prefix=''):
        response = s3_client.list_objects_v2(Bucket=bucket_name, Prefix=prefix, Delimiter='/')
        for content in response.get('CommonPrefixes', []):
            yield content.get('Prefix')

def download_binary(app:flask.Flask,version=None,platform=None):
    import requests, stat
    
    platform = platform or app.config['PLATFORM']
    executable_file: Path = Path(litestream_bin(app))
    if executable_file.exists():
        return True
    else:
        executable_file.parent.mkdir(parents=True,exist_ok=True)
    version = version or app.config['LITESTREAM_BINARIES_VERSION']
    url_path = app.config['LITESTREAM_BINARIES'][platform]
    url = app.config['LITESTREAM_BINARIES_URL'].format(version=version,platform=url_path)
    target_path = executable_file.parent / f'litestream-{url_path}'
    app.logger.debug(f'Downloading litestream binary from {url} to {executable_file}')
    response = requests.get(url, stream=True)
    if not response.status_code == 200:
        return False

    with open(target_path, 'wb') as f:
        f.write(response.raw.read())

    if url_path.endswith('.zip'):
        import zipfile
        with zipfile.ZipFile(target_path, 'r') as zip_ref:
            zip_ref.extractall(executable_file.parent)
    elif url_path.endswith('.tar.gz'):
        import tarfile
        with tarfile.open(target_path) as my_tar:
            my_tar.extractall(executable_file.parent) # specify which folder to extract to
            
    executable_file.chmod(executable_file.stat().st_mode | stat.S_IEXEC)
    return True
    
def run_restore_command(app,replica,replace=False):
    replica_path: Path = Path(app.instance_path) / 'replicas' / replica
    if replica_path.exists():
        if replace:
            replica_path.unlink()
        else:
            return None
            
    cmd = litestream_restore_cmd(app) + ' ' + str(replica_path)
    
    result = subprocess.run(shlex.split(cmd), stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    error = None
    if result.returncode != 0:
        app.logger.debug(f"Failed restoring {replica}: {result.stderr}")
        error = result.stderr.decode()
    
    return error

@functools.lru_cache()
def fetch_databases():
    app = flask.current_app
    creds = dict(
            aws_access_key_id=app.config.get('AWS_ACCESS_KEY_ID'), 
            aws_secret_access_key=app.config.get('AWS_SECRET_ACCESS_KEY')
            )

    bucket_name = app.config.get('LITESTREAM_BUCKET')
    prefix = app.config['LITESTREAM_PREFIX']

    if not all(list(creds.values())+[bucket_name]):
        return

    bds = AWSDatabaseStorage(bucket_name,prefix,**creds)
    folder_list = [d.rstrip('/') for d in bds.list_databases()]
    return folder_list