from eagle.extensions.services import Service, ServiceManager
from eagle.extensions import services
from eagle.config import get_platform
import eagle
import sys, shlex

from eagle.modules.testing.ba_genstream import generate_ba_bytes, generate_banila_bytes, generate_pog_cashout_bytes
from pathlib import Path
from time import sleep
import sys

import pytest

PLATFORM=get_platform()

BIN=Path(eagle.__file__).parent / 'bin' / PLATFORM
PYBIN=Path(eagle.__file__).parent / 'bin'

FLUSH=[True,False]
@pytest.mark.parametrize("flush", FLUSH)
def test_cat_service(flush):
    cat = Service('cat')
    outputs = []
    expected_outputs = []
    def on_stdout(l):
        outputs.append(l)
    cat.on_stdout = on_stdout
    cat.start()
    for i in range(150000):
        l = cat.write(i,flush=flush)
        assert l != 0
        expected_outputs.append(str(i))
    cat.stop()
    expected_outputs = ''.join(expected_outputs)
    outputs = ''.join(outputs)

    assert outputs == expected_outputs

def test_bill_acceptor_service(tmp_path):

    cmd = str(BIN / 'uba_pulse') + f' "{tmp_path}/dummy.db" 2'
    ba = Service(cmd)
    started = []
    inputs = [10, 20, 50, 100]
    outputs = []
    expected_outputs = [str(i*100) for i in inputs]
    
    def on_start():
        started.append(True)

    def on_stdout(l):
        outputs.append(l)
    
    ba.on_start = on_start
    ba.on_stdout = on_stdout

    ba.start()
    sleep(3)
    assert ba.is_alive()
    assert ba.returncode == None
    for i in inputs:
        ba_bytes = generate_ba_bytes(i)
        ba.write(ba_bytes)
        if PLATFORM == 'arm':
            sleep(10)
    if PLATFORM != 'arm':
        sleep(5)
    ba.stop(wait=False)
    sleep(1)
    ba.kill()

    assert len(started) == 1
    assert ba.returncode != None
    assert not ba.is_alive()
    assert expected_outputs == outputs

def test_fish_cashout_service(tmp_path):
    cmd = str(BIN / 'fish_print') + f' "{tmp_path}/dummy.db" 2'
    fish = Service(cmd)
    started = []
    inputs = [10]
    outputs = []
    expected_outputs = [str(i*-100) for i in inputs]

    def on_start():
        started.append(True)

    def on_stdout(l):
        outputs.append(l)
    
    fish.on_start = on_start
    fish.on_stdout = on_stdout

    fish.start()
    sleep(3)
    assert fish.is_alive()
    assert fish.returncode == None
    for i in inputs:
        fish.write(f'{i}\n'.encode())
    sleep(5)
    fish.stop(wait=False)
    sleep(1)
    fish.kill()
    assert len(started) == 1
    assert fish.returncode != None
    assert not fish.is_alive()
    assert expected_outputs == outputs

def test_lol_cashout_service(tmp_path):
    cmd = str(BIN / 'lol_print') + f' "{tmp_path}/dummy.db" 2'
    lol = Service(cmd)
    started = []
    inputs = [10]
    outputs = []
    expected_outputs = [str(i*-100) for i in inputs]

    def on_start():
        started.append(True)

    def on_stdout(l):
        outputs.append(l)
    
    lol.on_start = on_start
    lol.on_stdout = on_stdout

    lol.start()
    sleep(3)
    assert lol.is_alive()
    assert lol.returncode == None
    for i in inputs:
        lol.write(f'AMOUNT   ${i}\n'.encode())
    sleep(5)
    lol.stop(wait=False)
    sleep(1)
    lol.kill()
    assert len(started) == 1
    assert lol.returncode != None
    assert not lol.is_alive()
    assert expected_outputs == outputs

def test_lol1_cashout_service(tmp_path):
    cmd = str(BIN / 'lol1_print') + f' "{tmp_path}/dummy.db" 2'
    lol1 = Service(cmd)
    started = []
    inputs = [10]
    outputs = []
    expected_outputs = [str(i*-1) for i in inputs]

    def on_start():
        started.append(True)

    def on_stdout(l):
        outputs.append(l)
    
    lol1.on_start = on_start
    lol1.on_stdout = on_stdout

    lol1.start()
    sleep(3)
    assert lol1.is_alive()
    assert lol1.returncode == None
    for i in inputs:
        lol1.write(f'CREDIT: {i}\n'.encode())
    sleep(5)
    lol1.stop(wait=False)
    sleep(1)
    lol1.kill()
    assert len(started) == 1
    assert lol1.returncode != None
    assert not lol1.is_alive()
    assert expected_outputs == outputs

def test_banilla_cashout_service(tmp_path):
    cmd = str(PYBIN / 'banilla_print.py') + f' "{tmp_path}/dummy.db" -'
    banilla = Service(cmd)
    started = []
    inputs = [10]
    outputs = []
    expected_outputs = [str(i*-100) for i in inputs]

    def on_start():
        started.append(True)

    def on_stdout(l):
        outputs.append(l)
    
    banilla.on_start = on_start
    banilla.on_stdout = on_stdout

    banilla.start()
    sleep(3)
    assert banilla.is_alive()
    assert banilla.returncode == None
    for i in inputs:
        banilla_bytes = generate_banila_bytes(i)
        banilla.write(banilla_bytes)
    sleep(5)
    banilla.stop(wait=False)
    sleep(1)
    banilla.kill()
    assert len(started) == 1
    assert banilla.returncode != None
    assert not banilla.is_alive()
    assert expected_outputs == outputs

def test_pog_cashout_service(tmp_path):
    cmd = str(PYBIN / 'pog_print.py') + f' "{tmp_path}/dummy.db" -'
    pog = Service(cmd)
    started = []
    inputs = [10,9.45]
    outputs = []
    errors = []
    expected_outputs = ['-1000','-945']

    def on_start():
        started.append(True)

    def on_stdout(l):
        outputs.append(l)
    
    def on_stderr(l):
        errors.append(l)

    pog.on_start = on_start
    pog.on_stdout = on_stdout
    pog.on_stderr = on_stderr

    pog.start()
    sleep(3)
    assert pog.is_alive()
    assert pog.returncode == None
    for i in inputs:
        pog_bytes = generate_pog_cashout_bytes(i)
        pog.write(pog_bytes)
    sleep(5)
    pog.stop(wait=False)
    sleep(1)
    pog.kill()
    assert len(started) == 1
    assert pog.returncode != None
    assert not pog.is_alive()
    assert expected_outputs == outputs

def test_remote_pulse_cashin_service(tmp_path):
    cmd = str(PYBIN / 'remote_pulse_cashin.py') + f' "{tmp_path}/dummy.db" "10"'
    fish = Service(cmd)
    started = []
    inputs = [10]
    outputs = []
    errors = []
    expected_outputs = [str(i*100) for i in inputs]

    def on_start():
        started.append(True)

    def on_stdout(l):
        outputs.append(l)
    
    def on_stderr(l):
        errors.append(l)

    fish.on_start = on_start
    fish.on_stdout = on_stdout
    fish.on_stderr = on_stderr

    fish.start()
    sleep(3)
    assert fish.is_alive()
    assert fish.returncode == None
    for i in inputs:
        fish_bytes = dict(denom=i)
        fish.write(fish_bytes)
    sleep(5)
    fish.stop(wait=False)
    sleep(1)
    fish.kill()
    assert len(started) == 1
    assert fish.returncode != None
    assert not fish.is_alive()
    assert expected_outputs == outputs

def test_nonexisting_service():
    cmd = 'thisshouldnotbeareleabinaryexecutablethingy'
    cat = Service(cmd)
    start_errors = []

    def on_start_fail(e):
        start_errors.append(e)

    cat.on_start_fail = on_start_fail

    cat.start()
    assert str(start_errors[0]) == f"[Errno 2] No such file or directory: '{cmd}'"
    assert cat.is_alive() is False
    assert cat._process is None

def test_multiple_services():

    services = {}
    services['ba1'] = Service('cat')
    services['ba2'] = Service('cat')
    outputs = {'ba1':[],'ba2':[]}
    expected_outputs = {'ba1':[],'ba2':[]}

    def on_stdout1(l):
        outputs['ba1'].append(l)

    def on_stdout2(l):
        outputs['ba2'].append(l)
    services['ba1'].on_stdout = on_stdout1
    services['ba2'].on_stdout = on_stdout2

    for name,s in services.items():
        s.start()

    for name,s in services.items():
        for i in range(1500):
            l = s.write(i)
            assert l != 0
            expected_outputs[name].append(str(i))

    for name, s in services.items():        
        s.stop(wait=True)

    for name, s in services.items():        
        s.kill()

    assert {
        'ba1':[''.join(outputs['ba1'])],
        'ba2':[''.join(outputs['ba2'])]
        } == {
        'ba1':[''.join(expected_outputs['ba1'])],
        'ba2':[''.join(expected_outputs['ba2'])]
        }


def test_sm_status():

    services = ServiceManager()
    services['cat1'] = Service('cat')
    services['cat2'] = Service('cat')

    status = services.stopped()
    assert status['cat1'] == status['cat2'] == False

    services.start()
    status = services.started()
    assert status['cat1'] == status['cat2'] == True

    status = services.stopped()
    assert status['cat1'] == status['cat2'] == False

    services.stop()
    status = services.stopped()
    assert status['cat1'] == status['cat2'] == True
    


def test_sm_start_stop():

    services = ServiceManager()
    services['cat1'] = Service('cat')
    services['cat2'] = Service('cat')
    services['cat3'] = Service('cat')

    services.start()
    for s in services.values():
        assert s.returncode == None
        assert s.is_alive()

    services.stop()

    for s in services.values():
        assert s.returncode != None
        assert not s.is_alive()


def test_service_create(create_app):

    app = create_app(ROLE='LOL2')
    all_services = app.config['SERVICES_LIST']
    
    kwargs = dict(app=app,python=sys.executable,**app.config)

    formatted_services = { 
        n : Service(c.format(**kwargs)) for (n,c) in all_services.items() 
    }
    
    sm = ServiceManager(formatted_services)

    
    expected_command = '{app.root_path}/bin/{PLATFORM}/uba_pulse "{DATABASE}" 1'.format(DATABASE=app.config['DATABASE'],PLATFORM=PLATFORM,app=app)

    assert sm['eagle_sense'].command == shlex.split(expected_command)



def test_init_app(create_app):

    app = create_app(ROLE='LOL1')

    services.init_app(app)

    sm = app.extensions['services']
    expected_command = '{app.root_path}/bin/{PLATFORM}/uba_pulse "{DATABASE}" 1'.format(DATABASE=app.config['DATABASE'],PLATFORM=PLATFORM,app=app)
    assert sm['cashin'].command == shlex.split(expected_command)

def test_start_app(create_app):

    app = create_app(ROLE='POG_TEST')

    services.init_app(app)

    services.start_app(app)

    sm: ServiceManager = app.extensions['services']
    assert len(sm) == 2
    expected_cashin = '{app.root_path}/bin/{PLATFORM}/uba_pulse "{DATABASE}" 2'.format(DATABASE=app.config['DATABASE'], PLATFORM=PLATFORM,app=app)
    expected_cashout = '{app.root_path}/bin/pog_print.py "{DATABASE}" -'.format(app=app,DATABASE=app.config['DATABASE'])
    assert sm['cashin'].command == shlex.split(expected_cashin)
    assert sm['cashout'].command == shlex.split(expected_cashout)

    status = sm.started()
    assert status['cashin'] == status['cashout'] == True

    sm.kill()
    status = sm.stopped()
    assert status['cashin'] == status['cashout'] == True


def test_stop_app(create_app):

    app = create_app(ROLE='POG_TEST')

    services.init_app(app)

    services.start_app(app)

    sm: ServiceManager = app.extensions['services']
    assert len(sm) == 2
    expected_ba = '{app.root_path}/bin/{PLATFORM}/uba_pulse "{DATABASE}" 2'.format(DATABASE=app.config['DATABASE'], PLATFORM=PLATFORM,app=app)
    expected_co = '{app.root_path}/bin/pog_print.py "{DATABASE}" -'.format(app=app,DATABASE=app.config['DATABASE'])
    assert sm['cashin'].command == shlex.split(expected_ba)
    assert sm['cashout'].command == shlex.split(expected_co)

    status = sm.started()
    assert status['cashin'] == status['cashout'] == True

    services.stop_app(app)

    status = sm.stopped()
    assert status['cashin'] == status['cashout'] == True
