from .model import Transaction
import flask
from flask_appbuilder.models.sqla.interface import SQLAInterface
from flask_appbuilder import expose, action
from eagle.extensions import printing, appbuilder
from flask_appbuilder.widgets import ListWidget
from eagle.extensions.appbuilder import ModelView, HTMXView
from eagle.modules.machines.model import Machine
def render_receipt(t:Transaction, app:flask.Flask=None):

    if not app:
        app = flask.current_app
    with app.app_context():
        machine_name = t.machine_id
        machine = app.appbuilder.session.query(Machine).filter(Machine.id==t.machine_id).limit(1).first()
        if machine:
            machine_name = machine.name
        rendered_receipt = flask.render_template('recpt.j2', **{'MACHINE_NAME':machine_name, 'AMOUNT':t.amount, 'DATE':t.created_at,'ID':t.id})
    return rendered_receipt


class ListDownloadWidget(ListWidget):
    template = 'appbuilder/general/widgets/list_download.html'

class TransactionGeneralView(ModelView,HTMXView):
    route_base = '/transactions'
    list_title = "Transactions"
    base_permissions = ['can_list','can_show']
    datamodel = SQLAInterface(Transaction)

    label_columns = {"machine": "Machine"}
    list_columns = ["amount", "machine_id","created_at","updated_at"]
    show_columns = list_columns + ["id"]
    base_order = ('created_at','desc')
    exclude_route_methods = ("put", "post", "delete", "info","get")
    default_view = 'display'
    
    list_widget = ListDownloadWidget

    @action("view","View",confirmation=None,icon="fa-list",single=True)
    def view_receipts(self, item):
        """Shows rendered transaction(s)"""
        if not isinstance(item,list):
            item = [item]

        receipts = []

        for i in item:
            receipt = render_receipt(i)
            receipts.append(receipt)

        return flask.render_template('base.html',content = '<pre>' + '\n'.join(receipts) + '</pre>')


    @action("print","Print","Print receipts?","fa-print",single=True)
    def print_receipts(self, item):
        """Sends the `on_print` signal with the rendered transaction(s)"""
        if not isinstance(item,list):
            item = [item]

        for i in item:
            try:
                receipt = render_receipt(i)
                printing.on_print.send(flask.current_app,text=receipt)
            except Exception as e:
                flask.flash(f"Unable to print: {e}",category="danger")
                
        return flask.redirect(self.get_redirect())

    @expose('/csv', methods=['GET'])
    def download_csv(self):
        from flask_appbuilder.urltools import get_filter_args
        get_filter_args(self._filters)
        order_column, order_direction = self.base_order
        count, lst = self.datamodel.query(self._filters, order_column, order_direction)
        csv = ''
        for item in self.datamodel.get_values(lst, self.list_columns):
            csv += str(item) + '\n'
        response = flask.make_response(csv)
        response.headers['Content-Disposition'] = 'attachment; filename=mycsv.csv'
        response.mimetype='text/csv'
        return response

def init_app(app):
    appbuilder = app.extensions.get('appbuilder')
    if not appbuilder:
        return
    appbuilder.add_view(TransactionGeneralView, "Transactions", icon="fa-bank")
