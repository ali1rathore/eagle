import logging
from blinker import signal
log_emitted = signal('log-emitted',doc="A log message was emitted by the application")

class SignalLogger(logging.Handler):
    """For generating logs in a SQLite file"""
    def __init__(self, app, level=logging.NOTSET):
        super().__init__(level)
        self._app = app

    def emit(self, record):
        """
        Generates a new log into the SQLite file
        """
        self.acquire()
        try:
            log_emitted.send(self._app,name=record.name, level=record.levelname,function=record.funcName,thread=record.thread, path=record.pathname, msg=record.msg)
        except Exception as e:
            print(f"Failed to write to logdb: {e}")
        finally:
            self.release()

def init_app(app):
    logger = app.logger

    if app.config.get('ENABLE_SIGNAL_LOGGER'):
        siglogger = SignalLogger(app)
        logger.addHandler(siglogger)
        logger.setLevel(logging.DEBUG)
        app.logger.debug("Enabled SignalLogger.")