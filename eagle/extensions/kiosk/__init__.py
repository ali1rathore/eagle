from blinker import signal
import os, flask, shutil, re
from pathlib import Path
from eagle.config import get_platform
from eagle.extensions import services
from eagle.extensions.systemd.install import  start_service, stop_service, install_services, uninstall_services
from . import keyboards
#start_kiosk = signal("start-kiosk")
bp = flask.Blueprint('kiosk',__name__,template_folder='templates', static_folder='static')

@bp.route('/toggle-keyboard')
def keyboard():
    if flask.session.get('keyboard') == True:
        flask.session['keyboard'] = False
    else:
        flask.session['keyboard'] = True
    return flask.redirect(flask.request.referrer)

def install_kiosk_files(app):
    SCRIPT_PATH = Path(bp.root_path) / 'bin' / 'install_kiosk.sh'
    if(get_platform() == 'arm'):
        os.system(f'sh {SCRIPT_PATH}')
    else:
       app.logger.debug(f'Dummy start kiosk, would have run {SCRIPT_PATH}') 

def parse_resolution(res_string):
    match = re.search(r'\d+x\d+', res_string)
    if match:
        res=match.group(0)
        return res.replace('x',',')

def start_kiosk(app):
    fb_path = Path('/sys/class/graphics/fb0/modes')
    if fb_path.exists():
      fb_mode = fb_path.read_text()
      res = parse_resolution(fb_mode)
      app.config['WINDOW_SIZE']=res
    else:
        app.logger.debug("Framebuffer 0 not available, will not start kiosk")
        return
    if(get_platform() == 'arm') and fb_path.exists():
        home_path = Path(os.environ['HOME'])
        if not home_path.exists():
            return
        xinitrc_path = home_path / '.xinitrc'
        rendered_xinitrc = flask.render_template_string(app.config["XINITRC_TEMPLATE"])
        xinitrc_path.write_text(data=rendered_xinitrc)
        install_services(app, ['kiosk'], dry_run=(not shutil.which("systemctl")), enable=False)
        app.logger.debug("starting kiosk service")
        start_service('kiosk')

def stop_kiosk(app):
    if(get_platform() != 'arm'):
        app.logger.debug("stopping kiosk service")
        with app.app_context():
         stop_service('kiosk')
    

def get_current_keyboard_config():
    kb = flask.request.args.get('keyboard')
    if kb == 'numpad':
        return keyboards.DEFAULT_NUMPAD
    return keyboards.DEFAULT_KEYBOARD
    
def init_app(app:flask.Flask):
    app.register_blueprint(bp, url_prefix='/kiosk')
    app.add_template_global(get_current_keyboard_config,name='kioskboard')

def start_app(app):
    if app.config.get('ENABLE_KIOSK'):
        app.logger.debug("ENABLE_KIOSK found, enabling kiosk mode")
        start_kiosk(app)
    else:
        app.logger.debug("ENABLE_KIOSK not found, disabling kiosk mode")
        stop_kiosk(app)

def stop_app(app):
    stop_kiosk(app)

