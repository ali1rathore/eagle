import click, flask
from blinker import signal
import subprocess
import sys
update_requested = signal('updated-requested')

PIP_INSTALL_CMD='{sys.executable} -m pip install --log {logfile} --upgrade git+{url}@{version}'
PIP_UNINSTALL_CMD='{sys.executable} -m pip uninstall --log {logfile} --yes {name}'
PIP_INSTALL_LOGFILE='/tmp/updater.log'

def add_commands(app):
    @app.cli.command('update',help=f"Update {app.name}")
    def update_app():
        f"""Update this application ({app.name})"""
        update_requested.send(flask.current_app._get_current_object(),version=None)

def get_git_tag():
    git_version = None
    try:
        git_version = subprocess.check_output(['git', 'describe','--tags']).decode().strip()
    except:
        pass    
    return git_version

def fetch_gitlab_tags(project_id):
    import requests
    response = requests.get(f'https://gitlab.com/api/v4/projects/{project_id}/repository/tags').json()
    return response

def pip_install_command(app,url=None,version=None):
    """Generate the command to install this app."""
    version = version or app.config['VERSION']
    url = url or app.config['GITLAB_URL']
    return PIP_INSTALL_CMD.format(sys=sys,url=url,version=version,logfile=app.config['PIP_INSTALL_LOGFILE'])

def pip_uninstall_command(app):
    """Generate the command to uninstall this app."""
    return PIP_UNINSTALL_CMD.format(sys=sys,name=app.config['APP_NAME'],logfile=app.config['PIP_INSTALL_LOGFILE'])

def fetch_available_versions(app):
    return fetch_gitlab_tags(app.config['GITLAB_PROJECT_ID'])

def init_app(app:flask.Flask):
    app.config.setdefault('PIP_INSTALL_LOGFILE',PIP_INSTALL_LOGFILE)
    add_commands(app)
