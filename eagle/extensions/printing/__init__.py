import flask
from blinker import signal

on_print = signal('on-print',doc="Sent when printing is requested")


try:
    import cups
except:
    cups = None

from .printing import bp, is_enabled

def init_app(app:flask.Flask):
    start_app(app)
    app.register_blueprint(bp)

def start_app(app:flask.Flask):
    if 'cups_connection' in app.extensions:
        return

    if not cups:
        app.logger.debug(f'pycups not installed. Disabling printing extension')
        return   
    printer = app.config.get('PRINTER_NAME', None)
    if printer == None:
        app.logger.debug(f'Config "PRINTER_NAME" not set. Disabling printing extension')
        return
    # Temporary hack to get around build, need to move it to start_app
    try:   
        conn = cups.Connection()
    except:
        app.logger.debug(f'Error connecting to cups. Disabling printing extension')
        return   
    
    printers = conn.getPrinters()
    if printer not in printers.keys():
        app.logger.debug(f'Printer {printer} not found in installed printers: {list(printers.keys())}. Disabling printing extension')
        return
    app.extensions[ 'cups_connection'] = conn




    
    

