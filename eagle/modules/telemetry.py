"""
Reports telemetry using the scheduler
"""

import flask
import psutil
import datetime
from blinker import signal

on_send = signal('on-send')

bp = flask.Blueprint("telemetry", __name__)

def collect_telemetry() -> dict:
    telemetry = {}
    exceptions = {}
    for callback in TELEMETRY_FUNCIONS:
        try:
            telemetry.update(callback())
        except Exception as e:
            exceptions[callback.__name__] = e
    return telemetry, exceptions


def send_telemetry(app=None) -> dict:
    """Send the application telemetry.  Collects all telemetry
    by calling funcs in TELEMETRY_FUNCTIONS array.  Then simply
    sends the `on_send` signal.  Actual telemetry must be sent
    by creating a handler for the `on_send` signal. 

    Args:
        app (Flask, optional): The app to send telemetry for. Defaults to None.

    Returns:
        dict: _description_
    """
    if not app and flask.has_app_context():
        app = flask.current_app
    
    if app:
        with app.app_context():
            telemetry,exceptions = collect_telemetry()
    else:
        telemetry,exceptions = collect_telemetry()

    on_send.send(app,**telemetry)
    return telemetry, exceptions

def heartbeats() -> dict:
    app = flask.current_app
    heatbeats = app.extensions.get('heartbeats')
    if not heatbeats:
        heatbeats = 0
    heatbeats += 1
    app.extensions['heartbeats'] = heatbeats
    return dict(heartbeats=heatbeats)

def get_battery_info() -> dict:
    battery_data = {}
    try:
        battery = psutil.sensors_battery()
        battery = battery._asdict()
        battery_data['battery_minsleft'] = battery['secsleft'] / 60
        battery_data['battery_percent'] = battery['percent']
        battery_data['battery_power'] = battery['power_plugged']
    except:
        return {}
    return battery_data

def get_temperature() -> dict:
    if not hasattr(psutil,'sensors_temperatures'):
        return {}
    temp_data = {}
    temps = psutil.sensors_temperatures().get('cpu_thermal')
    if temps and len(temps):
        temp = temps[0]._asdict().get('current')
        if temp:
            temp_data = dict(temperature=temp)
    return temp_data

TELEMETRY_FUNCIONS = [get_battery_info,get_temperature,heartbeats]

def boot_time():
    # returns the time in seconds since the epoch
    try:
        last_reboot = psutil.boot_time()
    except:
        return 'Unknown'
    # converting the date and time in readable format
    reboot_time = datetime.datetime.fromtimestamp(last_reboot)
    return reboot_time.strftime("%Y-%m-%d %H:%M:%S")

def current_time():
    now = datetime.datetime.now()
    return now.strftime("%Y-%m-%d %H:%M:%S")

def send_startup_telemetry(app):
    t = dict(
        last_start_time=current_time(),
        last_boot_time=boot_time()
    )
    app.logger.debug(f'sending startup telemetry: {t}')
    on_send.send(app,**t)


def init_app(app:flask.Flask):
    app.register_blueprint(bp)

def start_app(app:flask.Flask):

    send_startup_telemetry(app)



def main():
    from eagle.extensions.thingsboard.rpc import TBRpcClient

    from flask import Config
    from eagle.config import get_host_id
    import sys
    device_name = get_host_id()
    if len(sys.argv) > 1:
        device_name = sys.argv[1]

    config = Config('instance')
    if not config.from_envvar('EAGLE_CONFIG',silent=True):
        if not config.from_pyfile('globals.py',silent=True):
            print("EAGLE_CONFIG not set")

    client = TBRpcClient(username=config['TB_API_USERNAME'],password=config['TB_API_PASSWORD'],host=config['TB_API_URL'])
    device = client.get_device_id(device_name=device_name)
    if device:
        client._device_id = device['id']['id']
        data = client.get_telemetry(keys='cashin,cashout')
        sorted_data = sorted(data['cashin'] + data['cashout'],key=lambda row: row['ts'])
        print(sorted_data)

if __name__ == '__main__':
    main()