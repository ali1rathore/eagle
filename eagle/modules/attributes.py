from eagle.config import get_my_ip
import flask
def get_attributes(app):
    current_ip = get_my_ip()
    if current_ip:
        app.config['IP_ADDRESS'] = current_ip
        
    return dict(
        role=app.config['ROLE'],
        mac=app.config['MAC_ADDRESS'],
        host_id=app.config['HOST_ID'],
        hostname=app.config['HOSTNAME'],
        version=app.config['VERSION'],
        revision=app.config['COMMIT_ID'],
        ip_address=app.config['IP_ADDRESS'],
    )

def init_app(app:flask.Flask):

    app.add_template_global(lambda: get_attributes(app).items(),name='attributes')
