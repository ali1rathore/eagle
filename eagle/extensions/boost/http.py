from logging import Logger
from persistqueue import SQLiteAckQueue as PersistentQueue
import flask
from .errors import TaskFailed

class HTTPQueue(PersistentQueue):
    def __init__(self,queue_name,subscribers) -> None:
        dbpath = flask.current_app.config['DATABASE_DIR'] + '/' + queue_name
        super().__init__(dbpath,multithreading=True)
        self._queue_name = queue_name
        self._subscribers = subscribers
        self._registered = False
        self.logger = Logger(queue_name)

    def __repr__(self) -> str:
        return f"HTTPQueue('{self._queue_name}',{self.subscribers})"

    @property
    def subscribers(self):
        subs = self._subscribers() if callable(self._subscribers) else self._subscribers
        if not isinstance(subs,list):
            subs = [subs]
        return subs

    def put(self,msg,**kwargs):

        payload = {'pqid':1,'data':msg}
        import requests
        urls = self.subscribers
        for url in urls:
            try:
                resp = requests.post('http://'+url+f'/{self._queue_name}', json=payload)
                if resp.ok and resp.status_code == 200 and resp.text.startswith('OK'):
                    return payload
            except Exception as e:
                pass

        raise TaskFailed(f"Unable to notify any subscribers for {self._queue_name} {urls}")
    
    def get(self,*args,**kwargs):
        if (not flask.current_app.got_first_request 
            and not flask.current_app.url_map._rules_by_endpoint.get(self._queue_name)
            and not self._registered
            ):
            @flask.current_app.post(f'/{self._queue_name}',endpoint=self._queue_name)
            def handle():
                data = flask.request.json.get('data')
                id = super(HTTPQueue,self).put(data)
                #func = flask.current_app.extensions.get('boost').function(self._queue_name)
#                func._execute_next(id)
                flask.current_app.logger.debug(f"Recieved HTTP item '{id}' for {self._queue_name} from {flask.request.remote_addr}: {data}")
                return f'OK {id}', 200
            csrf = flask.current_app.extensions.get('csrf')
            if csrf:
                csrf.exempt(handle)
            self._registered = True

        g = super().get(*args,**kwargs)
        return g