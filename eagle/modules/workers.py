import flask 
from eagle.extensions import services, printing
from eagle.modules import transactions,  machines
from eagle.modules.transactions.model import Transaction
from eagle.extensions.boost import boost, MQTTQueue, HTTPQueue, PersistQueue
POS = '127.0.0.1:5040'
WORKER = '127.0.0.1:5050' 
POS = lambda: list(machines.remote_pos_addresses().values())
WORKER = lambda: list(machines.remote_machine_addresses().values())
def QUEUE(queue_name):
    return PersistQueue(flask.current_app.config['DATABASE_DIR'] + '/' + queue_name,multithreading=True)

def MQTT(queue_name):
    return MQTTQueue(queue_name=queue_name,host=flask.current_app.config['HOST'],port=1883)

def HTTP(queue_name,*args,**kwargs):
    return HTTPQueue(queue_name=queue_name,*args,**kwargs)

@boost('transaction',broker_kind=QUEUE)
def create_transaction(amount: int):
    with boost.session as session:
        t = Transaction(amount=amount,machine_id=flask.current_app.config['HOST_ID'])
        session.add(t)
    result = t.to_json()
    send_to_pos.push(result)
    return result

@boost('publish',broker_kind=QUEUE)
def send_to_pos(transaction: Transaction):
    write_to_db.push(transaction)
    return transaction

@boost('store',broker_kind=HTTP,subscribers = POS)
def write_to_db(transaction: Transaction):
    with boost.session as session:
        t = Transaction.from_dict(transaction=transaction)
        session.add(t)
    print_receipt.push(t.to_json())

@boost('print',broker_kind=QUEUE)
def print_receipt(transaction: Transaction):
    t = transactions.model.Transaction.from_dict(transaction=transaction)
    if t.amount < 0:
        text = transactions.view.render_receipt(t,app=flask.current_app)
        result = printing.on_print.send(flask.current_app._get_current_object(),text=text)
    confirm.push(transaction)
    return t

@boost('confirm',broker_kind=QUEUE)
def confirm(transaction:Transaction):
    archive_transaction.push(transaction)
    return transaction

@boost('delete',broker_kind=HTTP,subscribers=WORKER)
def archive_transaction(transaction:Transaction):
    t = transactions.model.Transaction.from_dict(transaction=transaction)
    with boost.session as session:
        exists = session.query(Transaction).filter(Transaction.id==t.id).first()
        if exists:
            session.delete(exists)
            flask.current_app.logger.debug(f"Delete transaction {exists}")
            return transaction
    return transaction

def init_app(app):

    if app.config.get('ENABLE_BOOST') != True:
        return

    def handle_stdout(line):
        value = create_transaction.push(line)
        t = value.get()
        app.logger.debug(f"Saved transaction {t}")

    cashin = services.current_services('cashin',app=app)
    cashout = services.current_services('cashout',app=app)
    for c in [cashin,cashout]:
        if c:
            c.on_stdout = handle_stdout
    return app

def start_app(app):

    if app.config.get('ENABLE_BOOST') != True:
        return

    role = app.config['ROLE']
    if role == 'POS':
        write_to_db.consume()  
        print_receipt.consume()
        confirm.consume()
    else:
        create_transaction.consume()
        send_to_pos.consume()
        archive_transaction.consume()

def stop_app(app):
    pass