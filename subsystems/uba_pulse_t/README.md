# UBA to Pulse driver

This executable wil listen to the serial port for UBA (id003) protocol and 
convert to GPIO pulses.


### Building

1. cd to this directory
2. run `make`

##### wiringPi

wiringPi files are copied directly from [https://github.com/WiringPi/WiringPi/tree/final_official_2.50](https://github.com/WiringPi/WiringPi/tree/final_official_2.50)

Tp copy these files again
* cp -r softTone.* wiringSerial.* softPwm.* piHiPri.c ~/Work/eagle/subsystems/wiringPi/


### Running

1. run `./uba_pulse database.db` to start listening to serial port
