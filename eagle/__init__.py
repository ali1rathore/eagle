import sys
SINGLE_PRORCESS = True

def run_flask_cli(create_app):
    if os.environ.get('FLASK_ENV') == 'development':    
        import click
        from flask.cli import FlaskGroup
        @click.group(cls=FlaskGroup, create_app=create_app)
        def cli():
            """Development script for EAGLE."""
        cli.main()

    if 'FLASK_APP' not in os.environ:
        os.environ['FLASK_APP'] = 'eagle.app'
    app = create_app()
    init_app(app)
    app.cli.main()

def main():
    version = os.environ.get('EAGLE_VERSION')
    if not version:
        from .app import create_app
        run_flask_cli(create_app=create_app)

    version_dir = os.environ.get('EAGLE_VERSIONS_DIR')
    if not version_dir:
        from .config import CONFIG_PATH
        version_dir = CONFIG_PATH / version

    if SINGLE_PRORCESS:
        for m in list(sys.modules.keys()):
            if m.startswith('eagle'):
                print(f"Deleting module {m}")
                del sys.modules[m]

        with virtual_env(version_dir) as venv:
            if not venv.exists():
                venv.create()
                venv.install(INSTALL_URL.format(version=version))
            assert venv.contains('eagle')
            from eagle.app import create_app
            run_flask_cli(create_app=create_app)

    
    v = VirtualEnv(version_dir)
    if not v.exists():
        v.create()
        v.install(INSTALL_URL.format(version=version))
    assert venv.contains('eagle')
    v.run(f'eagle {" ".join(sys.argv[1:])}')


from pathlib import Path
import shutil, os
import sys
import subprocess
import contextlib

INSTALL_URL='git+https://gitlab.com/lakeshorelabs/eagle.git@{version}'

class VirtualEnv:
    def __init__(self, path: Path) -> None:
        self._path : Path = Path(path)
        self._python = shutil.which('python')

    @property
    def python(self):
        if not self.exists():
            return self._python
        return (self._path / 'bin' / 'python').absolute()

    def create(self):
        rc = subprocess.Popen(f'{self.python} -m venv {self._path}'.split()).wait()
        if rc == 0:
            return True
        return False

    def exists(self):
        return self._path.exists()

    def run(self,cmd, *args):
        subprocess.Popen(f'{self.python.parent}/{cmd} {" ".join(args)}'.split()).wait()


    def contains(self,*packages):
        for p in packages:
            rc = subprocess.Popen(f'{self.python} -m pip show {p}'.split()).wait()
            if rc != 0:
                return False
        return True

    def install(self,*args,upgrade=False):
        subprocess.Popen(f'{self.python} -m pip install {"--upgrade" if upgrade else ""} {" ".join(args)}'.split()).wait()

    def uninstall(self,*args):
        subprocess.Popen(f'{self.python} -m pip uninstall {" ".join(args)}'.split()).wait()

    def activate(self):
        return active_this(self._path)

    def distroy(self):
        shutil.rmtree(self._path)

def run_in_env(venvdir,python_file, *args):
    # Path to the script that must run under the virtualenv
    python_bin = venvdir / "bin"/ "python"
    python_file_bin = venvdir / "bin"/ python_file
    subprocess.Popen([python_bin, python_file_bin, *args])

def active_this(base):
    base = str(Path(base).absolute())
    site_packages = os.path.join(base, 'lib', 'python%s' % sys.version[:3], 'site-packages')
    prev_sys_path = list(l for l in sys.path.copy())
    import site
    site.addsitedir(site_packages)
    sys.real_prefix = sys.prefix
    sys.prefix = base
    # Move the added items to the front of the path:
    new_sys_path = []
    for item in list(sys.path):
        if item not in prev_sys_path:
            new_sys_path.append(item)
            sys.path.remove(item)
    sys.path[:0] = new_sys_path
    return prev_sys_path, sys.real_prefix

@contextlib.contextmanager
def virtual_env(envdir):
    venv = VirtualEnv(envdir)
    old_sys_path, old_prefix = venv.activate()
    old_modules = sys.modules.copy()
    old_executable = sys.executable
    sys.executable = venv.python
    yield venv
    sys.executable = old_executable
    new_modules = []
    for m in  list(sys.modules.keys()):
        if m not in old_modules:
            del(sys.modules[m])
            new_modules.append(m)

    sys.path = old_sys_path
    sys.prefix = old_prefix




def init_app(app):
    import flask,click

    @app.cli.command('upgrade',help=f"Upgrade from {app.config['VERSION']} to the latest version")
    @click.argument('version')
    def upgrade(version=None):
        print(f'Upgrading {app.name} to version {version}')
        version_dir = Path(flask.current_app.instance_path) / app.name / version
        
        venv = VirtualEnv(version_dir)
        venv.create()
        venv.install(f'git+https://gitlab.com/lakeshorelabs/eagle.git@{version}')
