from .base import ConfigFileSession, ConfigViewBase, ConfigInterface, CurrentAppConfigSession
from .validators import ValueMustBe, MustBeAlphaNum
from flask_appbuilder import action, AppBuilder
import flask
from pathlib import Path

instance_config_session = ConfigFileSession(lambda : Path(flask.current_app.instance_path) / 'config.py')    
global_config_session = ConfigFileSession(lambda : Path(flask.current_app.config['EAGLE_CONFIG']))    

class InstanceConfigView(ConfigViewBase):

    list_title = 'Instance Config'
    datamodel = ConfigInterface(instance_config_session)
    
    hidden_keys = ['TB_DEVICE_TOKEN']
    validators_columns = {
        'value':[
            ValueMustBe('ROLE',lambda: flask.current_app.config['AVAILABLE_ROLES']),
            ValueMustBe('APP_THEME',lambda: flask.current_app.config['AVAILABLE_THEMES']),
            ValueMustBe('ENABLE_REMOTE_CREDITS',['True','False']),
            MustBeAlphaNum('HOSTNAME'),
        ],
    }
    def post_update(self, item):
        flask.current_app.config.from_pyfile(self.datamodel.session.filepath)
        return super().post_update(item)

    def post_edit_redirect(self):
        return flask.redirect(flask.url_for('server.display'))

class GlobalConfig(ConfigViewBase):
    list_title = 'Global Config'
    datamodel = ConfigInterface(global_config_session)

    def post_update(self, item):
        flask.current_app.config.from_pyfile(self.datamodel.session.filepath)
        return super().post_update(item)

class AppConfigView(ConfigViewBase):
    list_title = 'App Config'
    datamodel = ConfigInterface(CurrentAppConfigSession())
    base_permissions = ['can_list']

    @action('override',text="Override",confirmation="Override configs?", icon="fa-pencil",multiple=True,single=True)
    def override(self,items):
        if not isinstance(items,list):
            items = [items]

        flask.current_app.logger.debug(f"Overriding items: {items}")
        for i in items:
            InstanceConfigView.datamodel.edit(i)

        return flask.redirect(flask.url_for('InstanceConfigView.list'))


def init_app(app):
    appbuilder:AppBuilder = app.extensions.get('appbuilder')
    if not appbuilder:
        return
    appbuilder.add_view(
        InstanceConfigView, "Instance", icon="fa-home", category="Config",category_icon="fa-gears"
    )
    appbuilder.add_view(
        GlobalConfig, "Global", icon="fa-globe", category="Config"
    )
    appbuilder.add_view(
        AppConfigView, "All", icon="fa-circle", category="Config"
    )