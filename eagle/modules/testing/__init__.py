from contextlib import contextmanager
import blinker
from eagle.extensions.services import current_services
from collections.abc import Iterable
import flask
from .ba_genstream import generate_ba_bytes, generate_banila_bytes, generate_pog_cashout_bytes

bp = flask.Blueprint('testing',__name__)
SLEEP = 1
@bp.route('/cashin/<int:amount>')
def do_cashin(amount):    
    cashin = current_services('cashin')
    if not cashin:
        return 'No cashin process', 404

    ba_bytes = generate_ba_bytes(amount)
    written = cashin.write(ba_bytes)
    if not written:
        return f'Failed to cashin {amount}', 500
    return 'Succes',200

@bp.route('/cashout/<amount>')
def do_cashout(amount):    
    cashout = current_services('cashout')
    if not cashout:
        return 'No cashin process', 404
    role = flask.current_app.config['ROLE']
    written = 0
    if role == 'FIRELINK_TEST':
        written = cashout.write(f'{amount}\n')
    elif role == 'LOL_TEST':
        written = cashout.write(f'AMOUNT   ${amount}\n')
    elif role == 'LOL1_TEST':
        written = cashout.write(f'CREDIT:  {int(amount)*100}\n')
    elif role == 'BAN_TEST':
        ba_bytes = generate_banila_bytes(amount)
        written = cashout.write(ba_bytes)
    elif role == 'POG_TEST':
        pog_bytes = generate_pog_cashout_bytes(amount)
        written = cashout.write(pog_bytes)    
    else:
        written = cashout.write(amount)
    if not written:
        return f'Failed to cashout {amount}'
    return 'Succes',200


@contextmanager
def captured_signals(app,signals=None):

    if not signals:
        signals = blinker.signal.__self__.values()
    recorded = []
    if not isinstance(signals,Iterable):
        signals = [signals]
    for s in signals:

        def record(sender, *args,**kwargs):
            recorded.append((s, sender, args, kwargs))
        s.connect(record)
    try:
        yield recorded
    finally:
        s.disconnect(record, app)

def init_app(app:flask.Flask):
    if not app.testing:
        return

    app.register_blueprint(bp)
