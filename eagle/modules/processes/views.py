from flask_appbuilder.models import decorators
from flask_appbuilder import action, ModelView
import flask
from .signals import service_started, service_stopped
from flask_appbuilder.models.generic import GenericModel, GenericSession, GenericColumn, interface


class ServiceModel(GenericModel):
    name = GenericColumn(str)
    command = GenericColumn(str)
    thread = GenericColumn(str, primary_key=True)
    is_alive = GenericColumn(str)
    pid = GenericColumn(str)
    
    @decorators.renders('status')
    def status(self):
        # will render this columns as bold on ListWidget
        if self.is_alive:
            return flask.Markup("""
            <button type="button" class="btn disabled btn-primary">ON</button>
            """)
        else:
            return flask.Markup("""
            <button type="button" class="btn disabled btn-danger">OFF</button>
            """)

    def __repr__(self):
        return f"Machine(id={self.id},name='{self.name}',address='{self.address}')"

class ServiceSession(GenericSession):

    def all(self):
        self.delete_all(ServiceModel())
        services = flask.current_app.extensions['services']
        for name,service in services.items():
            model = ServiceModel(name=name,thread=service.name,command=str(service._command),is_alive=service.is_alive(),pid=service.pid)
            self.add(model)
        return super().all()

class ServicesView(ModelView):
    list_title = 'Services'
    datamodel = interface.GenericInterface(ServiceModel, ServiceSession())
    base_permissions = ['can_list']
    list_columns = ['name', 'status', 'command', 'thread','pid']

    @action("restart","Restart Service",confirmation="Restart these services?",icon="fa-arrow-up",single=True)
    def restart(self, item):
        """Shows rendered transaction(s)"""
        if not isinstance(item,list):
            item = [item]
        services = flask.current_app.extensions['services']
        for i in item:
            if i.name in services:
                services.restart(i.name)
                service_started.send(flask.current_app._get_current_object(),service=i)

        return self.list()

    @action("stop","Stop Service",confirmation="Stop these services?",icon="fa-arrow-down",single=True)
    def stop(self, item):
        """Shows rendered transaction(s)"""
        if not isinstance(item,list):
            item = [item]
        services = flask.current_app.extensions['services']
        for i in item:
            if i.name in services:
                services[i.name].kill()
                service_stopped.send(flask.current_app._get_current_object(),service=i)

        return self.list()

def init_app(app):
    appbuilder = app.extensions.get('appbuilder')
    if not appbuilder:
        return
    appbuilder.add_view(
        ServicesView, "Services", icon="fa-gears", category="Developer",category_icon='fa-edit'
    )