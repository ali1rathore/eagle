import os
import string
import tempfile

def check_printer_status(printer: string, conn):
    status_list = {3:'idle', 4:'printing', 5:'paused'}
    printers = conn.getPrinters()
    if printer not in printers:
        return None
    mp = printers[printer]['printer-state']
    return status_list[mp]

def send_to_printer(printer: string, text: string, conn):
    with tempfile.NamedTemporaryFile(mode='w', delete=False) as temp:
         temp.write(text)
         temp.close()
         conn.printFile(printer, temp.name, 'reciept', {})
         os.unlink(temp.name)