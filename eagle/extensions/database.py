import flask
import sqlite3
import pathlib

def get_db():
    db = getattr(flask.g, "_database", None)
    if db is None:
        DATABASE = flask.current_app.config["DATABASE"]
        con = sqlite3.connect(DATABASE)
        flask.g._database = con
        db = flask.g._database
    return db


def init_app(app: flask.Flask):

    app.extensions["database"] = get_db
    db_path = app.config['DATABASE']

    db_path = pathlib.Path(db_path)
    if not db_path.exists():
        db_path.parent.mkdir(parents=True, exist_ok=True)
        
    with app.app_context():
        init_db()

    @app.cli.command("init-db")
    def init_db_cli():
        """
        Create the database and tables
        """
        with app.app_context():
            init_db()

def init_db():
    db = get_db()
    cursor = db.cursor()
    statements = CREATE_RECORD_DB.split(";")
    for s in statements:
        cursor.execute(s)
    db.commit()


CREATE_RECORD_DB = """

CREATE TABLE IF NOT EXISTS ledger (
   id INTEGER PRIMARY KEY AUTOINCREMENT,
   amount INTEGER NOT NULL,
   timestamp INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS reset_count (
   reset_count_id INTEGER PRIMARY KEY AUTOINCREMENT,
   reset_time INTEGER NOT NULL
);
INSERT or ignore into reset_count (reset_count_id, reset_time) values (0,1);

CREATE TABLE IF NOT EXISTS reset_period (
   reset_period_id INTEGER PRIMARY KEY AUTOINCREMENT,
   reset_period_time INTEGER NOT NULL
);
insert or ignore into reset_period (reset_period_id, reset_period_time) values  (0,0);


"""

