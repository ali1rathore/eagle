from eagle.modules.transactions.view import TransactionGeneralView
from eagle.modules.reports.model import Report
from flask_appbuilder.models.sqla.filters import FilterSmaller, BaseFilter, get_field_setup_query
from flask_babel import lazy_gettext

class FilterGreaterFunction(BaseFilter):
    name = "Filter view with a greater than function"
    arg_name = "gtf"

    def apply(self, query, func):
        query, field = get_field_setup_query(query, self.model, self.column_name)
        return query.filter(field > func())


class CashoutView(TransactionGeneralView):
    route_base = '/cashout'
    list_title = 'Cashouts for this Period'
    base_filters = [
        ['amount', FilterSmaller, 0],
        ['created_at', FilterGreaterFunction, Report.last_report_time]
    ]

def init_app(app):
    appbuilder = app.extensions.get('appbuilder')
    if not appbuilder:
        return
    appbuilder.add_view(CashoutView, "Cashouts", icon="fa-bank")
