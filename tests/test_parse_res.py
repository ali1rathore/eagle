import pytest
import re
from eagle.extensions.kiosk import parse_resolution

def test_parse_resolution():
    res_string = 'U:1600x1200p-0'
    resolution = parse_resolution(res_string)
    assert resolution == '1600,1200'
