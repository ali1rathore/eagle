from flask_appbuilder import  action, AppBuilder, expose, has_access
import flask
from flask_appbuilder.baseviews import expose_api
from flask_appbuilder.models.sqla.interface import SQLAInterface
from flask_appbuilder import ModelRestApi
from eagle.modules.transactions.model import Transaction
from eagle import modules
from blinker import signal

transactions_fetched = signal('transaction-fetched',"A transaction was fetched from a remote machine.")


def fetch_transactions():
    last_transaction_times = modules.transactions.most_recent_transactions()
    hostnames = modules.machines.remote_machine_addresses()
    requests = [('GET',f'http://{hostnames[machine_id]}/api/v1/transactions/{last_transaction_times.get(machine_id,"")}') for machine_id in hostnames]
    responses = (r for r in generate_responses(requests))
    transactions = []
    failed_requests = []
    for r in responses:
        if isinstance(r, ConnectionError):
            flask.current_app.logger.warning(f"Error Connecting {r}")
            failed_requests.append(r.request)
            if flask.has_request_context():
                flask.flash(f"Unable to connect to {r.request.url})",category="danger")        
        elif isinstance(r,Exception):
            flask.current_app.logger.warning(f"Error fetching {r}")
            failed_requests.append(r.request)
            if flask.has_request_context():
                flask.flash(f"Error fetching {r})",category="danger")
        elif r.status_code == 200:
            json_response = r.json()
            for index,id in enumerate(json_response['ids']):
                row = json_response['result'][index]
                transaction = dict(id=id,amount=row['amount'],machine_id=row['machine_id'],created_at=row['created_at'])
                transactions.append(transaction)
            if flask.has_request_context():
                flask.flash(f"Fetched {len(json_response['ids'])} transactions from {r.url}",category="success")
        else:
            flask.current_app.logger.warning(f"Failed fetching {r.url}: {r}")
            failed_requests.append(r.request)
            if flask.has_request_context():
                flask.flash(f"Failed fetching {r.url}: {r}: {r.reason} ({r.text})",category="danger")

    return transactions

def generate_responses(requests_generator):
    from requests_futures.sessions import FuturesSession
    import concurrent.futures

    def response_hook(resp, *args, **kwargs):
        resp = resp.text

    responses = []
        
    try:
        with concurrent.futures.ThreadPoolExecutor() as executor:
            session = FuturesSession(executor=executor)
            
            futures = (session.request(verb,url,hooks={
                'response':response_hook
            },timeout=10) for verb,url in requests_generator)

            for fut in concurrent.futures.as_completed(futures,timeout=None):
                r = None
                try:
                    r = fut.result()
                except Exception as e:
                    r = e
                responses.append(r)
    except Exception as e:
        flask.current_app.logger.error(f"Failed durring concurrent fetch: {e}")
    return responses


class TransactionModelApi(ModelRestApi):
    resource_name = 'transactions'
    #base_permissions = ['get']
    page_size = 10000
    allow_browser_login = True
    datamodel = SQLAInterface(Transaction)
    exclude_route_methods = ("put", "post", "delete", "info","get")
    base_order = ('created_at','desc')
    
    @expose_api(name='list_all',url='/',methods=["GET"],description="Get all transactions")
    @expose_api(name='list_since',url='/<since>',methods=["GET"],description="Get all transactions after 'since'")
    def get_list(self,since=None,**kwargs):
        if since:
            kwargs['rison'] = {
                "filters": [
                    {
                        "col": 'created_at',
                        "opr": "gt",
                        "value": since
                    },
                    ]
            }
        list = self.get_list_headless(**kwargs)
        ids = list.json['ids']
        if ids:
            flask.current_app.logger.debug(f"Returning Transactions to {flask.request.remote_addr} {ids}")
        return list

    @expose('/cashouts',methods=["GET"])
    def cashouts(self,**kwargs):
        kwargs['rison'] = {
                "filters": [
                    {
                        "col": 'amount',
                        "opr": "lt",
                        "value": 0
                    },
        ]
        }
        return self.get_list_headless(**kwargs)


    @expose('/cashins',methods=["GET"])
    def cashins(self,**kwargs):
        kwargs['rison'] = {
                "filters": [
                    {
                        "col": 'amount',
                        "opr": "gt",
                        "value": 0
                    },
        ]
        }
        return self.get_list_headless(**kwargs)

def init_app(app):
    appbuilder = app.extensions.get('appbuilder')
    if not appbuilder:
        return
    if app.config['ROLE'] != 'POS':
        appbuilder.add_api(TransactionModelApi)