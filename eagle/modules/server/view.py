from flask_appbuilder import BaseView, expose, ModelView
from flask_appbuilder.models.sqla.interface import SQLAInterface
import flask
from eagle.extensions import update
from .model import LogMessage
from .signals import shutdown_requested
from pathlib import Path


TEMPLATES = Path(__file__).parent / 'templates'

class Server(BaseView):
    route_base = '/server'
    template_folder = TEMPLATES
    
    @expose('/shutdown')
    def shutdown(self):
        shutdown_requested.send(flask.current_app._get_current_object())
        nexturl = flask.request.referrer or '/'
        return flask.redirect(nexturl)

    @expose('/display',methods=['GET'])
    def display(self):
        self.update_redirect()
        return flask.render_template('onboard.html')


    @expose('/update/<version>',methods=['POST'])
    def update(self,version=None):
        tags = update.fetch_available_versions(flask.current_app)
        versions = [t['name'] for t in tags]
        if version not in versions:
            flask.flash(f"Version {version} not available, select again.")
        else:
            flask.flash(f"Starting upgrade to version {version}")
            update.update_requested.send(flask.current_app._get_current_object(),version=version)
        return self.render_template('updates.html',tags=tags)
    
    @expose('/update',methods=['GET'])
    def list_updates(self):
        self.update_redirect()
        tags = update.fetch_available_versions(flask.current_app)
        return flask.render_template('updates.html',tags=tags)


class LogView(ModelView):
    route_base = '/logs'
    list_title = "Log Messages"
    base_permissions = ['can_list']
    datamodel = SQLAInterface(LogMessage)
    list_columns = ["created_at","level", "msg", "path", "function","thread","name"]
    base_order = ('created_at','desc')
    exclude_route_methods = ("put", "post", "delete", "info","get")

def init_app(app):

    appbuilder = app.extensions.get('appbuilder')
    if appbuilder:
        appbuilder.add_view_no_menu(Server(), "server",)
        appbuilder.add_view(LogView, "Logs", icon="fa-book",category="Developer")
        appbuilder.add_link("REBOOT",href='server.shutdown',icon='fa-power-off',category="Developer")
