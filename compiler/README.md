## Compiler helper script

This script will build the binaries from specified based directory and will place the built binaries into the destination directory.

### Usage
````bash
compile.sh <base director> <destination directory>
````

This script is used by docker-compose but can be used by itself. For example to compile it locally on a mac:
````bash
./compile.sh ../subsystems ../eagle/bin/Darwin
````