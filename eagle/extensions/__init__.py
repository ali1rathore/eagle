from . import (
    appbuilder,
    database,
    sentry,
    systemd,
    thingsboard,
    wifi,
    sse,
    package,
    startup,
    debugger,
    theme,
    services,
    zeroconf,
    printing,
    logger,
    executor,
    litestream,
    installer,
    update,
    signals,
    endpoints,
    grapesjs,
    features,
    terminal,
    boost,
    kiosk
)

from flask_htmx import HTMX
from flask_wtf.csrf import CSRFProtect, CSRFError

import flask

htmx = HTMX()
csrf = CSRFProtect()

def init_app(app:flask.Flask):
    logger.init_app(app)
    appbuilder.init_app(app)
    database.init_app(app)
    services.init_app(app)
    sentry.init_app(app)
    systemd.init_app(app)
    thingsboard.init_app(app)
    wifi.init_app(app)
    sse.init_app(app)
    package.init_app(app)
    startup.init_app(app)
    debugger.init_app(app)
    theme.init_app(app)
    zeroconf.init_app(app)
    printing.init_app(app)
    executor.init_app(app)
    litestream.init_app(app)
    features.init_app(app)
    terminal.init_app(app)
    htmx.init_app(app)
    app.context_processor(lambda: dict(htmx=flask.current_app.htmx))
    csrf.init_app(app)
    kiosk.init_app(app)

    @app.errorhandler(CSRFError)
    def handle_csrf_error(e):
        flask.current_app.logger.warning(f"CSRF Token is missing for {flask.request.url}")
        return flask.render_template('base.html', content=e.description), 400

    @app.errorhandler(404)
    def handle_404_error(e):
        if flask.request.url.endswith('favicon.ico'):
            return
        flask.flash(f"Page not found: {flask.request.url}",category='warning')
        return flask.redirect(flask.request.referrer or '/',code=404)
        
    installer.init_app(app)
    update.init_app(app)
    signals.init_app(app)
    endpoints.init_app(app)
    grapesjs.init_app(app)
    boost.init_app(app)