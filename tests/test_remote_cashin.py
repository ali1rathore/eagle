from eagle.modules import testing
from eagle.models import  db, append_transaction_to_db
from eagle.server import  send_pulses
from eagle.modules.credits import add_credits_requested
from eagle.modules.transactions.model import Transaction
from time import sleep
import pytest

def test_send_add_cashin_signal(create_app):

    app = create_app(ROLE="FIRELINK_TEST")
    add_credits_requested.connect(send_pulses)
    add_credits_requested.connect(append_transaction_to_db)
    add_credits_requested.send(app,amount=20)
    t = db.session.query(Transaction).order_by(Transaction.created_at.desc()).first()
    assert t and t.amount == 20