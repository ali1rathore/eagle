import string
import flask
import time
from .print_functions import check_printer_status, send_to_printer

bp = flask.Blueprint('printing',__name__,static_folder='static',template_folder='templates')

def e_check_printer_status(app: flask.Flask, conn=None):
    if conn == None:
        conn = app.extensions.get('cups_connection', None)
        if conn == None:
            raise RuntimeError('no cups connection')
    printer = app.config['PRINTER_NAME']
    return check_printer_status(printer, conn)

def is_enabled(app):

    conn = app.extensions.get('cups_connection')
    if not conn:
        app.logger.warn(f"Not printing since cups extension is not configured")
        return False
    printer = app.config.get('PRINTER_NAME')
    if not printer:
        app.logger.warn(f"Not printing since printer is not configured")
        return False

    return True

def e_send_to_printer(app: flask.Flask, text: string):

    if not is_enabled(app):
        return
        
    try:
        send_to_printer(app.config.get('PRINTER_NAME'), text, app.extensions.get('cups_connection'))
    except Exception as e:
        app.logger.error(f"Failed to print: {e}")



def __main__():
    pass