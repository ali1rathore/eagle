from flask_appbuilder import ModelView as BaseModelView, expose, has_access
import flask
from datetime import timezone, datetime
from dateutil import parser

def utc_to_local(utc_dt):

    if utc_dt == datetime.min:
        return "Beginning"
    if not utc_dt:
        return utc_dt

    if isinstance(utc_dt,str):
        utc_dt = parser.parse(utc_dt)
        
    t =  utc_dt.replace(tzinfo=timezone.utc).astimezone(tz=None)
    fmt = flask.current_app.config['TIME_FORMAT']
    if fmt:
        t = t.strftime(fmt)
    return t
def cents_to_dollars(cents,positive=False):
    sign = "-" if cents < 0 and not positive else ''
    dollars = '{}${:,.2f}'.format(sign,abs(cents)/100)
    return dollars

def show_role(role):
    # will render this columns as bold on ListWidget
    return flask.Markup('<b>' + role + '</b>')


def machine_name(machine):
    n = f"{machine}"
    return n

class ModelView(BaseModelView):
    formatters_columns={'created_at':utc_to_local,"updated_at":utc_to_local,"amount":cents_to_dollars,'machine':machine_name,'role':show_role}
    
class HTMXView:

    def __init_subclass__(cls) -> None:
        if 'can_display' not in cls.base_permissions:
            cls.base_permissions.append('can_display')

    @expose('/',methods=['GET'])
    @has_access
    def display(self,**kwargs):
        from flask_appbuilder.urltools import get_filter_args
        get_filter_args(self._filters)
        joined_filters = self._filters.get_joined_filters(self._base_filters)
        order_column, order_direction = self.base_order if self.base_order else ('','')
        count, lst = self.datamodel.query(joined_filters, order_column, order_direction)
        cols = [self.datamodel.get_pk_name()] + self.list_columns
        items = self.datamodel.get_values(lst, cols)       
        self.update_redirect() 
        return self.render_template('htmx_list.html',title=self.title,count=count,items=items,view=self)


def init_app(app):

    app.add_template_filter(cents_to_dollars)
    app.add_template_filter(utc_to_local)
