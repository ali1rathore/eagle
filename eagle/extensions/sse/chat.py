import flask

bp = flask.Blueprint('chat',__name__, static_folder='static', static_url_path='')

import html as cgi
from werkzeug.local import LocalProxy
current_chat = LocalProxy(lambda: flask.current_app._get_current_object().extensions['sse_chat'])
from .sse import Publisher

@bp.route('/publish', methods=['POST'])
def publish():
    sender_username = flask.request.form['username']
    chat_message = flask.request.form['message']

    template = '<strong>{}</strong>: {}'
    full_message = template.format(cgi.escape(sender_username),
                                    cgi.escape(chat_message))

    def m(subscriber_username):
        if subscriber_username != sender_username:
            return full_message
    current_chat.publish(m)

    return ''

@bp.route('/subscribe')
def subscribe():
    username = flask.request.args.get('username')
    return flask.Response(current_chat.subscribe(properties=username),
                            content_type='text/event-stream')

@bp.route('/')
def root():
    return flask.render_template('sse/chat.html')

def init_app(app:flask.Flask):
    app.register_blueprint(bp,url_prefix='/chat')
    chat_publisher = Publisher()
    app.extensions['sse_chat'] = chat_publisher


if __name__ == '__main__':
    # Starts an example chat application.
    # Run this module and point your browser to http://localhost:5000

    app = flask.Flask(__name__)
    init_app(app)
    app.run(debug=True, threaded=True)
