import pytest
import time
from eagle.modules.testing import captured_signals
from eagle.extensions.services import current_services, start_app, stop_app
from eagle.modules.testing.ba_genstream import generate_ba_bytes, generate_pog_cashout_bytes, generate_banila_bytes

SLEEP = 1

def test_stdinput_of_dummy_checkin_process_is_available(create_app):
    app = create_app(ROLE='TEST')
    start_app(app)
    time.sleep(SLEEP)
    cashin = current_services('cashin')
    assert cashin.stdin is not None
    stop_app(app)

def test_write_to_cat_process(create_app):

    app = create_app(ROLE='TEST')
    service = current_services()['cashout']
    service.start()
    cap = [service.stdout_event]
    with captured_signals(app, cap) as sigs:
        service.write(200)
        time.sleep(1)
        service.kill()
        assert len(sigs) >= 1
        assert sigs[0][3]['line'] == '200'
        assert sigs[0][1] == service
        assert sigs[0][0] == service.stdout_event 


@pytest.mark.parametrize("inp, outp", [
    (1, "100"),
    (5, "500"),
    (10, "1000"),
    (20, "2000"),
    (50, "5000"),
    (100, "10000")
])
def test_write_to_uba_pulse_process(create_app, inp, outp):

    app = create_app(ROLE='POG_TEST')
    cashin = current_services('cashin')
    cashin.start()
    cap = [cashin.stdout_event]
    with captured_signals(app, cap) as sigs:
        ba_bytes = generate_ba_bytes(inp)
        cashin.write(ba_bytes)
        time.sleep(SLEEP)
        cashin.stop(wait=False)
        time.sleep(SLEEP)
        assert len(sigs) >= 1
        assert sigs[0][3]['line'] == outp
    cashin.kill(wait=False)

@pytest.mark.parametrize("inp, outp", [
    ('1\n', '-100'),
    ('5\n', '-500'),
    ('10\n', '-1000'),
    ('20\n', '-2000'),
    ('50\n', '-5000')
])
def test_write_to_fish_print_process(create_app,inp,outp):

    app = create_app(ROLE='FIRELINK_TEST')
    cashout = current_services('cashout')
    cap = [cashout.stdout_event]
    cashout.start()
    with captured_signals(app, cap) as sigs:
        cashout.write(inp)
        time.sleep(2)
        assert len(sigs) >= 1
        assert sigs[0][3]['line'] == outp

    cashout.kill(force=False)

@pytest.mark.parametrize("inp, outp", [
    ("1", "-100"),
    ("5", "-500"),
    ("10", "-1000"),
    ("20", "-2000"),
    ("50", "-5000")
])
def test_write_to_ban_cashout_process(create_app,inp, outp):

    app = create_app(ROLE='BAN_TEST')
    cashout = current_services('cashout')
    cashout.start()
    cap = [cashout.stdout_event]

    with captured_signals(app, cap) as sigs:
        ba_bytes = generate_banila_bytes(inp)
        cashout.write(ba_bytes)
        time.sleep(SLEEP)
        assert len(sigs) >= 1
        assert sigs[0][3]['line'] == outp

    cashout.stop(wait=False)
    cashout.kill(force=False)

def test_write_to_pog_cashout_process(create_app):

    app = create_app(ROLE='POG_TEST')
    cashout = current_services('cashout')
    cashout.start()
    cap = [cashout.stdout_event]

    with captured_signals(app, cap) as sigs:
        pog_bytes = generate_pog_cashout_bytes("5.45")
        cashout.write(pog_bytes)
        time.sleep(2)
        assert len(sigs) >= 1
        assert sigs[0][3]['line'] == '-545'

    cashout.kill(force=False)