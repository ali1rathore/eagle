from flask_appbuilder import AppBuilder, IndexView, SQLA 
from flask_appbuilder.models.sqla import CustomSignallingSession,orm, Model
from flask_appbuilder.models.mixins import BaseMixin
from sqlalchemy import create_engine
import flask
from . import baseviews
from .baseviews import ModelView, cents_to_dollars, utc_to_local, HTMXView

bp = flask.Blueprint('fabtemplates',__name__,template_folder='templates')

class TemplateIndexView(IndexView):

    def __init__(self,template):
        self.index_template = template
        super().__init__()

from sqlalchemy.pool import StaticPool

db = SQLA(engine_options={'poolclass':StaticPool})

def init_app(app:flask.Flask):

    app.register_blueprint(bp)

    indexview = TemplateIndexView(app.config.get('INDEX_TEMPLATE','index.html'))
    db.init_app(app)
    appbuilder = AppBuilder(base_template=app.config['FAB_BASE_TEMPLATE'],indexview=indexview,session=db.session)
    db.create_all()
    if app.env == 'development':
        app.config.setdefault('FAB_API_SWAGGER_UI',True)

    app.add_template_global(appbuilder,'appbuilder')

    appbuilder.init_app(app,session=db.session)
    appbuilder.add_link('Home',href='TemplateIndexView.index',label='Home',icon='fa-home')
    appbuilder.add_link('Back',href='UtilView.back',icon = "fa-arrow-left")
    appbuilder.add_link('Kiosk',href='kiosk',icon = "fa-tablet")
    appbuilder.add_link('Onboard',href='server.display',icon = "fa-home",category="Developer")

    if app.env == 'development':
        appbuilder.add_link('API',href='/swagger/v1',icon = "fa-arrow-up",category="Developer")

    baseviews.init_app(app)

    if not default_users_exist(app):
        app.before_first_request(lambda: create_default_users(app))


def add_user(app,username,password,roles):
    appbuilder = app.extensions.get('appbuilder')
    if not appbuilder or not appbuilder.sm:
        return
    user = appbuilder.sm.find_user(username=username)
    if user:
        return
    roles =  [appbuilder.sm.find_role(role) for role in roles]
    user = appbuilder.sm.add_user(
        username, username, username, username+'@'+app.name+'.com', roles, password
    )

def default_users_exist(app):
    appbuilder = app.extensions.get('appbuilder')
    if not appbuilder or not appbuilder.sm:
        return
    users = [appbuilder.sm.find_user(username=config_user.split(':')[0]) for config_user in app.config.get('DEFAULT_USERS',[])]
    return all(users)

def create_default_users(app:flask.Flask):

    for config_user in app.config.get('DEFAULT_USERS',[]):
        username,password,roles = config_user.split(':')
        add_user(app,username=username,password=password,roles=roles.split(','))
