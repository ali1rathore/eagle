#!/bin/bash

set -e -x

# Handle command line argument
# $1: Directory with code
# $2: Diroctory to place compiled binaries
# $3: "CROSS_COMPILE" optional to trigger arm cross-build

if [ "$#" -lt 2 ]; then
    echo "Usage: $0 code_dir bin_dir"
#    exit -1
fi

if [ "$#" -gt 2 ] && [ $3 = "CROSS_COMPILE" ]; then
    export CROSS_COMPILE=1
fi

# Get OS
OS=$(uname -s) 
echo OS = $OS

# Make each subsystem
SUBSYSTEMS="wiringPi $(ls $1|grep -v Makefiles)"
BUILD_DIR=${2:-build}

cd $1

for s in $SUBSYSTEMS; do
    pushd $1/$s
    make clean
    make -f Makefile
    popd
done

# Cleanup .o files
find . -type f -name '*.o' -delete

# Copy executables to the build directory
if [ $OS = "Darwin" ]
then
    EXECUTABLES=`find $1 -type f -exec hexdump -n 4 -e '4/1 "%2x" " {}\n"'  {} \; | grep ^cffaedfe | awk '{print $2}'`
else
    EXECUTABLES=`find $1 -type f -exec hexdump -n 4 -e '4/1 "%2x" " {}\n"'  {} \; | grep ^7f454c46 | awk '{print $2}'`
fi
mkdir -p $BUILD_DIR || true

for e in $EXECUTABLES; do
    mv $e $BUILD_DIR
done