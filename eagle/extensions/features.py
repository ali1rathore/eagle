
from flask import Flask, current_app
try:
    from flask_unleash import Unleash
    unleash = Unleash()
except:
    unleash = None

def is_enabled(feature):
    context = {
        'userId': current_app.config['HOST_ID']
    }
    return unleash.client.is_enabled(feature,context=context)

def init_app(app: Flask):
    if not unleash or not app.config.get("ENABLE_UNLEASH",False) or not app.config.get("UNLEASH_URL"):
        return

    app.config["UNLEASH_APP_NAME"] = app.env
    app.config['UNLEASH_ENVIRONMENT'] = app.env
    unleash.init_app(app)