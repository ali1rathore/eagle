from requests.exceptions import ConnectionError
import flask
from blinker import signal
from eagle.extensions.appbuilder import db

transaction_stored = signal('transaction-stored',doc="A Transaction was stored in the db")

def write_transaction_to_db(app:flask.Flask,transactions):
    from eagle import modules
    if not isinstance(transactions,list):
        transactions = [transactions]
    for transaction in transactions:
        t = modules.transactions.model.Transaction.from_dict(transaction=transaction) 
        insert_db(t)
        transaction_stored.send(app,transaction=t)
    
def append_transaction_to_db(app:flask.Flask,amount,machine=None,**kwargs):
    from eagle import modules

    """Writes stout to db"""
    if machine and machine != app.config['HOST_ID']:
        app.logger.error(f"Attempted to append to transaction for machine {machine}, but this one is {app.config['HOST_ID']}")
        return
    t = modules.transactions.model.Transaction(amount=amount,machine_id=app.config['HOST_ID'])
    with app.app_context():
        t = insert_db(t)
        transaction_stored.send(app,transaction=t)
    return t
    
def init_write_queue(app):
    import queue
    app.extensions['writer'] = queue.Queue()

import threading
lock = threading.Lock()

def insert_db(record,delete=False):
    with lock:
        _commit_records(record,delete=delete)
    #flask.current_app.extensions['writer'].put((record,not delete),)
    return record
def write_database_forever(app):
    queue = app.extensions['writer']
    while True:
        try:
            records, insert = queue.get()
            if records:
                _commit_records(records=records,delete=not insert,session=db.session)
        except Exception as e:
            app.logger.error(f"Error writing to database {e}")

commited_event = signal('db-commited')

def _commit_records(records,delete=False,session=None):
    if not isinstance(records,list):
        records = [records]
    if not session:
        session = db.session
    
    jsons = []
    for r in records:
        jsons.append({r.__class__.__name__:r.to_json(),'insert':not delete})

    error = None
    action = 'INSERT'
    try:
        if delete:
            action = 'DELETE'
            for record in records:
                session.delete(record)
        else:
            for record in records:
                merged_record = session.merge(record)
                session.add(merged_record)
        session.commit()
    except Exception as e:
        error = e
        session.rollback()
    finally:
        pass #session.close()

    if error:
        flask.current_app.logger.debug(f'[FAILURE-{action}] {error} : {records}')
    else:
        flask.current_app.logger.debug(f'[{action}] {records}')
        commited_event.send(flask.current_app._get_current_object(),records=records)
    
    return not error
    

def init_app(app:flask.Flask):
    
    app.context_processor(lambda: dict(q=db.session.query))
    init_write_queue(app)    