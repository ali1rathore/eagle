from flask_appbuilder import BaseView, has_access, expose,AppBuilder
from eagle.extensions import litestream, gateway
from pathlib import Path
import flask
def create_replica_appspec(host_id,callable=True):
    replicas_dir = Path(flask.current_app.config['REPLICAS_DIR'])
    secret_key = flask.current_app.secret_key
    instance_path = replicas_dir / host_id
    app = f'eagle.app:create_app(instance_path="{instance_path}",DEBUG=False,HOST_ID="{host_id}",APP_THEME="cerulean.css")'
    if callable:
        from eagle.app import create_app
        app = lambda: create_app(SECRET_KEY=secret_key,REPLICAS_ENABLED=False,CSTORES_ENABLED=False,ENV='replica',APP_NAME=f"eagle-{host_id}",instance_path=instance_path,DEBUG=False,HOST_ID=host_id,APP_THEME="cerulean.css")
    return app

class ReplicaView(BaseView):
    route_base = '/db'

    @has_access
    @expose('/<name>')
    @expose('/<name>/<path:path>')
    def set(self,name,path=''):
        replicas = litestream.fetch_databases()
        litestream.create_replicas_config(flask.current_app,replicas=replicas)

        if name not in replicas:
            flask.flash(f"No replica named {name}",category='danger')
            return self.render_template('replicas.html',replicas=replicas), 404

        for db in ['TRANSACTIONS_DB','MACHINES_DB','REPORTS_DB','USERS_DB']:
            dbname = flask.current_app.config[db].name
            replica = name+'/'+dbname
            error = litestream.run_restore_command(flask.current_app,replica=replica)
            if error:
                flask.flash(f"Failed to restore: {replica}: {error}",category='danger')
                return self.render_template('replicas.html',replicas=replicas)
        
        gateway.current_gateway.mounts[f"/replica/{name}"] = create_replica_appspec(name)
        return flask.redirect(f'/replica/{name}/{path}')

    
    @has_access
    @expose('/')
    def list(self,):
        replicas = litestream.fetch_databases()
        return self.render_template('replicas.html',replicas=replicas)

def init_app(app:flask.Flask):
    app.config.setdefault('REPLICAS_DIR',Path(app.instance_path) / 'replicas')
    enabled = app.config.setdefault('REPLICAS_ENABLED',True)

    appbuilder: AppBuilder = app.extensions.get('appbuilder')
    if appbuilder and enabled:
        appbuilder.add_view(
            ReplicaView, "Replicas", icon="fa-fast-backward"
        )
        gateway.init_app(app)