
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <pthread.h>
#include <string.h>
#include <sqlite3.h>
#include <ctype.h>

#include <sys/time.h>
#include <time.h>

#include <wiringSerial.h>
#include <database.h>

#define DEVICE "/dev/ttyS0"
int s;
int sout;
//        FILE *fp;
unsigned char RxData;
unsigned char status = 0x3e;

void delay_ms(unsigned int howLong);

int amount;
unsigned char buf[128];
enum mode_list
{
    testing = 2,
    production = 0
};

int parse_stdin_args(char *argv[])
{
    return atoi(argv[2]);
}

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        fprintf(stderr,
                "Usage: %s DATABASE mode\nwhere mode is:\n\t(0=fish_print, 2=test)\n", argv[0]);
        exit(-1);
    }

    char *DATABASE_NAME = parse_db_args(argv);
    int rc = init_db(DATABASE_NAME);
    int mode = parse_stdin_args(argv);

    if (mode == production)
    {
        if ((s = serialOpen(DEVICE, 9600)) == -1)
        {
            fprintf(stderr, "Cannot open %s\n", DEVICE);
            exit(-1);
        }
        sout = s;
    }
    else{
        s = 0;
        sout = open("/dev/null", O_WRONLY);
    }

    fprintf(stderr, "Driver For Ithaca 750 Emulation\n");
    int sync = 0;
    int escape = 0;
    int count = 0;
    while (1)
    {
        usleep(10);
        if (serialDataAvail(s) >= 1)
        {
            RxData = (unsigned char)serialGetchar(s);
            //                fprintf(stderr, "Got %x\n", RxData);
            if ((RxData == 0x1d) && (sync == 0))
            {
                sync = 1;
                continue;
            }
            else if (sync != 0)
            {
                sync = 0;
                switch (RxData)
                {
                case 0x7a:
                    serialPutchar(sout, status);
                    status = 0x3e;
                }
                continue;
            }
            if ((RxData == 0x1b) && (escape == 0))
            {
                escape = 1;
                continue;
            }
            else if (escape != 0)
            {
                escape = 0;
                switch (RxData)
                {
                case 0x2a:
                    fprintf(stderr, "reset\n");
                    status = 0x0e;
                }
                continue;
            }
            if (isprint(RxData))
            {
                buf[count] = RxData;
                count++;
            }
            if (RxData == 0x0a)
            {
                buf[count] = 0;
                count = 0;
                if (strstr((char *)buf, "AMOUNT"))
                {
                    float tmp_amt;
                    tmp_amt = atof(strchr((char *)buf, '$') + 1) * 100;
                    amount = (int)tmp_amt * -1;
                    process_cashout(amount);
                }
            }
        }
    }

    return 0;
}

