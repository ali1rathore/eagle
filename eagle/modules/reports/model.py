from sqlalchemy import Column, String, DateTime, func, orm
from flask_appbuilder.models.sqla import Model
from flask_appbuilder.models.mixins import BaseMixin
import uuid
import datetime
from eagle.modules.transactions.model import Transaction
from eagle.extensions.appbuilder import db
from eagle.modules.machines.model import Machine


        
def report_transactions(mode=None,start_id=None,end_id=None):
    query = db.session.query(Transaction.machine_id, func.sum(Transaction.amount),func.count(Transaction.id))
    if mode == 'cashin':
        query = query.filter(Transaction.amount > 0)
    elif mode == 'cashout':
        query = query.filter(Transaction.amount < 0)
    
    if start_id:
        query = query.filter(Transaction.updated_at > start_id)
    if end_id:
        query = query.filter(Transaction.updated_at <= end_id)

    result = query.group_by(Transaction.machine_id).all()

    return [{'machine':r[0],'amount':r[1],'count':r[2]} for r in result]

class Report(BaseMixin,  Model):
    __bind_key__ = 'reports'
    id = Column(String, primary_key=True,default=lambda: str(uuid.uuid4()))
    created_at = Column(DateTime(timezone=True), default=datetime.datetime.utcnow)

    def __repr__(self):
        return f"Report(id={self.id},created_at='{self.created_at}')"

    def summary(self):
        start_tid, end_tid = self.get_range()
        machine_summary = {}
        cashins = {m['machine']: m for m in report_transactions(mode='cashin',start_id=start_tid,end_id=end_tid)}
        cashouts = {m['machine']: m for m in report_transactions(mode='cashout',start_id=start_tid,end_id=end_tid)}
        machine_ids = set([*cashins.keys(),*cashouts.keys()])
        machines = db.session.query(Machine).filter(Machine.id.in_(machine_ids)).all()
        machines_id_names = {m.id:m.name for m in machines}
        machines_id_roles = {m.id:m.role for m in machines}
        for m in machine_ids:
            ccin,ccout=0,0
            cin = cashins.get(m,0)
            name = machines_id_names.get(m,'unknown')
            role = machines_id_roles.get(m,'unknown')

            if cin:
                cin = cashins[m]['amount']
                ccin = cashins[m]['count']
            cout = cashouts.get(m,0)
            if cout:
                cout = cashouts[m]['amount']
                ccout = cashouts[m]['count']

            machine_info = {'start':start_tid,'end':end_tid,'id':m, 'name':name, 'role':role, 'total_cred_in':cin, 'total_cred_out':cout,'net_credit':cin + cout,'count_cin':ccin,'count_cout':ccout}
            machine_summary[m] = machine_info
        
        return machine_summary

    @classmethod
    def new_report(cls,session):
        previous_report_id, end_id = session.query(Report.id,Report.created_at).order_by(Report.created_at.desc()).first() or (None,0)
        (newest_transaction_id,) = session.query(Transaction.id).filter(Transaction.updated_at>end_id).order_by(Transaction.created_at.desc()).first() or (None,)
        if not newest_transaction_id:
            return None
        nr = Report()
        return nr

    def get_range(self,session=None):
        if not session:
            session = db.session
        previous_report_id, prev_report_time = session.query(Report.id,Report.created_at).filter(Report.created_at<self.created_at).order_by(Report.created_at.desc()).limit(1).first() or (None,0)
        return prev_report_time or datetime.datetime.min, self.created_at
   
    @classmethod
    def last_report_time(cls):
        session = db.session
        (t,) = session.query(cls.created_at).order_by(cls.created_at.desc()).limit(1).first() or (datetime.datetime.min,)
        return t

    def amount(self,machine_id=None):
        session = db.session
        q = self.filter_transactions(session.query(func.sum(Transaction.amount)))
        if machine_id:
            q = self.filter_machine(q,machine_id=machine_id)
        (s,) = q.first()
        if s is None:
            s = 0
        return s

    def cashins(self):
        session = db.session
        q = self.filter_transactions(session.query(func.sum(Transaction.amount)).filter(Transaction.amount>0))
        (s,) = q.first()
        if s is None:
            s = 0
        return s
    
    def cashouts(self):
        session = db.session
        q = self.filter_transactions(session.query(func.sum(Transaction.amount)).filter(Transaction.amount<0))
        (s,) = q.first()
        if s is None:
            s = 0
        return s

    def filter_machine(self,query,machine_id):
        return query.filter(Transaction.machine_id==machine_id)

    def filter_transactions(self,query):
        start,end = self.get_range()
        return query.filter(Transaction.updated_at > start,Transaction.updated_at <= end)

    def get_transactions(self,session=None,mode='',machine_id=''):
        if not session:
            session = db.session
        start,end = self.get_range()
        query = self.filter_transactions(session.query(Transaction))
        if machine_id:
            query = self.filter_machine(query,machine_id)
        if mode == 'cashin':
            query = query.filter(Transaction.amount > 0)
        elif mode == 'cashout':
            query = query.filter(Transaction.amount < 0)
        return query.all()

def init_app(app):
    app.context_processor(lambda: dict(Report=Report))