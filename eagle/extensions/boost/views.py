from flask_appbuilder import ModelView
import flask
from eagle.extensions.appbuilder.baseviews import HTMXView
from flask_appbuilder.models.generic import GenericModel, GenericSession, GenericColumn, interface
from .base import Boost, BoostedFunction, PersistQueue

from flask_appbuilder.models import decorators
def render_data(data):
    return data
def render_status(status):
    
    s = {
        0:'Initialized',
        1:'Ready',
        2:'Unacked',
        5:'Acked',
        9:'Failed'
        }.get(status,'Unknown')
    return flask.Markup(s)

def render_timestamp(ts):
    from datetime import datetime
    dt_obj = datetime.fromtimestamp(ts)
    return flask.Markup(dt_obj)
class QueueItemModel(GenericModel):
    id = GenericColumn(str,primary_key=True)
    queue = GenericColumn(str)
    data = GenericColumn(str)
    timestamp = GenericColumn(str)
    status = GenericColumn(str)

class QueueItemsInterface(interface.GenericInterface):
    def __init__(self,session=None):
        if not session:
            session = QueueSession()
        super().__init__(QueueItemModel,session)

    def delete(self, item):
        self.message = (f"Set task as FAILED: {item.id}: {item.data} ", 'danger')
        return self.session.delete(item.id)

class QueueSession(GenericSession):

    def __init__(self):
        super().__init__()
        self.query_class =  'QueueItemModel'

    def get(self, pk):
        queue_name, id = pk.split('-')
        q = self.boost.boost_queue_fun_map[queue_name].queue
        row = q.get(id=int(id),block=False,raw=True)
        row['id'] = row['pqid']
        self.add_row(queue_name=queue_name,r=row)
        return super().get(pk=pk)

    def delete(self, pk):
        queue_name, id = pk.split('-')
        q = self.boost.boost_queue_fun_map[queue_name].queue
        q.ack_failed(id=int(id))
        q.task_done()
        self.delete_all(QueueItemModel())
        return self.all()

    def all(self):
        self.delete_all(QueueItemModel())
        for func in self.boost.boost_queue_fun_map.values():
            queue = func.queue
            if isinstance(queue,PersistQueue):
                rows = queue.queue()
                for r in rows:
                    self.add_row(func._queue_name,r)
        return super().all()

    def add_row(self,queue_name,r):
        r['id'] = queue_name + '-' + str(r['id'])
        model = QueueItemModel(queue=queue_name,**r)
        self.add(model)
    @property
    def boost(self)->Boost:
        return flask.current_app.extensions.get('boost')


class QueueItemsView(ModelView,HTMXView):
    list_title = 'Queue Items'
    datamodel = QueueItemsInterface()
    base_permissions = ['can_list','can_delete']
    list_columns = ['queue','id','data','timestamp','status']
    show_columns = list_columns
    formatters_columns = {
        'status':render_status,
        'timestamp':render_timestamp,
        'data':render_data
    }
    links = [
        dict(
            url=lambda name: flask.url_for('.show',pk=name),
            title='View',
            icon='fa fa-search',
            description='See Details'
        )
    ]


class QueueModel(GenericModel):
    name = GenericColumn(str,primary_key=True)
    size = GenericColumn(int)
    ready = GenericColumn(int)
    active = GenericColumn(int)
    ack = GenericColumn(int)
    unack = GenericColumn(int)
    failed = GenericColumn(int)
    kind = GenericColumn(str)
    consumer = GenericColumn(str)
    doc = GenericColumn(str)

    @classmethod
    def from_func(self,func: BoostedFunction):
        doc = func.__doc__ if func.__doc__ != 'A named generic notification emitter.' else ''
        consumer = f"{func._f.__module__}:{func.__name__}"
        kind = func.queue.__class__.__name__
        if isinstance(func.queue,PersistQueue):
            queue = dict(
                size = func.queue.size,
                ready = func.queue.ready_count(),
                active = func.queue.active_size(),
                ack = func.queue.acked_count(),
                unack = func.queue.unack_count(),
                failed = func.queue.ack_failed_count()
            )
        else:
            queue = dict(
                size = -1,
                ready = -1,
                active = -1,
                ack = -1,
                unack = -1,
                failed = -1
            )
        model = QueueModel(name=func._queue_name,**queue,kind=kind,consumer=consumer,doc=doc)
        return model
        
class BoostSession(GenericSession):

    def __init__(self):
        super().__init__()
        self.query_class =  'QueueModel'

    def all(self):
        self.delete_all(QueueModel())
        for name in self.boost.boost_queue_fun_map.keys():
            self.add_object(name)
        return super().all()

    def get(self, pk):
        self.delete_all(QueueModel())
        self.add_object(pk)
        return super().get(pk=pk)

    def add_object(self,name):
        model = self._queue_to_obj(name)
        self.add(model)

    @property
    def boost(self)->Boost:
        return flask.current_app.extensions.get('boost')

    def _queue_to_obj(self,name):
        func = self.boost.boost_queue_fun_map[name]
        return QueueModel.from_func(func)

class BoostInterface(interface.GenericInterface):
    def __init__(self,session=None):
        if not session:
            session = BoostSession()
        super().__init__(QueueModel,session)

class QueuesView(ModelView,HTMXView):
    list_title = 'Queues'
    datamodel = BoostInterface()
    base_permissions = ['can_list','can_show']
    list_columns = ['name','size','ready','active','ack','unack','failed','consumer','doc']
    show_columns = list_columns

    links = [
        dict(
            url=lambda name: flask.url_for('.show',pk=name),
            title='View',
            icon='fa fa-search',
            description='See Details'
        )
    ]

def init_app(app):

    appbuilder = app.extensions.get('appbuilder')
    if appbuilder:
        appbuilder.add_view(QueuesView, "Queues", icon="fa-tasks",category="Developer")
        appbuilder.add_view(QueueItemsView, "QueueItems", icon="fa-th-list",category="Developer")