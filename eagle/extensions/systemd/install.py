"""
Functions to install/uninstall systemd services
"""
import pathlib
import shutil
import flask
import os

def systemctl(command: str,service:str=''):
    """
    Run the systemctl command
    """
    if not shutil.which('systemctl'):
        return
    uid = os.getuid()
    cmd = flask.current_app.config['SYSTEMD_SERVICE_COMMAND_TEMPLATE'].format(uid=uid,command=command,service=service)
    flask.current_app.logger.debug(cmd)
    os.system(cmd)

def enable_service(service: str):
    systemctl('enable',service)

def start_service(service):
    systemctl('start',service=service)

def status_service(service):
    systemctl('status',service=service)

def stop_service(service):
    systemctl('stop',service=service)

def disable_service(service):
    systemctl('disable',service=service)

def get_systemd_path():
    return pathlib.Path(os.environ['HOME']) / '.config' / 'systemd' / 'user'

def install_services(app:flask.Flask,services=None,services_list=None,path=None,dry_run=True,enable=True):
    """
    Install the files for systemd services
    """
    if dry_run:
        app.logger.warning(f"Dry-Run of install (no files will be created)")

    if not path:
        path = get_systemd_path()

    if not path.exists() and not dry_run:
        app.logger.debug(f'Creating services directory: {path}')
        path.mkdir(parents=True,exist_ok=True)

    workdir = app.config.setdefault('SYSTEMD_WORKDIR',app.instance_path)
    added = []

    if not services:
        services = app.config.get('SYSTEMD_SERVICES',[])

    if not services_list:
        services_list = app.config.get('SERVICES_LIST',{})

    for service in services:

        executable = services_list.get(service)
        if not executable:
            continue

        if executable.startswith('bin/'):
            executable = pathlib.Path(app.root_path) / executable

        #wanted_by = 'multi-user.target' # multi-user is not avialable in user services
        wanted_by = 'default.target'
        description = f"{app.name}-{service}"
        service_file = app.config['SYSTEMD_SERVICE_TEMPLATE'].format(
            Description=description,
            ExecStart=executable,
            WorkingDirectory=str(workdir).replace(os.environ['HOME'], '%h'), # user home directory may not exist at install
            WantedBy=wanted_by
        )

        file = pathlib.Path(path) / f'{service}.service'

        added.append(str(service))

        if dry_run or app.env == 'development':
            print(f"{file}\n: {service_file}")
            continue

        with open(file,'w') as f:
            f.write(service_file)

        if enable:
            enable_service(service)
            start_service(service)


    app.logger.debug(f"Created services: {added} in {path}")
    return added


def write_linger_file(app,dry_run=False,*args,**kwargs):
    import subprocess, getpass, shutil

    lingerdir = pathlib.Path(app.config['SYSTEMD_LINGER_DIR'])

    if not app.testing and not shutil.which('systemctl'):
        app.logger.warning(f'Command "systemctl" not found.  Ignoring liger files')
        dry_run = True

    user = app.config.setdefault('SYSTEMD_USER',getpass.getuser())
    lingerfile = lingerdir / user
    commands = [
        f"sudo mkdir -p {lingerdir}",
        f"sudo touch {lingerfile}"
    ]
    app.logger.debug(f'Creating linger file for {user}: {lingerfile}')
    for c in commands:
        app.logger.debug(f'Running command "{c}"' + (' [simulated]' if dry_run else ''))
        if not dry_run:
            if app.testing:
                c = c.replace('sudo ','')
            subprocess.Popen(c.split()).wait()

    return lingerfile

def uninstall_services(app,path=None):
    """
    Removes systemd service files
    """
    if not path:
        path = get_systemd_path()

    removed = []
    all_services = app.config.get('SERVICES_LIST',{})
    for service in all_services.keys():
        file = pathlib.Path(path) / f'{service}.service'
        if file.exists():
            stop_service(service)
            disable_service(service)
            removed.append(str(file))
            file.unlink()
    return removed

def is_installed(app,path=None):
    if not path:
        path = get_systemd_path()
    found = []
    services = set(app.config.get('SYSTEMD_SERVICES',[]))
    for service in services:
        file = pathlib.Path(path) / f'{service}.service'
        if file.exists():
            found.append(service)
    return set(found) == services


def write_systemd_files(app:flask.Flask,**kwargs):
    has_systemd = shutil.which('systemctl')
    if not has_systemd:
        app.logger.warning(f'Command "systemctl" not found, not writing systemd files')

    services = app.config['SYSTEMD_SERVICES']
    services_list = app.config['SERVICES_LIST']
    added = install_services(app,services,services_list,dry_run=(not has_systemd))
    app.logger.debug(f"Added systemd files: {added}")
