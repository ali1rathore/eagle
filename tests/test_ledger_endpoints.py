import pytest
import time
from eagle.extensions import services
from eagle.modules.transactions import get_last_transaction
from eagle.modules.transactions.model import Transaction

@pytest.mark.skip("dontlike")
@pytest.mark.parametrize("inp", [
    (1),
    (5),
    (10),
    (20),
    (50),
    (100)
])
def test_cashin_is_in_ledger(create_app,inp):
    """
    Given: the ba_pulse is running
    When: a cashin happens (/cashin/<amount> endpoint)
    Then: the /cashin endpoint should have the latest cashin
    """
    app = create_app(ROLE='BAN_TEST')
    cashin = services.current_services('cashin')
    cashin.start()
    client = app.test_client()
    resp = client.get(f'/cashin/{inp}')
    assert resp.status_code == 200
    time.sleep(3)
    cashin.stop(wait=True)
    t = get_last_transaction()
    cashin.kill(force=False)

    assert t.amount == inp*100


def test_cashout_is_in_ledger(create_app):
    """
    Given: the ba_pulse is running
    When: a cashin happens (/cashin/<amount> endpoint)
    Then: the /cashin endpoint should have the latest cashin
    """
    app = create_app(ROLE='BAN_TEST')
    cashout = services.current_services('cashout')
    cashout.start()
    client = app.test_client()
    import random
    import decimal

    
    amounts = (int(decimal.Decimal(random.randrange(1, 10000))) for i in range(100))
    print(amounts)
    for inp in amounts:
        resp = client.get(f'/cashout/{inp}')
        assert resp.status_code == 200
        time.sleep(1)
        t = get_last_transaction()
        assert t and t.amount == inp*-100


    cashout.stop(wait=False)
    cashout.kill(force=True)

@pytest.mark.skip("dontlike")
@pytest.mark.parametrize("inp", [
    (1),
    (5),
    (5.75),
    (10),
    (10.5),
    (20),
    (50)
])
def test_fish_cashout_is_in_ledger(create_app,inp):
    """
    Given: the fist_print is running
    When: a cashout happens (/test_fish_cashout endpoint)
    Then: the /cashout endpoint should have the latest cashout
    """
    app = create_app(ROLE='FIRELINK_TEST')
    cashout = services.current_services('cashout')
    cashout.start()
    client = app.test_client()
    resp = client.get(f'/cashout/{inp}')
    time.sleep(2)
    assert resp.status_code == 200

    resp = client.get('/cashout')

    data = resp.json
    assert data[1][-1]['cashout_cred'] == inp
    cashout.kill(force=False)
@pytest.mark.skip("dontlike")
@pytest.mark.parametrize("inp", [
    (1),
    (5),
    (5.75),
    (10),
    (10.5),
    (20),
    (50)
])
def test_lol_cashout_is_in_ledger(create_app,inp):
    """
    Given: the lol_print is running
    When: a cashout happens (/test_lol_cashout endpoint)
    Then: the /cashout endpoint should have the latest cashout
    """
    app = create_app(ROLE='LOL_TEST')
    cashout = services.current_services('cashout')
    cashout.start()
    client = app.test_client()
    resp = client.get(f'/cashout/{inp}')
    time.sleep(2)
    assert resp.status_code == 200

    resp = client.get('/cashout')

    data = resp.json
    assert data[1][-1]['cashout_cred'] == inp
    cashout.kill(force=False)
@pytest.mark.skip("dontlike")
@pytest.mark.parametrize("inp, outp", [
    (100, 1),
    (500, 5),
    (550, 5.5),
    (1000, 10),
    (1750, 17.5),
    (2000, 20),
    (5000, 50)
])
def test_lol1_cashout_is_in_ledger(create_app,inp,outp):
    """
    Given: the lol1_print is running
    When: a cashout happens (/test_lol1_cashout endpoint)
    Then: the /cashout endpoint should have the latest cashout
    """
    app = create_app(ROLE='LOL1_TEST')
    cashout = services.current_services('cashout')
    cashout.start()
    client = app.test_client()
    resp = client.get(f'/cashout/{inp}')
    time.sleep(2)
    assert resp.status_code == 200

    resp = client.get('/cashout')

    data = resp.json
    assert data[1][-1]['cashout_cred'] == outp
    cashout.kill(force=False)