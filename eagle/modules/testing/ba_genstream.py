from subprocess import Popen, PIPE, STDOUT


BA_STREAM_TEMPLATE="""fc 5 11 27 56
fc 5 11 27 56
fc 5 11 27 56
fc 5 5 82 0
fc 5 12 bc 64
fc 5 5 82 0
fc 6 13 {amount_encoded} {checksum}
fc 5 50 aa 5
fc 5 5 82 0
fc 5 5 82 0
fc 5 14 8a 1
fc 5 5 82 0
fc 5 15 3 10
fc 5 5 82 0
fc 5 16 98 22
fc 5 5 82 0
fc 5 11 27 56
fc 5 11 27 56
fc 5 11 27 56
"""

BANILA_STREAM_TEMPLATE="""
1b 1b 00 00 00 05 18 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 05 
18 53 54 4f 52 45 20 49 44 3a 20 30 00 00 00 00
00 00 00 00 00 00 00 00 00 05 18 54 45 52 4d 49 
4e 41 4c 23 20 30 00 00 00 00 00 00 00 00 00 00
00 00 00 05 18 44 41 54 45 54 49 4d 45 20 30 33 
2f 32 33 2f 32 32 20 31 36 3a 35 37 00 05 18 54
49 43 4b 45 54 23 20 31 33 32 00 00 00 00 00 00 
00 00 00 00 00 00 00 05 18 41 4d 4f 55 4e 54 20
24 {amount_encoded} 2e 30 30 00 00 00 00 00 00 00 00 00 00 00 
00 05 18 53 69 78 20 61 6e 64 20 30 30 2f 31 30
30 00 00 00 00 00 00 00 00 00 00 05 18 00 00 00 
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 05 18 00 00 00 00 00 00 00 00 00 
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 05
18 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
00 00 00 00 00 00 00 00 00 00 00 00 1b 1b 1b 1b 
"""

POG_STREAM_TEMPLATE="""14:50:34 * 11/25/20
TICKET#00000012 --- CREDIT VALUE = $0.05 0x00
** VOUCHER TICKET **
** TRIPLE SEVENS  **
----------------------------------------
CRED:  4949 $ {amount_encoded}
********TWO HUNDRED FORTY SEVEN*********
******DOLLARS AND FORTY FIVE CENTS******
----------------------------------------
X                  
----------------------------------------
VOID IF MUTILATED          VAL# 67F03ED5
COPYRIGHT 1986-98 LEISURE TIME TECH.INC.
----------------------------------------


----------------------------------------
LEISURE TIME TECH. V:POG_510C/R510POG2 *
MACH#000001 BANK#001
----------------------------------------
<LEISURE TIME TECH.>
******  VALID ON THIS DATE ONLY!  ******"""

def to_ascii(text):
    ascii_values = ""
    for chars in text:
        ascii_values += hex(ord(chars)).replace('0x', ' ')
    return ascii_values

def generate_banila_stream(amount:str):
    
    amount_encoded = to_ascii(amount)

    return BANILA_STREAM_TEMPLATE.format(amount_encoded=amount_encoded)

def generate_pog_cashout_stream(amount):
    return POG_STREAM_TEMPLATE.format(amount_encoded=amount)

def generate_ba_stream(amount):
    
    ENCODED_AMOUNTS = {
        '1' : '61',
        '2' : '62',
        '5' : '63',
        '10': '64',
        '20': '65',
        '50': '66',
        '100': '67'
    }
    CHECKSUM = {
        '1' : 'b0 fb',
        '2' : '2b c9',
        '5' : 'a2 d8',
        '10': '1d ac',
        '20': '94 bd',
        '50': '0f 8f',
        '100': '86 9e'        
    }
    if str(amount) not in ENCODED_AMOUNTS.keys():
        raise RuntimeError(f'Amount must be one of {ENCODED_AMOUNTS.keys()}')

    amount_encoded = ENCODED_AMOUNTS[amount]
    checksum = CHECKSUM[amount]

    return BA_STREAM_TEMPLATE.format(amount_encoded=amount_encoded, checksum=checksum)

def generate_banila_bytes(amount):
    amount = str(amount)
    hex_stream = generate_banila_stream(amount).replace('\n',' ').split()
    fixed_stream = []
    for b in hex_stream:
        if len(b) == 1:
            b = '0'+b
        fixed_stream.append(b)
    hex_bytes = [bytes.fromhex(b) for b in fixed_stream]
    return b''.join(hex_bytes)

def generate_ba_bytes(amount):
    amount = str(amount)
    hex_stream = generate_ba_stream(amount).replace('\n',' ').split()
    fixed_stream = []
    for b in hex_stream:
        if len(b) == 1:
            b = '0'+b
        fixed_stream.append(b)
    hex_bytes = [bytes.fromhex(b) for b in fixed_stream]
    return b''.join(hex_bytes)

def generate_pog_cashout_bytes(amount):
    amount = str(amount)
    pog_stream = generate_pog_cashout_stream(amount)
    fixed_stream = []
    for char in pog_stream:
        my_hex =  char.encode()
        if my_hex == b'\n':
            my_hex = b'\r'
        fixed_stream.append(my_hex)
    return b''.join(fixed_stream)


if __name__ == "__main__":
    ban_stream = generate_banila_bytes("15")
    byte_stream = generate_ba_bytes(100)
    pog_stream = generate_pog_cashout_bytes("9.45")
    test = pog_stream

    with open('pog_bytes.txt','wb') as f:
        f.write(pog_stream)

    with open('ban_bytes.txt','wb') as f:
        f.write(ban_stream)

    with open('ba_bytes.txt','wb') as f:
        f.write(byte_stream)

    #print(generate_ba_stream('1'))
    #p = Popen(['./uba_stream'], stdout=PIPE, stdin=PIPE, stderr=PIPE)
    #stdout,stderr = p.communicate(input=generate_ba_stream('1').encode())
    #print(f'stdout = {stdout}')