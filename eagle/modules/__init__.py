from . import (
    telemetry,
    replicas,
    configuration,
    attributes,
    processes,
    server,
    reports,
    testing,
    machines,
    transactions,
    fetching,
    credits,
    pos,
    workers,
    cstores
)


def init_app(app):
    configuration.init_app(app)
    telemetry.init_app(app)
    attributes.init_app(app)
    processes.init_app(app)
    replicas.init_app(app)
    server.init_app(app)
    reports.init_app(app)
    testing.init_app(app)
    machines.init_app(app)
    transactions.init_app(app)
    fetching.init_app(app)
    credits.init_app(app)
    pos.init_app(app)
    workers.init_app(app)
    cstores.init_app(app)
