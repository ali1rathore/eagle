from threading import Thread
import pytest, requests
import flask
from eagle.app import create_app as create_eagle
import shlex
from time import sleep



@pytest.fixture()
def runner(app):
    r = app.test_cli_runner()
    def runfunc(command):
        return r.invoke(r.app.cli,shlex.split(command))
    yield runfunc


@pytest.fixture()
def create_app(tmp_path):
    def create_func(**kwargs):
        
        app = create_eagle(instance_path=tmp_path,**kwargs,TESTING=True,SECRET_KEY='secret')       
        app_ctx = app.app_context()
        app_ctx.push()
        return app
    yield create_func

@pytest.fixture(scope='function')
def test_client(create_app):

    def create_test_client(**kwargs):
        app = create_app(**kwargs)
        return app.test_client()

    yield create_test_client


@pytest.fixture(scope='function')
def app(create_app):
    return create_app()

@pytest.fixture(scope='function')
def client(test_client):
    return test_client()



@pytest.fixture(scope='session')
def server():
    host='0.0.0.0'
    port='5057'

    from eagle.app import create_app
    from eagle.extensions import startup
    app = create_app(TESTING=True)
    startup.on_start.send(app)

    thread = Thread(target=app.run, daemon=True, kwargs=dict(host=host,port=port))
    thread.daemon = True
    thread.start()
    sleep(2)
    yield app,f'http://{host}:{port}'

    requests.get(f'http://{host}:{port}/server/shutdown/1')
    #thread.join()
