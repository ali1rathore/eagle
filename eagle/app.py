# -*- coding: utf-8 -*-
import flask, click
from pathlib import Path
from . import extensions, config, modules, models
# TODO: locale.setlocale(locale.LC_TIME, locale.getlocale())

# logging.basicConfig(format='\n%(asctime)s %(levelname)-8s %(message)s\n[%(pathname)3s:%(lineno)d]',datefmt='%Y-%m-%d:%H:%M:%S',level=logging.DEBUG)

def create_app(config_file: Path = None, instance_path: Path = None, **overrides:flask.Config):
    """Create an application with custom configurations.

    Configurations passed into this function will override
    all other configurations.

    Args:
        config_file (Path, optional): Path to a config file. Relative
            paths will be relative to the instance_path. Defaults to None.
        overrides (dict, optional): dictionary of configuration value overrides.

    Raises:
        EnvironmentError: The configuration file does not exist
    """

    # Set CONFIG_VERSION=latest if you want each version to have a separate config dir.
    # Otherwise all versions of this app will share the same config dir.
    # Set CONFIG_VERSION to a specific version to use that configuration

    app = flask.Flask(
            config.APP_NAME,
            instance_path=str(instance_path) if instance_path else config.INSTANCE_PATH,
            static_folder=None, template_folder=config.TEMPLATE_FOLDER,
            instance_relative_config=True)

    with app.app_context():
        config.init_app(app,config_file=config_file,**overrides)
        extensions.init_app(app)
        modules.init_app(app)
        models.init_app(app)

    text = f"""
    instance_path: {app.instance_path}
    NAME: {app.config['APP_NAME']}
    DATABASE: {app.config['DATABASE']}
    CONFIG: {app.config['EAGLE_CONFIG']}
    ROLE: {app.config['ROLE']}
    HOST_ID: {app.config['HOST_ID']}
    HOSTNAME: {app.config['HOSTNAME']}
    TESTING: {app.testing}
    ENVIRONMENT: {app.env}
    env_file: {app.config['ENV_FILE']}
    root_path: {app.root_path}
    extensions: {list(app.extensions.keys())}
    blueprints: {list(app.blueprints.keys())}
    static_folder: {app.static_folder}
    static_url_path: {app.static_url_path}
    template_folder: {app.template_folder}
    session_cookie_name: {app.session_cookie_name}
    VERSION: {app.config['VERSION']}
    REVISION: {app.config['COMMIT_ID']}
    """

    app.logger.debug(text)

    @app.route('/version')
    def version():

        return flask.current_app.config['VERSION']

    @app.cli.command('version',help=f"Print the curent version of {app.name}")
    def print_version():
        f"""Print the version of {app.name}"""
        click.echo(flask.current_app.config['VERSION'])

    @app.route('/favicon.ico')
    def favicon():
        return flask.Response(app.config.get('FAVICON',''))

    init_kiosk(app)
    return app

def init_kiosk(app:flask.Flask):
    url = app.config.setdefault('KIOSK_URL','/kiosk')
    @app.route(url)
    def kiosk():
        flask.session['keyboard'] = True
        return flask.render_template('kiosk.html')

@extensions.installer.install_requested.connect
def install_app(app):
    extensions.kiosk.install_kiosk_files(app)
    extensions.systemd.write_systemd_files(app)
    extensions.systemd.write_linger_file(app)

@modules.fetching.transactions_fetched.connect
def handle_new_transactions(app,transactions):
    models.write_transaction_to_db(app,transactions=transactions)