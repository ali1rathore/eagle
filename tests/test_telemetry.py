from eagle.modules import testing
from eagle.modules import telemetry

def test_send_telemetry_sends_signal(create_app):

    app = create_app()

    with testing.captured_signals(app=app,signals=telemetry.on_send) as sigs:
        tele, exceptions = telemetry.send_telemetry(app=app)
        assert sigs[0][0] == telemetry.on_send
        assert sigs[0][1] == app
        assert sigs[0][3]['heartbeats'] == 1