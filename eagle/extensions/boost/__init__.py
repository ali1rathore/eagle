from .base import Boost, MQTTQueue, HTTPQueue, PersistQueue
from . import views

boost = base.Boost()

def init_app(app):
    if app.config.setdefault('ENABLE_BOOST',False) != True:
        return
    boost.init_app(app)
    views.init_app(app)

__all__ = ['Boost','MQTTQueue','HTTPQueue','PersistQueue','init_app']