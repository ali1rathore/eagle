from sqlalchemy import Column, String, DateTime, Boolean
from flask_appbuilder.models.mixins import BaseMixin
from flask_appbuilder.models.sqla import Model
from sqlalchemy_utils import UUIDType, generic_repr
import datetime, json, uuid
import flask

@generic_repr
class CStore(BaseMixin,  Model):

    __bind_key__ = 'cstores'
    id = Column(UUIDType(binary=False), primary_key=True,default=uuid.uuid4)
    name = Column(String(50), nullable=False)
    address = Column(String(50), nullable=True)
    properties = Column(String,nullable=True)
    created_at = Column(DateTime(timezone=True), default=datetime.datetime.utcnow)
    updated_at = Column(DateTime(timezone=True), onupdate=datetime.datetime.utcnow)
    available = Column(Boolean, nullable=True, default=True)

    @property
    def host_id(self):
        if self.properties:
            return json.loads(self.properties).get('HOST_ID','')
        return ''

    @property
    def hostname(self):
        if self.properties:
            return json.loads(self.properties)['HOSTNAME']
        return 'NONE'

    def __repr__(self):
        return f"CStore(id='{self.id}',name='{self.name}',address='{self.address}')"

    def __html__(self):
        return f"{self.name} at{self.address}"


def init_app(app:flask.Flask):
    
    app.context_processor(lambda: dict(CStore=CStore))