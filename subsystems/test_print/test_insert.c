/* Copyright */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>
#include <sqlite3.h>

//unsigned char buf[16];
sqlite3 *db;
char sql[128];
char cmd[128];
char *err_msg = 0;
int db_insert(int);

int db_insert(int denom){
    struct timeval current_time;
    gettimeofday(&current_time, NULL);

    sprintf(sql, "INSERT into ledger (amount, timestamp) VALUES (%d, %ld)", denom, current_time.tv_sec);
    int rc = sqlite3_exec(db, sql, 0, 0, &err_msg);

    if (rc != SQLITE_OK ) {

        fprintf(stderr, "SQL error: %s\n", err_msg);

        sqlite3_free(err_msg);
    //    sqlite3_close(db);

        return 1;
    }

return(0);
}


int main(int argc, char **argv)
{  
    if( argc!=2 ){
      fprintf(stderr, "Usage: %s DATABASE\n", argv[0]);
      exit(1);
    }

    int rc = sqlite3_open(argv[1], &db);

    if (rc != SQLITE_OK) {

        fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        exit(1);
    }
    int count;
	// Wait here
    //    printf("All set, sending dummy inserts\n");
    while(1){
    //            count = 100 - (srand() % 199);
    //            count = 100 - arc4random_uniform(199);
                count = 600;
                printf("%d\n", count);
                fflush(stdout);
                db_insert(count);
                
                  
        sleep(10);
    }
}
