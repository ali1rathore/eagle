from sqlalchemy import Integer, Column, String, DateTime
from flask_appbuilder.models.mixins import BaseMixin
from flask_appbuilder.models.sqla import Model
import datetime

from eagle.models import insert_db
class LogMessage(BaseMixin,  Model):
    id = Column(Integer,primary_key=True,autoincrement=True)
    msg = Column(String, nullable=False)
    name = Column(String, nullable=False)
    level = Column(String, nullable=False)
    thread = Column(String, nullable=False)
    function = Column(String, nullable=False)
    path = Column(String, nullable=False)
    level = Column(String, nullable=False)
    created_at = Column(DateTime(timezone=True), default=datetime.datetime.utcnow)

    def __repr__(self):
        return f"LogMessage(name='{self.name}',level='{self.level}',msg='{self.msg},time='{self.created_at}')"

def write_log_to_db(app,**record):
    sqla = app.extensions.get('sqlalchemy')
    if not sqla:
        return
    row = LogMessage(**record)
    insert_db(row)

def init_app(app):
    pass