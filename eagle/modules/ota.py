CURRENT_VERSION="changeme"

if __name__ == '__main__':
    configs =  {k:v for (k,v) in globals().items() if v and isinstance(v,str) and k.upper()==k}
    from eagle.app import create_app
    from eagle.modules import configuration
    app = create_app(DEBUG=True)
    if app.config.get('CURRENT_VERSION') != CURRENT_VERSION:
        with app.app_context():
            app.logger.warning(f"Updating eagle configuration with { configs } at {configuration.views.InstanceConfigView.datamodel.session.filepath}")
            for val in (configuration.ConfigModel(name=k,value=v) for k,v in configs.items()):
                item = configuration.views.InstanceConfigView.datamodel.edit(val)
