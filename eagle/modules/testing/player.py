from time import sleep
import requests
from eagle.modules.machines.model import Machine
from eagle.extensions.appbuilder import db
from eagle.app import create_app
import sys
import flask
def play_game(address,amount):
    mode = 'cashin'
    if amount < 0:
        mode = 'cashout'
    amount = abs(amount)
    try:
        resp = requests.get(f"http://{address}/{mode}/{amount}")
    except Exception as e:
        print(f"Failed fetching from {address}: {e}")
        return
    return resp

def play_all(amount,roles = None):

    
    machines = db.session.query(Machine).filter(Machine.available==True).all()

    if not roles:
        roles = flask.current_app.config['AVAILABLE_ROLES']
    if 'POS' in roles:
        del roles[roles.index('POS')]
    addresses = [m.address for m in machines if m.role in roles]

    for a in addresses:
        resp = play_game(a,amount)
        if not resp:
            pass
        elif not resp.ok:
            print(f"Failed playing {resp.request.url}: {resp.text}")
        else:
            print(f"Playing {amount} in {a}")

if __name__ == '__main__':
    args = sys.argv[1:]
    app = create_app()
    print(f'playing {args}')
    with app.app_context():
        for i in [1, 5, 10, 20, 50, 100, -1, -15, -75, -100, 5]:
            play_all(i,roles=args)