#include <sqlite3.h>
#include <sys/time.h>
#include <stdio.h>

int append_ledger(int);
char *err_msg = 0;

sqlite3 *db;
char sql[128];


int process_cashout(int denom)
{
	printf("%d\n", denom);
	fflush(stdout);
    append_ledger(denom);
    return 0;
}

int append_ledger(int denom){
    struct timeval current_time;
    gettimeofday(&current_time, NULL);
    sprintf(sql, "INSERT into ledger (amount, timestamp) VALUES (%d, %ld)", denom , current_time.tv_sec);
    int rc = sqlite3_exec(db, sql, 0, 0, &err_msg);

    if (rc != SQLITE_OK ) {

        fprintf(stderr, "SQL error: %s\n", err_msg);
        sqlite3_free(err_msg);

    }

return(0);
}

char * parse_db_args(char *argv[]) {
    char * DATABASE = NULL;
    DATABASE = argv[1];
    return DATABASE;
}

int init_db(char * DBNAME){

    int rc = sqlite3_open(DBNAME, &db);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
    }
    return rc;
}
