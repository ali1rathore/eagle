/*
 * By SydBernard
 */

#ifndef id003_driver
#define id003_driver

//#include "crc_kermit.h"

#define 	SYNC  				0xFC

/*		STATUS		*/
#define		STATUS_REQUEST		0x11
#define		ENABLE_				0x11
#define		IDLING				0x11
#define		ACCEPTING			0x12
#define		ESCROW				0x13  // +DATA
#define		STACKING			0x14
#define		VEND_VALID			0x15
#define		STACKED				0x16
#define		REJECTING			0x17  // +DATA
#define		RETURNING			0x18
#define		HOLDING				0x19
#define		DISABLE_			0x1A
#define		INHIBIT				0x1A
#define		INITIALIZE			0x1B

/*	POWER UP STATUS	*/
#define		POWER_UP 							0x40
#define		POWER_UP_WITH_BILL_IN_ACCEPTOR 		0x41
#define		POWER_UP_WITH_BILL_IN_STACKER 		0x42

/*	ERROR STATUS	*/
#define		STACKER_FULL			0x43
#define		STACKER_OPEN			0x44
#define		JAM_IN_ACCEPTOR			0x45
#define		JAM_IN_STACKER			0x46
#define		PAUSE					0x47
#define		CHEATED					0x48
#define		FAILURE					0x49  // +DATA
#define		COMMUNICATION_ERROR		0x4A

/*	RESPONSE TO VEND VALID AND RESPONSE TO OPERATION COMMAND 	*/
#define		ACK						0x50
#define		INVALID_COMMAND			0x4B

/*	OPERATION COMMAND 	*/
#define		RESET_			0x40
#define		STACK1			0x41
#define		STACK2			0x42
#define		RETURN_			0x43
#define		HOLD			0x44
#define		WAIT			0x45

/*	SETTING COMMAND AND RESPONSE TO SETTING COMMAND	*/
#define		CMD_EN_DIS_DENOMI			0xC0  	//   ENABLE/DISABLE DENOMINATION
#define		CMD_SECURITY_DENOMI			0xC1
#define     CMD_MODE       				0xC2
#define		CMD_INHIBIT_ACCEPTOR		0xC3
#define		CMD_DIRECTION				0xC4
#define		CMD_OPTIONAL_FUNCTION		0xC5
#define		CMD_BARCODE					0xC6
#define		CMD_BAR_INHIBIT				0xC7

/*	SETTING STATUS REQUEST */
#define		SSR_EN_DIS_DENOMI				0x80  //   ENABLE/DISABLE DENOMINATION
#define		SSR_SECURITY_DENOMI				0x81
#define		SSR_MODE_REQUEST				0x82
#define		SSR_INHIBIT_ACCEPTOR			0x83
#define		SSR_DIRECTION					0x84
#define		SSR_OPTIONAL_FUNCTION			0x85
#define		SSR_BARCODE_REQUEST				0x86
#define		SSR_BAR_INHIBIT_REQUEST			0x87
#define		SSR_VERSION_REQUEST				0x88
#define		SSR_VERSION_INFORMATION			0x88
#define		SSR_BOOT_VERSION_REQUEST		0x89
#define		SSR_BOOT_VERSION_INFORMATION	0x89
#define		SSR_BILL_TABLE					0x8A

void id003_format(unsigned char LNG, unsigned char CMD, unsigned char *DATA);
void rx_stream_uba(unsigned char temp);
void id003_master( uint8_t CMD, uint8_t *DATA);
//void id003_slave( uint8_t CMD, uint8_t *DATA);
int loop_id003(void);

#endif
