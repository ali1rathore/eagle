from sqlalchemy import Column, String, Integer, DateTime
from eagle.extensions.appbuilder import Model,BaseMixin
import datetime
import uuid
from dateutil import parser
from sqlalchemy_utils import UUIDType, generic_repr
        
@generic_repr
class Transaction(BaseMixin,  Model):
    __bind_key__ = 'transactions'

    id = Column(UUIDType(binary=False), primary_key=True,default=uuid.uuid4)
    amount = Column(Integer, nullable=False)
    machine_id = Column(String, nullable=False)
    created_at = Column(DateTime(timezone=True), default=datetime.datetime.utcnow)
    updated_at = Column(DateTime(timezone=True), default=datetime.datetime.utcnow)

    @classmethod
    def from_dict(cls,transaction):
        ca = transaction.get('created_at')
        if ca:
            created_at = parser.parse(ca)
        t = cls(id=transaction['id'],amount=transaction['amount'],machine_id=transaction['machine_id'],created_at=created_at)
        return t

    def to_json(self):
        d = super().to_json()
        d['id'] = str(d['id'])
        return d

def init_app(app):
    app.context_processor(lambda: dict(Transaction=Transaction))
