import json, queue, time
import flask
import traceback
import threading
import contextlib
from typing import Any
import uuid
from persistqueue import SQLiteAckQueue as PersistQueue
from .http import HTTPQueue
from .mqtt import MQTTQueue
from .errors import TaskFailed
import inspect

DEFAULT_DELAY=10

class BoostedResponse:
    def __init__(self,id,error,rv,function=None) -> None:
        self._value = rv
        self._id = id
        self._error = error
        self._is_set = False
        self._function: BoostedFunction = function

    def set(self,value):
        self._value = value
        self._is_set = True

    def __repr__(self) -> str:
        if self._is_set:
            return repr(self._value)
        else:
            return f"<BoostedResponse ({self._function}) Pending {self._id}>"
        
    def get(self,):
        if not self._is_set:
            response = self._function._execute_next(id=self._id)
            self._value = response._value
            self._id = response._id
            self._error = response._error
            self._is_set = True
        return self._value

class BoostedFunction:

    def __init__(self,f,queue_name,queue_kind=None,app=None,boost=None,delay=DEFAULT_DELAY,*args,**kwargs) -> None:
        self._f = f
        self._app: flask.Flask = None
        self._queue = None
        self._queue_name = queue_name
        self._queue_class = queue_kind
        self._boost:Boost = boost
        self.consumer_thread = None
        self._pushed = []
        self._delay = delay
        self._args = args
        self._kwargs = kwargs

        if app:
            self.init_app(app)

    @property
    def queue(self):
        if self._queue == None:
           with self._app.app_context():
                self._queue = self._queue_class(self._queue_name,*self._args,**self._kwargs)
        return self._queue
    
    def init_app(self,app):
        self._app = app

    def __call__(self, *args: Any, **kwds: Any) -> Any:
        return self.call(*args,**kwds)

    def __repr__(self) -> str:
        return f"BoostedFunction({self._f.__name__}) -> ({self._queue_name})"

    @property
    def __name__(self) -> str:
        return self._f.__name__

    def call(self, *args: Any, **kwds: Any) -> Any:
        try:
            with self._app.app_context():
                self._app.logger.debug(f"Calling {self.__name__}({', '.join((str(a) for a in args))},{kwds})")
                r =  self._f(*args,**kwds)
                self._app.logger.debug(f"Returned {self.__name__} -> {r}")
            return r
        except Exception as e:
            self._boost._app.logger.error(f"Error calling {self._f.__name__}: {repr(e)}")
            traceback.print_exc()
            raise

    def consume(self):
        if not self.consumer_thread:
            self.consumer_thread = threading.Thread(
                target=self.loop_forever,
                name=self.__name__
            )
            self.consumer_thread.start()
        return self.consumer_thread
        
    def sleep(self):
        if self._delay:
            time.sleep(self._delay)

    def loop_forever(self):
        with self._app.app_context():
            id = None
            while True:         
                rv =  self._execute_next(id)
                self.sleep()
                    

    def _execute_next(self,id=None):
        raw_item = self.queue.get(id=id,raw=True)
        id = raw_item.get('pqid')
        item = raw_item.get('data',{})
        args = item.get('args',[])
        kwargs = item.get('kwargs',{})
        rv, error = None, None
        try:
            rv = self.call(*args,**kwargs)
        except (TaskFailed, Exception) as e:
            if id:
                self.queue.nack(id=id)
            else:
                self.queue.nack(item=item)
            error = e
        except Exception as e:
            if id:
                self.queue.ack_failed(id=id)
            else:
                self.queue.ack_failed(item=item)
            error = e
        else:
            if id:
                self.queue.ack(id=id)
            else:
                self.queue.ack(item=item)
        
        self.queue.task_done()

        return BoostedResponse(id,error,rv,function=self)

    def push(self,*args,**kwargs):
        item = dict(args=args,kwargs=kwargs)
        id = self.queue.put(item)
        return BoostedResponse(id,None,None,function=self)


def inspect_simple(frame):
    """ inspect function name, parameters """
    funcname = frame.f_code.co_name
    print("function {}()".format(funcname))

    # pull tuple from frame
    args, args_paramname, kwargs_paramname, values = inspect.getargvalues(
        frame)

    # show formal parameters
    for i in (args if args is not None else []):
        print("\t{}={}".format(i, values[i]))

    # show positional varargs
    if args_paramname is not None:
        varglist = values[args_paramname]
        for v in (varglist if varglist is not None else []):
            print("\t*{}={}".format(args_paramname, v))

    # show named varargs
    if kwargs_paramname is not None:
        varglist = values[kwargs_paramname]
        for k in (sorted(varglist) if varglist is not None else []):
            print("\t*{} {}={}".format(kwargs_paramname, k, varglist[k]))


class Boost:
    def __init__(self,app=None) -> None:
        self._app = app
        if self._app:
            self.init_app(self._app)
        self.boost_queue_fun_map = {}
        self._session = None

    def get_calling_boosted_function(self):
        frame = inspect.currentframe()
        funcs = self.funcnames
        while frame:
            funcname = frame.f_code.co_name
            if funcname in funcs:
                boosted = frame.f_globals[frame.f_code.co_name]
                inspect_simple(frame)
                return boosted
            frame = frame.f_back
        return None

    @property
    def funcnames(self):
        return [f.__name__ for f in self.boost_queue_fun_map.values()]

    def function(self,queue_name):
        return self.boost_queue_fun_map[queue_name]

    def init_app(self,app):
        self._app = app
        self._app.extensions['boost'] = self
        for name,func in self.boost_queue_fun_map.items():
            func.init_app(app)
    @property       
    @contextlib.contextmanager
    def session(self,):
        sess = flask.current_app.appbuilder.session
        yield sess
        new = sess.new.copy()
        dirty = sess.dirty.copy()
        deleted = sess.deleted.copy()
        try:
            self._app.logger.debug(f"[Commit Start] new={new} deleted={deleted} dirty={dirty}")
            sess.commit()
        except Exception as e:
            sess.rollback()
            self._app.logger.error(f"""
            Failed commiting session: 
            new={new}
            dirty={dirty}
            deleted={deleted}
            ------
            {e}
            ------
            Called by {self.get_calling_boosted_function()}
            """)
            raise
        else:
            self._app.logger.debug(f"[Commit Finished]")
        finally:
            pass#sess.close()
    def boost(self,queue_name,broker_kind,
            delay=DEFAULT_DELAY,
            *args,
            **kwargs
           ):
        """
        """
        def _deco(wrapped):
            func = BoostedFunction(wrapped,queue_name=queue_name,queue_kind=broker_kind,boost=self,delay=delay,*args,**kwargs)
            self.boost_queue_fun_map[queue_name] = func
            return func
        return _deco  

    def __call__(self, *args: Any, **kwds: Any) -> Any:
        return self.boost(*args,**kwds)
