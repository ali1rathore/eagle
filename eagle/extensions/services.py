from threading import Thread
import subprocess
import time
import traceback
import flask
import shlex, sys
from blinker import signal
import selectors

class ServiceManager(dict):

    def restart(self,name):
        svs = self[name]
        new_svs = svs.clone()
        self[name] = new_svs
        new_svs.start()
        return new_svs

    def start(self):
        for s in self.values():
            s.start()

    def stop(self):
        for s in self.values():
            s.stop()

    def kill(self):
        for s in self.values():
            s.kill()

    def stopped(self):
        d = {}
        for n,s in self.items():
            d[n] = s.returncode != None
        return d

    def started(self):
        d = {}
        for n,s in self.items():
            d[n] = s._process != None
        return d

class Service(Thread):
    def __init__(self, command,app=None,restart=False,**kwargs) -> None:
        self.app = app
        self._command = command
        self._process = None
        self._stdout_event = None
        self._stderr_event = None
        self._restart = restart

        super().__init__(target=self._launch_and_watch, daemon=True,**kwargs)

    @property
    def stdout_event(self):
        if not self._stdout_event:
            self._stdout_event = signal(f'{self.name}-stdout-event')
        return self._stdout_event

    @property
    def stderr_event(self):
        if not self._stderr_event:
            self._stderr_event = signal(f'{self.name}-stderr-event')
        return self._stderr_event

    def clone(self):
        newsvs = Service(
            command = self._command,
            app = self.app,
            name = self.name
        )
        newsvs.on_stderr = self.on_stderr
        newsvs.on_exit = self.on_exit
        newsvs.on_start = self.on_start
        newsvs.on_stdout = self.on_stdout
        newsvs.on_start_fail = self.on_start_fail
        return newsvs

    @property
    def stdin(self):
        return self._process.stdin if self._process else None

    @property
    def returncode(self):
        return self._process.returncode if self._process else None

    @property
    def pid(self):
        return self._process.pid if self._process else None

    @property
    def stdout(self):
        return self._process.stdout if self._process else None

    @property
    def stderr(self):
        return self._process.stderr if self._process else None

    @property
    def command(self):
        return shlex.split(self._command)

    def write(self, txt: str,flush=True):
        if not self.stdin:
            return 0
        stxt = txt
        if not type(stxt)==bytes:
            stxt = str(stxt).encode()
        l = self.stdin.write(stxt)
        if flush:
            self.stdin.flush()
        return l

    def read(self):
        if self.stdout:
            return self.stdout.read()

    def start(self, wait=True, timeout_s=5):
        if self._started.isSet():
            return True
        super().start()
        if wait:
            for _ in range(timeout_s):
                if self.stdin:
                    return True
                if not self.is_alive():
                    return False
                time.sleep(1)
            return False

    def stop(self, wait=True):
        if not self._process:
            return False
        if self.stdin:
            if not self.stdin.closed:
                self.stdin.close()

        if wait:
            self._process.wait()
            self.join()

        return True

    def kill(self, wait=True, force=False):
        if not self._process or self._process.returncode is not None:
            return False

        self._process.terminate()
        if self._process.returncode is None and force:
            self._process.kill()
        if wait:
            self._process.wait()
            self.join()

        return True

    def on_exit(self, returncode):
        self.debug(
            f"Exited service {self.command} rc={returncode} in thread {self.name}"
        )

    def on_start(self):
        self.debug(f"Started service {self.command} in thread {self.name}")

    def on_start_fail(self, e):
        self.debug(
            f"Failed to start {self.command} in thread {self.name}: {e.__class__.__name__}{e}"
        )

    def on_stdout(self, line):
        #self.debug(f"Recieved stdout={line} from {self.command} in thread {self.name}")
        if self.app:
            with self.app.app_context():
                self.stdout_event.send(self,line=line)
        else:
            self.stdout_event.send(self,line=line)


    def on_stderr(self, line):
        #self.debug(f"Recieved stderr={line} from {self.command} in thread {self.name}")
        if self.app:
            with self.app.app_context():
                self.stderr_event.send(self,line=line)
        else:
            self.stderr_event.send(self,line=line)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(command={self.command},name='{self.name}',rc={self.returncode})"

    def _create_subprocess(self):
        try:
            self._process = subprocess.Popen(
                self.command,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                stdin=subprocess.PIPE,
                universal_newlines=False,
                text=False,
            )
        except Exception as e:
            self.on_start_fail(e)
        return self._process

    def debug(self, string):
        print(string)

    def _launch_and_watch(self, *args, **kwargs):

        while True:
            popen = self._create_subprocess()
            if not popen:
                return

            self.on_start()

            self.collect_outputs()

            self.on_exit(self.returncode)
            if not self._restart:
                break
        

    def collect_outputs(self):
        sel = selectors.DefaultSelector()
        sel.register(self._process.stdout, selectors.EVENT_READ)
        sel.register(self._process.stderr, selectors.EVENT_READ)
        finished = False
        while not finished:
            try:
                for key, _ in sel.select():
                    data = key.fileobj.read1().decode().strip()
                    if data:
                        if key.fileobj is self._process.stdout:
                            self.on_stdout(data)
                        else:
                            self.on_stderr(data)
                    if not data:
                        finished = True
            except Exception as e:
                self.app.logger.error(f"Error collecting outputs for {self}: {e}") if self.app else print(f"Error collecting outputs for {self}: {e}")
                traceback.print_exc()
                
        return_code = self._process.wait()
        return return_code

def current_services(name=None,app=None):
    if not app:
        app = flask.current_app
    sm = app.extensions['services']
    
    if name:
        if name in sm:
            return sm[name]
        return None
    
    return sm

def start_app(app):
    sm = app.extensions.get('services')
    if sm:
        sm.start()

def stop_app(app, *args, **kwargs):
    sm = app.extensions.get('services')
    if sm:
        sm.kill()

def init_app(app):
    """ServiceManager is created at runtime using signals"""
    

