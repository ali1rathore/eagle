import pytest
import flask
from eagle.extensions.printing import printing
from eagle.extensions.printing import start_app

def test_check_printer_status(create_app):
    app = create_app()
    start_app(app)
    st = printing.e_check_printer_status(app)
    assert st != 'paused'