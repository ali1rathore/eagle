import flask
import click
import shutil
import pathlib
from .install import install_services, uninstall_services, is_installed, status_service, write_linger_file, write_systemd_files


SYSTEMD_SERVICE_COMMAND_TEMPLATE="XDG_RUNTIME_DIR=/run/user/{uid} systemctl --user {command} {service}"
SYSTEMD_SERVICE_TEMPLATE="""
[Unit]
Description={Description}
Wants=network-online.target
After=network-online.target

[Service]
ExecStart={ExecStart}
WorkingDirectory={WorkingDirectory}
StandardOutput=inherit
StandardError=inherit
Restart=always

[Install]
WantedBy={WantedBy}
"""


bp = flask.Blueprint('systemd',__name__)
bp.cli.help = 'Commands for systemd'

@bp.cli.command('install')
@click.option("--dry-run",is_flag=True,default=False)
def install(dry_run):
    """
    Install all systemd services
    """
    app = flask.current_app
    services = app.config.get('SERVICES',[])
    all_services = app.config.get('SERVICES_LIST',{})
    added = install_services(app,services,all_services,dry_run=dry_run)
    click.echo(f"Added service files: {','.join(added)}")



@bp.cli.command('uninstall')
def uninstall():
    """
    Uninstall all systemd services
    """
    removed = uninstall_services(flask.current_app)
    click.echo(f"Removed service files: {','.join(removed)}")


@bp.cli.command('check')
def check():
    """
    Check installed all systemd services
    """
    services = flask.current_app.config.get('SERVICES',[])
    for s in services:
        status_service(s)
    installed = is_installed(flask.current_app)
    click.echo(installed)

def start_app(app):
    with app.app_context():
        install_services(app,dry_run=(not shutil.which("systemctl")))

def init_app(app:flask.Flask):

    app.config.from_object(__name__)

    if not app.testing and not shutil.which("systemctl"):
        app.logger.debug(f"Command 'systemctl' not found. Disabling 'systemd' extension.")
        return

    app.register_blueprint(bp)
