import flask
from . import sse, chat
from flask import Flask, stream_with_context, current_app
from time import sleep

def get_publisher(app:Flask=None):
    if not app:
        app = current_app._get_current_object()
    return app.extensions['sse']

bp = flask.Blueprint('sse',__name__,template_folder='templates',url_prefix='/events')

def stream_template(template_name, **context):
    flask.current_app.update_template_context(context)
    t = flask.current_app.jinja_env.get_template(template_name)
    rv = t.stream(context)
    rv.disable_buffering()
    return rv

data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
def generate():
    for item in data:
        yield str(item)
        sleep(1)

@bp.route('/stream')
def stream_view():
    rows = ([r] for r in get_publisher().subscribe())# generate()
    return flask.Response(stream_with_context(stream_template('sse/template.html', rows=rows)))

@bp.route('/')
def events():
    return flask.render_template(['sse/table.html','sse/index.html'])

@bp.route('/ping')
def ping():
    get_publisher().publish(data='PONG')
    return {}, 200

@bp.route('/listen', methods=['GET'])
def listen():
    rows = get_publisher().subscribe()
    return flask.Response(rows, mimetype='text/event-stream')

def init_app(app:flask.Flask):
    app.register_blueprint(bp)
    publisher = sse.Publisher()
    app.extensions['sse'] = publisher
    chat.init_app(app)
