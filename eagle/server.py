import subprocess, shutil
import logging, os, sys
import flask
from eagle.app import create_app as create_eagle
from eagle.models import append_transaction_to_db
from eagle.modules.credits import add_credits_requested
from eagle.extensions import thingsboard, zeroconf, litestream
from eagle.extensions import startup
from eagle.extensions import sse,services
from eagle.extensions import sentry
from eagle.extensions import printing, update, executor, kiosk
from eagle.modules import telemetry, attributes, configuration, machines, transactions, fetching, processes, server, workers, pulse
from eagle import models
from eagle.config import get_platform, get_my_ip, get_hostname
from time import sleep

def generate_banner(app):
    banner = f"""
    Starting {app.name} {app.config['VERSION']} 
    Revision: {app.config['COMMIT_ID']}
    Instance: {app.instance_path}
    Config: {app.config.get('EAGLE_CONFIG')}
    Role: {app.config.get('ROLE')}
    Database: {app.config['DATABASE']}
    SQLA: {app.config['SQLALCHEMY_DATABASE_URI']}
    Debug: {app.debug} 
    Env: {app.env}
    """
    print(banner)
    return banner

def setup_port_forwarding(app):
    src = app.config.get('PUBLIC_PORT')
    if not src:
        return
    dst = app.config['PORT']
    if src == dst or not shutil.which('iptables'):
        return

    try:
        subprocess.Popen(f"sudo iptables -t nat -A PREROUTING -p tcp --dport {src} -j REDIRECT --to-port {dst}".split()).wait()
        subprocess.Popen(f"sudo iptables -t nat -I OUTPUT -p tcp -d 127.0.0.1 --dport {src} -j REDIRECT --to-ports {dst}".split()).wait()
    except Exception as e:
        app.logger.error(f"Failed forwarding port {src} to {dst}: {e}")
    else:
        app.logger.debug(f"Setup port forwarding from {src} to {dst}")

def do_shutdown(delay_s=5):
    from time import sleep
    sleep(delay_s)
    import os, signal
    os.kill(os.getpid(), signal.SIGKILL)

def create_shutdown_job(app,delay_s=5):
    if flask.has_request_context():
        flask.flash(message=f"System is shutting down",category="danger")

    with app.app_context():
        app.logger.debug(f"Shutting down system in {delay_s} seconds")
    from threading import Thread
    platform = get_platform()
    if platform != 'arm':
        t = Thread(target=do_shutdown,name="Updating thread")
    else:
        t = Thread(target=do_reboot,name="Updating thread")
    t.daemon = True
    t.start()
    app.extensions['shutdown'] = t

def do_reboot(delay_s=5):
    from time import sleep
    sleep(delay_s)
    subprocess.run("sudo reboot", shell=True, check=True)

def set_hostname(new_name):
    if get_platform() == 'arm':
        HERE = os.path.dirname(__file__)
        CHANGE_HOSTNAME = os.path.join(HERE, 'bin/change_hostname.sh')
        if shutil.which('sudo') == None:
            return
        output=subprocess.run(["sudo", CHANGE_HOSTNAME, new_name])
        if output.returncode != 0:
            raise RuntimeError('Hostname change failed')


def send_pulses(app :flask.Flask, amount: int, **kwargs):

    delay = app.config.get("DELAY")

    if not delay:
        app.logger.debug(f"DELAY not set for role {app.config['ROLE']}. Not sending pulses")
        return
    
    pulses = pulse.send_pulses(amount,delay)
    app.logger.debug(f'send {pulses} pulses with delay = {delay}')

@startup.on_start.connect
def check_hostname(app):
    current_hostname = get_hostname()
    desired_hostname = app.config['HOSTNAME']
    if desired_hostname != current_hostname:
        set_hostname(desired_hostname)

startup.on_start.connect(workers.start_app)
startup.on_stop.connect(workers.stop_app)

startup.on_start.connect(executor.start_app)
startup.on_stop.connect(executor.stop_app)

startup.on_stop.connect(services.stop_app)
startup.on_start.connect(services.start_app)

server.signals.shutdown_requested.connect(create_shutdown_job)

startup.on_start.connect(generate_banner)
startup.on_start.connect(setup_port_forwarding)
startup.on_start.connect(telemetry.start_app)

startup.on_start.connect(printing.start_app)
startup.on_start.connect(kiosk.start_app)



@models.transaction_stored.connect
def print_transaction(app,transaction:transactions.model.Transaction):
    if transaction.amount < 0:
        text = transactions.view.render_receipt(transaction)
        printing.on_print.send(app,text=text)

startup.on_start.connect(sentry.start_app)

def publish_sse(app,*args,**kwargs):
    sse.get_publisher(app).publish(data=kwargs)

@models.transaction_stored.connect
def publish_transaction(app,transaction):
    sse.get_publisher(app).publish(data=transaction.to_json(),channel='Transactions')
    
for e in [machines.model.machine_added,machines.model.machine_removed]:
    e.connect(publish_sse)

telemetry.on_send.connect(publish_sse)
@telemetry.on_send.connect
def publish_tb_if_configured(app,**data):
    if not app.config.get('TB_DEVICE_TOKEN'):
        return
    
    executor = app.extensions.get('executor')
    if not executor:
        return
    executor.submit(thingsboard.publish_to_thingsboard,app,**data)
    #thingsboard.publish_to_thingsboard(app,**data)

@startup.on_start.connect
def register_app(app):
    zeroconf.start_app(app)
    startup.on_stop.connect(zeroconf.stop_app)
    executor = app.extensions.get('executor')
    if not executor:
        return

    app.logger.info(f'Adding zeroconf registration job')

    executor.submit(zeroconf.start_discovery,app)
    #zeroconf.start_register(app,ip=get_my_ip(),properties=attributes.get_attributes(app))
    executor.submit(zeroconf.start_register,app,ip=get_my_ip,properties=attributes.get_attributes)


zeroconf.service_added.connect(machines.model.add_machine)
zeroconf.service_removed.connect(machines.model.remove_machine)

def fetch_and_write():
    app = flask.current_app._get_current_object()
    executor = app.extensions.get('executor')
    try:
        for t in fetching.fetch_transactions():
            fetching.transactions_fetched.send(app,transactions=[t])
    except Exception as e:
        flask.current_app.logger.error(f"Failed to fetch transactions: {e}")
    if executor.__proxied_object._shutdown:
        return
        
    sleep(flask.current_app.config['FETCH_EVERY_S'])
    executor.submit(fetch_and_write)

def add_fetch_transactions(app):
    if app.config.get('ROLE') != 'POS':
        return

    executor = app.extensions.get('executor')
    if not executor:
        return

    app.logger.info(f'Adding fetch_and_write job')

    executor.submit(fetch_and_write)

def send_telemetry_and_log_errors():
    """
    Send telemetry. Added by 'telemetry' module
    """
    from time import sleep
    app=flask.current_app._get_current_object()
    executor = app.extensions.get('executor')

    tele, exceptions = telemetry.send_telemetry(app)
    for funcname,e in exceptions.items():
        app.logger.debug(f'Error sending telemetry for {funcname}: {e}')

    if executor.__proxied_object._shutdown:
        return
    sleep(app.config['TELEMETRY_EVERY_S'])
    executor.submit(send_telemetry_and_log_errors)

@startup.on_start.connect
def send_scheduled_telemetry(app):

    from time import sleep
    executor = app.extensions.get('executor')
    app = flask.current_app._get_current_object()
    if not executor:
        return

    executor.submit(send_telemetry_and_log_errors)


@startup.on_start.connect
def connect_to_thingsboard(app):
    executor = app.extensions.get('executor')
    if not executor:
        return
    
    def finish_publish(f):
        app.logger.info('Finished connecting to TB!')
    fut = executor.submit(thingsboard.start_publish,app,attributes=attributes.get_attributes)
    fut.add_done_callback(finish_publish)

@startup.on_start.connect
def start_litestream(app):
    if not app.config.get('LITESTREAM_ENABLED',False):
        return
    sm = app.extensions['services']
    ls = services.Service(litestream.litestream_cmd(app),app=app,name='litestream')
    sm['litestream'] = ls
    ls.stdout_event.connect(lambda sender,line:app.logger.info(f"Output from {sender.name}: {line}"))
    ls.stderr_event.connect(lambda sender,line:app.logger.error(f"Error from {sender.name}: {line}"))
    ls.start()

#@startup.on_start.connect
def start_database_writer(app):
    executor = app.extensions.get('executor')
    if not executor:
        return
    
    def finish_writing(f):
        app.logger.info('Database writer was shutdown, resubmitting task')
        fut = executor.submit(models.write_database_forever,app)
        fut.add_done_callback(finish_writing)

    fut = executor.submit(models.write_database_forever,app)
    fut.add_done_callback(finish_writing)

@update.update_requested.connect
def start_upgrade_service(app,version):
    latest = version
    if version == None:
        versions = update.fetch_available_versions(app)
        latest = versions[0]['name']

    cmd = update.pip_uninstall_command(app)
    uninstaller = app.extensions['services']['uninstaller'] = services.Service(cmd,app=app,name='uninstaller')
    def do_install_if_uninstalled(returncode):
        if returncode != 0:
            app.logger.error(f"Not running upgrade since uninstall failed with returncode: {returncode}.  See {app.config['PIP_INSTALL_LOGFILE']} for details")
            return
        cmd = update.pip_install_command(app,version=latest)
        updater = app.extensions['services']['updater'] = services.Service(cmd,app=app,name='updater')
        updater.start()
    uninstaller.on_exit = do_install_if_uninstalled
    uninstaller.start()

@thingsboard.mqtt.firmware_updated.connect
def after_firmware_updated(app,commit_id,result):

    app.logger.info(f'{app.name} updated to {commit_id}')
    if not result:
        create_shutdown_job(app)


@thingsboard.on_provision_result.connect
def update_instance_config(app,status,creds):
    if not creds:
        return

    val = configuration.ConfigModel(name='TB_DEVICE_TOKEN',value=creds)
    item = configuration.views.InstanceConfigView.datamodel.edit(val)
    app.config.update(item)

import atexit

@startup.on_start.connect
def handle_app_exit(app):
    def OnExitApp():
        app.logger.debug(f"Exitting {app.config['APP_NAME']}")
        startup.on_stop.send(app)
    atexit.register(OnExitApp)

@processes.signals.service_started.connect
def flash(app,service):
    if flask.has_request_context():
        flask.flash(f"Starting service '{service.command}'",category='danger')

@startup.on_start.connect
def connect_terminal(app:flask.Flask):

    @app.before_first_request
    def after_request():
        service = services.Service(app.config['SERVICES_LIST']['xterm'],app=app,name='xterm',restart=False)
        service.on_stderr = service.on_stdout = app.logger.info
        app.extensions['services']['xterm']  = service

def create_app(**kwargs):

    app = create_eagle(**kwargs)
    init_app(app)
    app.logger.debug(f'\n>>>>> Starting services\n')
    with app.app_context():
        startup.on_start.send(app)
    app.logger.debug(f'\n<<<<< Started services\n')
    return app

def create_server():
    app = create_app()

    if os.environ.get('SERVER_SOFTWARE','').startswith('gunicorn'):
        app.logger.debug(f'Setting up error logging for gunicorn')
        gunicorn_logger = logging.getLogger('gunicorn.error')
        app.logger.handlers = gunicorn_logger.handlers
        app.logger.setLevel(gunicorn_logger.level)

    return app.wsgi_app

def init_app(app):
    """_summary_
    Connect conditional signals
    Args:
        app (_type_): _description_
    """
    if app.config['ROLE'] != 'POS':
        add_credits_requested.connect(send_pulses)
        add_credits_requested.connect(append_transaction_to_db)

    if app.config.get('ENABLE_BOOST')!=True:
        startup.on_start.connect(add_fetch_transactions)

    printing.on_print.connect(printing.printing.e_send_to_printer)

import gunicorn.app.base
class StandaloneApplication(gunicorn.app.base.BaseApplication):

    def __init__(self, app, options=None):
        self.options = options or {}
        self.application = app
        super().__init__()

    def load_config(self):
        config = {key: value for key, value in self.options.items()
                  if key in self.cfg.settings and value is not None}
        for key, value in config.items():
            self.cfg.set(key.lower(), value)

    def load(self):
        return self.application


if __name__ == '__main__':
    
    app = create_app()
    options = {
        'bind': '%s:%s' % (app.config['HOST'], app.config['PORT']),
        'workers': 1,
        'worker_class': 'sync',
        'loglevel': "DEBUG",
        'timeout':6,
        'preload_app': True
    }
    app.logger.debug(f'Setting up error logging for gunicorn')
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel('DEBUG')

    StandaloneApplication(app.wsgi_app, options).run()