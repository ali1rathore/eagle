from . import model, view

def init_app(app):
    model.init_app(app)
    view.init_app(app)