#!/usr/bin/env python3
import time
import logging
import sqlite3
import sys
import os


def sprint(hx):
    return
    print(hx.hex() + " ", file=sys.stderr, end="")
    if hx > b"\x1f" and hx < b"\x7f":
        print(hx.decode(), file=sys.stderr, end="")
    print(" ", file=sys.stderr)
    sys.stderr.flush()


def run():
    use_stdin = False
    if len(sys.argv) < 2:
        print(f"Usage: {sys.argv[0]} DATABASE", file=sys.stderr)
        exit(1)

    if len(sys.argv) > 2 and sys.argv[2] == "-":
        use_stdin = True
        print("will use stdin", file=sys.stderr)

    db_path = sys.argv[1]
    conn = sqlite3.connect(db_path)
    c = conn.cursor()

    import serial

    if not use_stdin:
        inp = serial.Serial("/dev/ttyS0", 38400, parity=serial.PARITY_NONE)
        outp = inp
    else:
        outp = open(os.devnull, "wb")
        inp = sys.stdin.buffer
    while 1:
        x = inp.read(1)
        sprint(x)
        if x == b"\x1b":
            outp.write(b"\x00")
            inp.read(1)
            sprint(x)
            print("sent 0", file=sys.stderr)
            continue
        if x == b"\x05":
            x = inp.read(1)
            sprint(x)
            if x != b"\x18":
                continue
            scrape_str = ""
            while 1:
                x = inp.read(1)
                sprint(x)
                if x == b"\x00":
                    if scrape_str == "":
                        break
                    else:
                        print(scrape_str, file=sys.stderr)
                        if "AMOUNT" in scrape_str:
                            cash_out = int(float(scrape_str[8:]) * -100)
                            print(cash_out)
                            sys.stdout.flush()

                            logging.debug("cash out = " + str(cash_out))
                            try:
                                c.execute(
                                    "insert into ledger (amount,timestamp) VALUES (%s,%s)"
                                    % (str(cash_out), time.time())
                                )
                            except sqlite3.OperationalError:
                                print(
                                    "exception  sqlite3.OperationalError",
                                    file=sys.stderr,
                                )
                            conn.commit()

                        break
                scrape_str += x.decode("ascii")


if __name__ == "__main__":
    run()