from typing import Optional
import flask
from .mqtt import FlaskTBBMqttClient
from .tb_mqtt_client import IncorrectProtocolVersion, BadUsernameOrPassword, ServerUnavailable, NotAuthorized, InvalidClientIdentifier
import os
import socket
import bcrypt, base64
import threading
import paho.mqtt.client as paho
import pathlib
from .tb_mqtt_client import log, RESULT_CODES
from json import loads, dumps
from blinker import signal
import queue, time

CONNECT_RETRY_DELAY=30

on_provision_result = signal('on-provision-result')

def get_client(app=None,new=False,raise_error=False) -> Optional[FlaskTBBMqttClient]:
    if not app:
        app = flask.current_app

    c = app.extensions.get('tb_mqtt_client')

    host = app.config.get('TB_DEVICE_HOST')
    token = app.config.get('TB_DEVICE_TOKEN')
    if not c or new:
        if not host or not token:
            app.logger.warning(f'Missing configuration TB_DEVICE_HOST or TB_DEVICE_TOKEN.  Unable to create MQTT Client')
            return None

        c = FlaskTBBMqttClient(host=host,token=token)
        c.init_app(app)
        app.logger.debug(f'Created new MQTT Client: {c._client._client_id} in thread {threading.current_thread().ident}')
        c.set_server_side_rpc_request_handler(FlaskTBBMqttClient.rpc_handler)
        
    def on_connected(client,userdata,flags,result_code,*args,**kwargs):
        if result_code == 0:
            app.logger.debug(f'Connected to host {host} {args} {kwargs}: rc={result_code} flags={flags} userdata={userdata}')
        elif result_code in tb_mqtt_client.RESULT_CODES:
            result = tb_mqtt_client.RESULT_CODES[result_code]
            app.logger.warn(f'Error connecting to TB {host} {args} {kwargs}: rc={result_code} ({result}) flags={flags} userdata={userdata}. Disabling client')
            client._client.loop_stop()
        else:
            app.logger.debug(f'Unkown result_code "{result_code}" when connecting to host {host} {args} {kwargs}: rc={result_code} flags={flags} userdata={userdata}')

    while not c.is_connected():
        try:
            c.connect(callback=on_connected,timeout=5)
        except socket.timeout as e:
            app.logger.debug(f'Timed out while connecting client to TB Host: {host}. Retrying in {CONNECT_RETRY_DELAY} seconds: {e}')
            time.sleep(CONNECT_RETRY_DELAY)
        except NotAuthorized as e:
            app.logger.warning(f'Unable to connect to TB. TB_DEVICE_TOKEN not accepted: {e}')
            if not raise_error:
                return None
            raise
        except socket.gaierror as e:
            app.logger.warning(f'Unable to connect to TB at {host}, probably due to no internet. Retrying in {CONNECT_RETRY_DELAY} seconds: {e}')
            time.sleep(CONNECT_RETRY_DELAY)
        except Exception as e:
            app.logger.debug(f'Unable to connect to TB at {host}. Unknown error: {e}')
            if not raise_error:
                return None
            raise

    if not new:
        app.extensions['tb_mqtt_client'] = c

    return c

def get_access_token(app:flask.Flask)->str:
    if 'TB_SECRET_SALT' not in app.config:
        return None

    salt = base64.b64decode(app.config['TB_SECRET_SALT'].encode())
    mac = app.config['HOST_ID'].encode()
    # Hash a password for the first time, with a randomly-generated salt
    hashed = bcrypt.hashpw(mac, salt)
    return hashed.decode()

def start_publish(app:flask.Flask,attributes=None):
    access_token = app.config.get('TB_DEVICE_TOKEN')
    if not access_token:
        app.logger.debug(f'Configuration for TB_DEVICE_TOKEN not found. Trying to provision device')
        status, creds = provision(app)

    if 'TB_DEVICE_TOKEN' not in app.config:
        app.logger.warning(f'Config "TB_DEVICE_TOKEN" not set. Disabling thingboard extension')
        return

    client = get_client(app)
    if not client:
        app.logger.warn(f"Failed creating TB client to Thingsboard. Not starting publisher")
        

    if client and attributes:
        attributes = attributes(app) if callable(attributes) else attributes
        client.send_attributes(attributes,quality_of_service=1)

        client.start_firmware_update()

    q = app.extensions.get('telemetry')
    if not q:
        return
    while True:
        item = q.get()
        _publish_to_thingsboard(app,client,**item)
        q.task_done()
        
    return True

def start_app(app:flask.Flask):
    pass

class ProvisionClient(paho.Client):
    PROVISION_REQUEST_TOPIC = "/provision/request"
    PROVISION_RESPONSE_TOPIC = "/provision/response"

    def __init__(self, host, port, provision_request):
        super().__init__()
        self._host = host
        self._port = port
        self._username = "provision"
        self.on_connect = self.__on_connect
        self.on_message = self.__on_message
        self.__provision_request = provision_request

    def __on_connect(self, client, userdata, flags, rc):  # Callback for connect
        if rc == 0:
            log.info("[Provisioning client] Connected to ThingsBoard ")
            client.subscribe(self.PROVISION_RESPONSE_TOPIC)  # Subscribe to provisioning response topic
            provision_request = dumps(self.__provision_request)
            log.info("[Provisioning client] Sending provisioning request %s" % provision_request)
            client.publish(self.PROVISION_REQUEST_TOPIC, provision_request)  # Publishing provisioning request topic
        else:
            log.info("[Provisioning client] Cannot connect to ThingsBoard!, result: %s" % RESULT_CODES[rc])

    def __on_message(self, client, userdata, msg):
        decoded_payload = msg.payload.decode("UTF-8")
        log.info("[Provisioning client] Received data from ThingsBoard: %s" % decoded_payload)
        decoded_message = loads(decoded_payload)
        provision_device_status = decoded_message.get("status")
        if provision_device_status == "SUCCESS":
            self.__credentials = decoded_message["credentialsValue"]
        else:
            log.debug("[Provisioning client] Provisioning was unsuccessful with status %s and message: %s" % (
                provision_device_status, decoded_message["errorMsg"]))
        self.disconnect()

    def provision(self):
        log.info("[Provisioning client] Connecting to ThingsBoard")
        self.__credentials = None
        self.connect(self._host, self._port, 60)
        self.loop_forever()

    def get_credentials(self):
        return self.__credentials

def provision(app:flask.Flask):

    host_id = app.config['HOST_ID']
    creds = None
    errors, status = None, None

    access_token = get_access_token(app)
    if not access_token:
        return "MISSING_SALT",None

    app.logger.debug(f'Provisioning device {host_id} with token {access_token}')
    host = app.config['TB_DEVICE_HOST']
    try:
        status,creds,errors = FlaskTBBMqttClient.provision(
            host=host,
            provision_device_key=app.config['TB_DEVICE_PROVISION_KEY'],
            provision_device_secret=app.config['TB_DEVICE_PROVISION_SECRET'],
            device_name=host_id,
            access_token=access_token,
        )
    except socket.timeout as e:
        app.logger.warning(f"Timed out while trying to provision device to '{host}': {e}")
        status = 'TIMED_OUT'
    except socket.gaierror as e:
        app.logger.warning(f'Unable to connect to TB, probably due to no internet: {e}')
        status = 'NO_INTERNET'
    except Exception as e:
        app.logger.warning(f"Failed provisionning device: {e}")
        status = 'UNKOWN_ERROR'

    app.logger.debug(f'Provisioning Result: status={status} creds={creds} errors={errors}')

    if status == 'FAILURE':
        app.logger.warning(f"Failed provisionning device. Already provisioned: {errors}")
        creds = access_token
        
    elif status == 'NOT_FOUND':
        app.logger.warning(f"Failed provisionning device due to Invalid key or secret: {errors}")

    on_provision_result.send(app,status=status,creds=creds)

    return status,creds

def init_app(app:flask.Flask):

    host = app.config.setdefault('TB_DEVICE_HOST','thingsboard.cloud')
    if not host:
        return
    if 'TB_DEVICE_NAME' not in app.config:
        app.config['TB_DEVICE_NAME'] = app.config['HOST_ID']

    if 'TB_DEVICE_CLAIM_DURATION' not in app.config:
        app.config['TB_DEVICE_CLAIM_DURATION'] = 30000

    telemtry_queue = queue.Queue()
    app.extensions['telemetry'] = telemtry_queue

def publish_to_thingsboard(app,**data):
    q = app.extensions.get('telemetry')
    if not q:
        return
    q.put(data)


def _publish_to_thingsboard(app,client,**data):
    c = client
    if c:
        c.send_telemetry(telemetry=data)

if __name__ == "__main__":
    import bcrypt, base64
    salt = bcrypt.gensalt()
    print(base64.b64encode(salt).decode())
