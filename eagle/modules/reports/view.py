from datetime import datetime
from flask_appbuilder import action, expose, has_access
from flask_appbuilder.models.sqla.interface import SQLAInterface
from eagle.extensions import printing
from eagle.extensions.appbuilder import HTMXView, ModelView
from eagle.models import insert_db
from .model import Report, db
import flask


def render_report(report: Report, app:flask.Flask=None,):
    if not app:
        app = flask.current_app
        
    with app.app_context():
        rendered_receipt = flask.render_template('report.j2', report=report,now=datetime.utcnow())

    return rendered_receipt

class ReportsView(ModelView,HTMXView):
    route_base = '/reports'
    list_title = 'Reports'
    datamodel = SQLAInterface(Report)
    list_columns = ["id","amount","created_at"]
    base_permissions = ['can_list','can_show',"can_delete"]
    base_order = ('created_at','desc')
    default_view = 'display'
    @expose('/summary/<report>')
    @expose('/summary/')
    def summary(self,report=None):
        if report:
            r = db.session.query(Report).filter(Report.id==report).first_or_404()
        else:
            r = Report(created_at=datetime.utcnow())
        return flask.render_template('reports.html',report=r)

    @expose('/transactions')
    def transactions(self):
        report = flask.request.args.get('report')
        machine = flask.request.args.get('machine')
        if report:
            r = db.session.query(Report).filter(Report.id==report).first_or_404()
        else:
            r = Report(created_at=datetime.utcnow())
        start,end = r.get_range()
        filters = dict(
            _flt_1_created_at=start,
            _flt_2_created_at=end
        )
        if machine:
            filters['_flt_0_machine_id'] = machine
        url = flask.url_for('TransactionGeneralView.display',**filters)
        return flask.redirect(url)

    links = [
        dict(
            url=lambda pk: flask.url_for('.transactions',report=pk),
            icon="fa fa-table",
            title="Transactions",
        ),
        dict(
            url=lambda pk: flask.url_for('.summary',report=pk),
            icon="fa fa-search",
            title="Summary",
        )
    ]

    @action("view","View",confirmation=None,icon="fa-list",single=True,multiple=False)
    def view_reports(self, item):
        """Shows rendered transaction(s)"""
        if not isinstance(item,list):
            item = [item]

        receipts = []

        for i in item:
            receipt = render_report(i)
            receipts.append(receipt)

        return flask.render_template('base.html',content='<pre>' + '\n'.join(receipts) + '</pre>')

    @action("print","Print","Print report?","fa-print",single=True,multiple=False)
    def print(self, item):
        """Sends the `on_print` signal with the rendered transaction(s)"""
        if not isinstance(item,list):
            item = [item]

        for i in item:
            try:
                report = render_report(i)                
                printing.on_print.send(flask.current_app,text=report)
            except Exception as e:
                flask.flash(f"Unable to print report: {e}",category="danger")
                
        return flask.redirect(self.get_redirect())

    @expose('/new')
    def new(self):
        nr = Report.new_report(db.session)
        if not nr:
            flask.flash(f'No new transactions to report',category='info')
            return flask.redirect(flask.url_for(f'.{self.default_view}'))
        insert_db(nr)
        flask.flash(f"Created New Period",category='success')
        return flask.redirect(flask.url_for(f'.{self.default_view}'))

def init_app(app):
    appbuilder = app.extensions.get('appbuilder')
    if appbuilder:
        appbuilder.add_view(ReportsView, "Reports", icon="fa-table")
        appbuilder.add_link('Summary', href='ReportsView.summary', icon="fa-dollar")

