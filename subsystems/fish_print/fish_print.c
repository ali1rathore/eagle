/* Copyright */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <wiringPi.h>
#include <string.h>
#include <sqlite3.h>
#include "gpio.h"
#include <database.h>

enum mode_list
{
    testing = 2,
    production = 0
};
void loop_count_pulses(void);
int parse_stdin_args(char *argv[]);

int parse_stdin_args(char *argv[])
{
    return atoi(argv[2]);
}

int main(int argc, char **argv)
{
    if (argc < 3)
    {
        fprintf(stderr,
                "Usage: %s DATABASE mode\nwhere mode is:\n\t(0=fish_print, 2=test)\n", argv[0]);
        exit(-1);
    }

    char *DATABASE_NAME = parse_db_args(argv);
    int rc = init_db(DATABASE_NAME);
    int mode = parse_stdin_args(argv);
    setup_wipi();
    if (mode == testing)
    {
        char *test_buf = NULL;
        size_t max_line = 0;
        fprintf(stderr, "Test mode\n");
        while (getline(&test_buf, &max_line, stdin) != -1)
        {
            float cashout;
            if ((cashout = atof(test_buf)) != 0)
            {
                process_cashout(cashout * -100);
            }
            free(test_buf);
            test_buf = NULL;
        }
    }
    else
    {
        fprintf(stderr, "Production mode\n");
        loop_count_pulses();
    }
}
