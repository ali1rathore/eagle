from flask_appbuilder import  action, AppBuilder, expose, has_access
import flask
from flask_appbuilder.baseviews import expose_api
from flask_appbuilder.models.sqla.interface import SQLAInterface
from flask_appbuilder import ModelRestApi
from eagle.modules.transactions.model import Transaction
from blinker import signal
from wtforms import IntegerField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.validators import DataRequired, NumberRange
from flask_appbuilder.fieldwidgets import BS3TextFieldWidget, Select2Widget
from flask_appbuilder.forms import DynamicForm
import flask
from flask_appbuilder import SimpleFormView
from flask_babel import lazy_gettext as _
from eagle.extensions.appbuilder import db
from eagle.modules.machines.model import Machine

add_credits_requested = signal('add-credits-requested',"A request to add credits from the POS was submitted.")

class CreditModelApi(ModelRestApi):
    # TODO Replace this with basic RestView (no model)
    resource_name = 'credits'
    allow_browser_login = True
    datamodel = SQLAInterface(Transaction)
    exclude_route_methods = ("put", "post", "delete", "info","get")
    base_order = ('created_at','desc')

    @expose_api(name='add_credit',url='/add',methods=["POST"],description="Add credits for a game")
    def add_credit(self,**kwargs):
        if flask.current_app.config.get('ROLE') == 'POS':
            return 'Not Found',404
        amount = flask.request.json.get('amount')
        if not amount:
            return flask.jsonify({'status':'error'})
        machine = flask.request.json.get('machine')
        if not machine:
            return flask.jsonify({'status':'error'})
        if machine != flask.current_app.config.get('HOST_ID'):
            return flask.jsonify({'status':'error'})

        add_credits_requested.send(flask.current_app._get_current_object(),amount=int(amount),machine=machine)

        return flask.jsonify({'status':'success'})


def list_machines():
    q = db.session.query(Machine)
    id = flask.request.args.get('id')
    if id:
        q = q.filter(Machine.id==id)
    return q

class AddCash(DynamicForm):

    game = QuerySelectField('Game',
                        description=('Which Game?'),
                        query_factory=list_machines,
                        validators = [DataRequired()],
                        )
    amount = IntegerField(('Amount'),
        description=('How much to add?'), 
        validators = [DataRequired("A valid integer between 1 and 20 is required"), NumberRange(1, 20, "Amount must be between 1 and 20")], widget=BS3TextFieldWidget())

class AddCashView(SimpleFormView):
    form = AddCash
    form_title = 'Add Credits'

    def form_get(self, form):
        pass
        #form.game = form.game.id

    def form_post(self, form):
        machine = form.game.data
        amount = form.amount.data * 100
        add_credits_requested.send(flask.current_app._get_current_object(),machine=machine.id,amount=amount)
        return flask.redirect(self.get_redirect())


def add_credits_to_machine(app,amount,machine=None):
    if app.config['ROLE'] != 'POS':
        return
    if not machine:
        return
    import requests
    machine_obj = db.session.query(Machine).filter(Machine.id==machine).first() or None
    if not machine_obj:
        app.logger.debug(f'Unable to find machine {machine}, ignoring')
        return
        
    machine = machine_obj
    url = f'http://{machine.address}/api/v1/credits/add'
    
    data = {'amount': amount,'machine':machine.id}

    try:
        resp = requests.post(url, json = data)
        if resp.status_code == 200:
            if flask.has_request_context():
                flask.flash(f"Game {machine.role} updated with {amount} credits",category='success')
            app.logger.debug(f'Added {amount} credits to {machine}')
        else:
            if flask.has_request_context():
                flask.flash(f"Failed adding credits to {machine.name}",category='danger')
            app.logger.debug(f'Failed adding {amount} credits to {machine.name}')
    except Exception as e:
        flask.flash(f"Failed adding credits: {e}",category='danger')
        app.logger.debug(f'Failed adding {amount} credits to {machine.name}: {e}')

def init_app(app):
    if not app.config['ENABLE_REMOTE_CREDITS']:
        return
    appbuilder = app.extensions.get('appbuilder')
    
    if app.config['ROLE'] != 'POS':
        if appbuilder:
            appbuilder.add_api(CreditModelApi)
    else:
        if appbuilder:
            appbuilder.add_view_no_menu(AddCashView)
        add_credits_requested.connect(add_credits_to_machine)
