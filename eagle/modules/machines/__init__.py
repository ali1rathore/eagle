import flask
from eagle.extensions.appbuilder import db
from . import model, view


def remote_pos_addresses(app=None):
    """Returns the dict[Machine.id,Machine.address] for all non POS HOST_ID except current_app's"""
    if not app:
        app = flask.current_app
    q = db.session.query(model.Machine.id,model.Machine.address,model.Machine.properties).filter(model.Machine.id!=app.config['HOST_ID'],model.Machine.available==True)

    import json
    result = {}
    for id, address, props in q.all():
        role = json.loads(props)['role']
        if role == 'POS':
            result[id] = address
    return result


def remote_machine_addresses(app=None):
    """Returns the dict[Machine.id,Machine.address] for all non POS HOST_ID except current_app's"""
    if not app:
        app = flask.current_app
    q = db.session.query(model.Machine.id,model.Machine.address,model.Machine.properties).filter(model.Machine.id!=app.config['HOST_ID'],model.Machine.available==True)

    import json
    result = {}
    for id, address, props in q.all():
        role = json.loads(props)['role']
        if role != 'POS':
            result[id] = address
    return result

def init_app(app):
    model.init_app(app)
    view.init_app(app)