import flask
from pathlib import Path
from .tb_mqtt_client import TBDeviceMqttClient, FW_TITLE_ATTR, FW_VERSION_ATTR, FW_URL_ATTR
from .remote_shell import TBRemoteShell
from blinker import signal

firmware_updated = signal('firmware-updated')


class FlaskTBBMqttClient(TBDeviceMqttClient):
    rpc_commands = {}

    def __init__(self, host, token=None, port=1883, quality_of_service=None, chunk_size=0,app: flask.Flask=None):
        super().__init__(host, token, port, quality_of_service, chunk_size)
        self._remote_shell = TBRemoteShell()
        self._app = app
        if self._app:
            self.init_app(app)

    def init_app(self,app):
        self._app = app
        app.extensions['tb_mqtt_client'] = self

    def on_rpc(self,method,*args, **kwargs):
        def inner(func):
            self.rpc_commands[method] = func
            # code functionality here
            self._app.logger.debug(f"Registered RPC for {method} -> {func}")
            #func()
        # returning inner function
        return inner

    def start_firmware_update(self):
        if not self._app.config.get('COMMIT_ID'):
            return
        self._client.subscribe("v2/fw/response/+")
        self.current_firmware_info = {
            "current_" + FW_TITLE_ATTR: self._app.config['APP_NAME'],
            "current_" + FW_VERSION_ATTR: self._app.config['COMMIT_ID']
        }
        self.send_telemetry(self.current_firmware_info)
        self._TBDeviceMqttClient__request_firmware_info()

    def on_firmware_received(self, firmware_info, firmware_data):
        #import urllib.request
        #resp = urllib.request.urlopen(firmware_info[FW_URL_ATTR])
        #firmware_data = resp.read()
        #commit = firmware_data.decode().strip()
        url = firmware_info.get(FW_URL_ATTR)
        output = ''
        if url:
            commit = url.split('@')[-1]
            output = upgrade_to_commit(commit)
            firmware_updated.send(self._app,commit_id=commit,result=output)
                
        elif firmware_data:
            filename = firmware_info.get(FW_TITLE_ATTR,'unknown') + '-' + firmware_info.get(FW_VERSION_ATTR,'unknown') + '.py'
            filepath = Path(self._app.instance_path) / filename
            output = execute_python_file(filepath,firmware_data)
            #if output:
            #    firmware_updated.send(self._app,commit_id=filename,result=output)
                
        version_to = firmware_info.get(FW_VERSION_ATTR)
        self._app.logger.debug(f'Firmware updated to {version_to}: {output}')        
        return output

    def rpc_handler(self,request_id,request_body,*args,**kwargs):
        client = self
        method = request_body['method']
        params = request_body.get('params',{})

        # Check if it's a remote shell command
        if method in self._remote_shell.shell_commands.keys():
            return self._remote_shell.handle_rpc_request(client,request_id,request_body)


        command_to_run = client.rpc_commands.get(method)
        if not command_to_run:
            self._app.logger.warning(f'Unable to run command {method}, not registered')
            return
        result = None
        if not params:
            result = command_to_run()
        elif isinstance(params,dict):
            result = command_to_run(**params)
        elif isinstance(params,list):
            result = command_to_run(*params)
        elif isinstance(params,str):
            result = command_to_run(params)
        else:
            self._app.logger.warning(f'Unaccepted paramters: {params} for {method}')

        self.send_rpc_reply(req_id=request_id,resp=result)

        self._app.logger.debug(f"Handled RPC Request: method={method} kwargs={kwargs} result={result}")

def execute_python_file(filepath,firmware_data):
    success = False
    import sys, subprocess, pathlib

    filepath.write_bytes(firmware_data)

    run =  [sys.executable,str(filepath)] 
    run_process = subprocess.run(run,capture_output=True,text=True)
    output = ''
    if run_process.returncode != 0:
        output += run_process.stdout + '\n' + '[ERRORS]\n' + run_process.stderr
        logfile = pathlib.Path(str(filepath) + '.log')
        logfile.write_text(output)
    else:
        success = True

    return success

def upgrade_to_commit(commit):
    import subprocess, shlex, sys
    uninstall =  [sys.executable,'-m','pip','uninstall','--yes','eagle'] 
    install =  [sys.executable,'-m','pip','install','--upgrade',f'git+https://gitlab.com/lakeshorelabs/eagle.git@{commit}'] 
    uninstall_process = subprocess.run(uninstall,capture_output=True,text=True)
    output = ''
    if uninstall_process.returncode != 0:
        output += uninstall_process.stdout + '\n' + '[ERRORS]\n' + uninstall_process.stderr

    install_process = subprocess.run(install,capture_output=True,text=True)
    if install_process.returncode != 0:
        output += install_process.stdout + '\n' + '[ERRORS]\n' + install_process.stderr
    
    if output:
        install =  [sys.executable,'-m','pip','install','--upgrade',f'git+https://gitlab.com/lakeshorelabs/eagle.git@dev'] 
        install_process = subprocess.run(install,capture_output=True,text=True)
        output += '[REINSTALL]\n' + install_process.stdout + '\n' + '[ERRORS]\n' + install_process.stderr

    return output
def init_app(app:flask.Flask):

    client = FlaskTBBMqttClient(host=app.config["TB_DEVICE_HOST"], token=app.config["TB_DEVICE_TOKEN"])
    client.init_app(app)
    client.set_server_side_rpc_request_handler(FlaskTBBMqttClient.rpc_handler)
