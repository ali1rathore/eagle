import flask
import os
try:
    import sentry_sdk
    from sentry_sdk.integrations.flask import FlaskIntegration
except Exception as e:
    sentry_sdk = None

SENTRY_SAMPLE_RATE=0.1

def init_app(app:flask.Flask):
    pass

def start_app(app:flask.Flask):

    if not sentry_sdk:
        app.logger.debug(f"Unable to import sentry_sdk: {e}")
        return

    sentry_dsn = app.config.setdefault('SENTRY_DSN',os.environ.get('SENTRY_DSN'))
    sentry_rate = app.config.setdefault('SENTRY_SAMPLE_RATE', os.environ.get('SENTRY_SAMPLE_RATE',SENTRY_SAMPLE_RATE))

    if not sentry_dsn:
        app.logger.debug(f"Sentry DSN not set: disabling sentry")
        return
    
    @app.route('/sentry-test')
    def sentry_test():
        return 1/0

    appbuilder = app.extensions.get('appbuilder')
    if appbuilder:
        appbuilder.add_link("New Issue",href='sentry_test',icon='fa-fire',category="Developer")
    
    @app.before_request
    def before_request():
        error_id = flask.session.pop('error_id',None)
        if error_id:
            flask.g.error_id = error_id

    @app.errorhandler(500)
    def server_error_handler(error):
        event_id = sentry_sdk.last_event_id()
        flask.session['error_id'] = event_id
        return flask.redirect(flask.request.referrer or '/')

    for key in app.config.get('SENTRY_TAG_KEYS',[]):
        sentry_sdk.set_tag(key.lower(), app.config.get(key))

    sentry_sdk.set_tag('debug', app.debug)

    app.extensions['sentry'] = __package__

    sentry_sdk.init(
        dsn=sentry_dsn,
        server_name=app.config['HOST_ID'],
        integrations=[FlaskIntegration()],
        environment=app.env,

        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production.
        traces_sample_rate=sentry_rate,

        # By default the SDK will try to use the SENTRY_RELEASE
        # environment variable, or infer a git commit
        # SHA as release, however you may want to set
        # something more human-readable.
        release=f"{app.config['APP_NAME']}-{app.config['VERSION']}",
    )
