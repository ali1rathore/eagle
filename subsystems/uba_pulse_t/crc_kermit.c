
#include "crc_kermit.h"


uint16_t crc16(uint8_t byte, uint16_t fcs)
{
	uint8_t bit;

    for(bit=0; bit<8; bit++)
    {
        fcs ^= (byte & 0x01);
        fcs = (fcs & 0x01) ? (fcs >> 1) ^ 0x8408 : (fcs >> 1);
        byte = byte >> 1;
    }
    return fcs;
}


uint16_t crc_kermit(uint8_t* data, uint8_t number_of_bytes)
{
	uint16_t prev_CRC = 0;
	uint8_t j;

	for(j=0; j<number_of_bytes; j++){
		prev_CRC = crc16(*data, prev_CRC);
		data++;
	}

return prev_CRC;
}


uint8_t ckecksum_comparator(uint8_t protocol, uint8_t *DATA){
	uint8_t _lng;
	_checksum_ krmit, rx_crc;

	if(protocol) {	_lng = DATA[2];}else{_lng = DATA[1];}

	krmit.byte1 = DATA[_lng - 2];
	krmit.byte2 = DATA[_lng - 1];

	rx_crc.result = crc_kermit(DATA, _lng - 2);

	if ( rx_crc.result == krmit.result ){
		return 1;
	}else{	return 0; }
}
