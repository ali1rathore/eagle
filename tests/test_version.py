from eagle import __version__

def test_version_cli(runner):
    """The 'version' command should return the correct version"""
    with open(__version__.__file__) as f:
        v = f.read()

    result = runner("version")
    assert v == result.output.strip()

def test_version_route(client):
    with open(__version__.__file__) as f:
        v = f.read()

    result = client.get("/version")
    assert v == result.data.decode()

def test_version_config(create_app):

    app = create_app()
    
    with open(__version__.__file__) as f:
        v = f.read()

    assert app.config['VERSION'] == v
