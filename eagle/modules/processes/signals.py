
import blinker

service_started = blinker.signal('service-started')
service_stopped = blinker.signal('service-stopped')