# -*- coding: utf-8 -*-
# @Author  : ydf
# @Time    : 2019/8/8 0008 13:33
import json

import flask
from ..base.consumer import AbstractConsumer
from . import queue as sqla_queue


class SqlachemyConsumer(AbstractConsumer):
    """
    Sqlachemy implements five kinds of database simulation message queues and supports consumption confirmation.
    """ 

    def serve_forever(self):
        while True:
            sqla_task_dict = self.queue.get()
            kw = {'body': json.loads(sqla_task_dict['body']), 'sqla_task_dict': sqla_task_dict}
            self._execute_task(kw)

    def _confirm_consume(self, kw):
        self.queue.set_success(kw['sqla_task_dict'])

    def _requeue(self, kw):
        self.queue.set_task_status(kw['sqla_task_dict'], sqla_queue.TaskStatus.REQUEUE)


sqla_queue.SqlaQueue.consumer = SqlachemyConsumer