from eagle.extensions import thingsboard
from eagle.modules import testing
import flask
import os, pathlib
import pytest
HERE = pathlib.Path(__file__).parent
PROV_FILE = HERE.parent / 'instance' / 'provision.py'
ACCESS_FILE = HERE.parent / 'instance' / 'thingsboard' / 'access.py'

#@pytest.mark.skipif(False, reason=f"Takes too long to run")
@pytest.mark.skipif(not PROV_FILE.exists(), reason=f"Test provisionning file {PROV_FILE} does not exist")
def test_device_provision_timeout(app:flask.Flask,tmp_path):
    """provision a device"""
    app.config.from_pyfile(PROV_FILE.absolute())
    app.config['TB_ACCESS_FILE'] = tmp_path / 'access.py'
    app.config['TB_DEVICE_HOST'] = 'example.com'
    status,result = thingsboard.provision(app)
    assert not result
    assert status == 'TIMED_OUT'

@pytest.mark.skipif(not PROV_FILE.exists(), reason=f"Test provisionning file {PROV_FILE} does not exist")
def test_device_client_timeout(app:flask.Flask):
    """provision a device"""
    app.config.from_pyfile(PROV_FILE.absolute())
    app.config['TB_DEVICE_HOST'] = 'example.com'

    c = thingsboard.get_client(app)
    assert not c

@pytest.mark.skipif(not PROV_FILE.exists(), reason=f"Test provisionning file {PROV_FILE} does not exist")
def test_device_bad_provision_key(app:flask.Flask,tmp_path):
    """provision a device"""
    app.config.from_pyfile(PROV_FILE.absolute())
    app.config['TB_ACCESS_FILE'] = tmp_path / 'access.py'
    app.config['TB_DEVICE_PROVISION_KEY'] = 'badkey'

    status,creds = thingsboard.provision(app)
    assert status == 'NOT_FOUND' and creds is None

@pytest.mark.skipif(not PROV_FILE.exists(), reason=f"Test provisionning file {PROV_FILE} does not exist")
def test_device_bad_provision_secret(app:flask.Flask,tmp_path):
    """provision a device"""
    app.config.from_pyfile(PROV_FILE.absolute())
    app.config['TB_ACCESS_FILE'] = tmp_path / 'access.py'
    app.config['TB_DEVICE_PROVISION_SECRET'] = 'badsecret'

    status,creds = thingsboard.provision(app)
    assert status == 'NOT_FOUND' and creds is None


@pytest.mark.skipif(not PROV_FILE.exists(), reason=f"Test provisionning file {PROV_FILE} does not exist")
def test_device_already_provisioned(app:flask.Flask,tmp_path):
    """provision a device"""
    app.config.from_pyfile(PROV_FILE.absolute())
    app.config['TB_ACCESS_FILE'] = tmp_path / 'access.py'

    status,creds = thingsboard.provision(app)
    assert status == 'FAILURE' and creds is None


@pytest.mark.skipif(not PROV_FILE.exists() or not ACCESS_FILE.absolute().exists(), reason=f"Test provisionning file {PROV_FILE} does not exist")
def test_device_client_connected(app:flask.Flask):
    """provision a device"""
    app.config.from_pyfile(PROV_FILE.absolute())
    app.config.from_pyfile(ACCESS_FILE.absolute())
    app.config["TB_DEVICE_HOST"]="thingsboard.cloud"

    c = thingsboard.get_client(app)
    assert c.is_connected()


@pytest.mark.skipif(not PROV_FILE.exists(), reason=f"Test provisionning file {PROV_FILE} does not exist")
def test_device_client_bad_token(app:flask.Flask):
    """Connected with a bad token should result in no client getting created"""
    app.config.from_pyfile(PROV_FILE.absolute())
    app.config['TB_DEVICE_TOKEN'] = 'badtoken'
    with pytest.raises(thingsboard.tb_mqtt_client.NotAuthorized) as e_info:
        thingsboard.get_client(app,raise_error=True)


@pytest.mark.skipif(not PROV_FILE.exists(), reason=f"Test provisionning file {PROV_FILE} does not exist")
def test_device_provisions_on_start(create_app):
    """Create created"""
    app = create_app(TB_ACCESS_FILE='badaccessfile.py',EAGLE_CONFIG=PROV_FILE.absolute())
    with testing.captured_signals(app,thingsboard.on_provision_result) as events:
        thingsboard.start_app(app)
        assert len(events)==1
        assert thingsboard.on_provision_result in events[0]
