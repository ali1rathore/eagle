from setuptools import find_packages, setup
import os


def get_version():
    HERE = os.path.dirname(__file__)
    with open(os.path.join(HERE, NAME, "__version__.py")) as f:
        return f.read().strip()


NAME = "eagle"
VERSION = get_version()


def package_files(directory):
    paths = []
    for (path, directories, filenames) in os.walk(directory):
        for filename in filenames:
            paths.append(os.path.join("..", path, filename))
    return paths


REQUIREMENTS = [
    "bcrypt==3.2.0",
    "get-mac==0.8.3",
    "flask==2.0.3",
    "python-dotenv==0.19.2",
    "blinker==1.4",
    "platformdirs==2.4.0",
    "psutil==5.9.0",
    "requests==2.25.1",
    "pyserial==3.5",
    "mmh3", # needed for tb ota
    'paho-mqtt', 'jsonschema', 'requests', # needed for tb mqtt
    "flask-theme-adminlte3", "flask-gravatar",
    "zeroconf",
    "flask-appbuilder==4.1.3",
    "requests-futures",
    "flask-executor==1.0.0",
    "flask-htmx",
    "flask-unleash",
    "flask-socketio",
    "boto3",
    "persist-queue"
]

CODEGEN_REQUIREMENTS = [
    "organize-tool",
    "cogapp",
    "python-box",
    "prance[osv]",
    "ruamel.yaml",
    "openapi-spec-validator",
]
DEV_REQUIREMENTS = [
    "pytest","pytest-parallel","pytest-html",
    "sqlite-web",
    "click-web",
    "flask-debugtoolbar",
    "pre-commit",
    "tb-rest-client",
    "pip", "setuptools", "wheel"
] #+ CODEGEN_REQUIREMENTS

PROD_REQUIREMENTS = [
    "sentry-sdk",
    "gunicorn",
]


import subprocess
import sys
import pathlib
from setuptools import setup
from setuptools.command.build_py import build_py
from setuptools.command.build_ext import build_ext

class Build_ext_first(build_py):
    def run(self):
        self.run_command("build_ext")
        return build_py.run(self)


SUBSYSTEMS_DIR = pathlib.Path(__file__).parent / 'subsystems'
SUBSYSTEMS =  [SUBSYSTEMS_DIR / 'wiringPi', SUBSYSTEMS_DIR / 'uba_pulse'] #[SUBSYSTEMS_DIR / 'wiringPi']+list(SUBSYSTEMS_DIR.iterdir())
class BuildExt(build_ext):
    """Customized setuptools install command - builds protos on install."""
    def run(self):
        for s in SUBSYSTEMS:
            print(f"***Making {s}***")
            if subprocess.call("make clean".split(),cwd=s) != 0:
                sys.exit(-1)
            if subprocess.call("make",cwd=s) != 0:
                sys.exit(-1)
        build_ext.run(self)

setup(
    name=NAME,
    version=VERSION,
    packages=find_packages(),
    package_data={NAME: package_files(NAME)},
    include_package_data=True,
    zip_safe=False,
    install_requires=REQUIREMENTS+PROD_REQUIREMENTS,
    extras_require={
        "dev": DEV_REQUIREMENTS,
        "all": DEV_REQUIREMENTS + PROD_REQUIREMENTS,
    },
    entry_points={
        "console_scripts": [
            "eagle=eagle:main"
        ],
    }
#   ,cmdclass={
#        'build_ext': BuildExt,
#        'build_py': Build_ext_first
#    }
)
