/*
 * By SydBernard
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include "id003.h"
#include "crc_kermit.h"
#include <sys/time.h>

_checksum_ Checksum03;

typedef union _indi_
{
    uint8_t byte;
    struct
    {
        uint8_t i0 : 1; // Sync recieved 0xFC
        uint8_t i1 : 1; // Length byte
        uint8_t i2 : 1; // Checksum state
        uint8_t i3 : 1; // CRC passed
        uint8_t i4 : 1;
        uint8_t i5 : 1;
        uint8_t i6 : 1; // Vend valid
        uint8_t i7 : 1; // Bill stacked
    };
} _indi_;

/* Exported variables ------------------------------------------------------------*/
_indi_ indi = {.i0 = 0, .i1 = 0, .i2 = 0, .i3 = 0, .i6 = 0, .i7 = 0};

uint8_t id003_tx_string[100];
uint8_t id003_rx_string[250];

uint8_t n, lngtrm, bill;;
void serialFlush (const int fd);
extern int s, sout;
extern int val, mode;

/* ---------------------------------------------------------------------------*/
// Send to serial port macros
#define TRMIT_CMD(_cmd_)             \
    id003_format(0x05, _cmd_, DATA); \
    write(sout, DATA, 0x05);         \

#define TRMIT_CMD2(_cmd_, _lng_)      \
    DATA[3] = 0;                      \
    DATA[4] = 0;                      \
    id003_format(_lng_, _cmd_, DATA); \
    write(sout, DATA, _lng_);

/* ---------------------------------------------------------------------------*/
// Format outgoing stream
void id003_format(unsigned char LNG, unsigned char CMD, unsigned char *DATA)
{
    DATA[0] = SYNC;
    DATA[1] = LNG;
    DATA[2] = CMD;

    Checksum03.result = crc_kermit(DATA, LNG - 2);
    DATA[LNG - 2] = Checksum03.byte1;
    DATA[LNG - 1] = Checksum03.byte2;

    return;
}

// Handle data coming from serial stream
void rx_stream_uba(unsigned char temp)
{

    if (indi.i0 == 1)
    {
        // Copy to cmd budder
        id003_rx_string[n++] = temp;
        if (indi.i1 == 1)
        {
            // Handle length
            lngtrm = temp - 2;
            indi.i1 = 0;
            goto salir;
        }
        lngtrm = lngtrm - 1;
        if (lngtrm == 0)
        {
            // Through whole command sequence
            // Get checksum
            indi.i0 = 0;
            n = 0;
            indi.i2 = 1;
            if (ckecksum_comparator(id003, id003_rx_string))
            {
                //   if (id003_rx_string[2] == ESCROW) fprintf(stderr, "[DEBUG][BA] Valid Checksum\n");
                indi.i3 = 1;
//                indi.i0 = 0;
                if (mode == 0)
                {
                    serialFlush(s);
                }

            }
            else
            {
                indi.i3 = 0;
                indi.i2 = 0;
            }
        }
    }
    // Handle sync (0xFC)
    if (((temp == SYNC) && (indi.i0 == 0)))
    {
        indi.i0 = 1;
        indi.i1 = 1;
        n = 0;
        id003_rx_string[n++] = SYNC;
    }
salir:

    return;
}

// Return inserted bill
int loop_id003(void)
{
    int val = -1;
    if (lngtrm != 0)
    {
        return val;
    }

    id003_master(id003_rx_string[2], id003_tx_string);
    if (indi.i7 == 1)
    {
        fprintf(stderr, "[DEBUG][BA] Accept Bill: %02X\n", bill);
        switch (bill)
        {
        case 0x61:
            val = 1;
            break;
        case 0x62:
            val = 2;
            break;
        case 0x63:
            val = 5;
            break;
        case 0x64:
            val = 10;
            break;
        case 0x65:
            val = 20;
            break;
        case 0x66:
            val = 50;
            break;
        case 0x67:
            val = 100;
            break;
        default:
            fprintf(stderr, "[ERROR][BA] Unknown bill value from BA:  %02X\n", bill);
            break;
        }

        indi.i7 = 0;
        bill = 0;
    }
    indi.i2 = 0;
    indi.i3 = 0;

    id003_rx_string[2] = 0;
    return val;
}

/* id003_state_machine master, perform actions depending of id003 commands*/
void id003_master(uint8_t CMD, uint8_t *DATA)
{
    //    if(CMD != 0){
    //    fprintf(stderr, "CMD = %x\n", CMD);
    //    }
    switch (CMD)
    {
    case ACK:
        TRMIT_CMD(STATUS_REQUEST);
        break;
    case INHIBIT:
        fprintf(stderr, "[DEBUG][BA] Inhibit\n");
        TRMIT_CMD(STATUS_REQUEST);
        break;
    case INITIALIZE:
        fprintf(stderr, "[DEBUG][BA] Initializing\n");
        TRMIT_CMD(SSR_VERSION_REQUEST);
        break;
    case IDLING:
        TRMIT_CMD(STATUS_REQUEST);
        break;
    case ACCEPTING:
        fprintf(stderr, "[DEBUG][BA] Accepting\n");
        TRMIT_CMD(STATUS_REQUEST);
        break;
    case ESCROW:
        bill = id003_rx_string[3]; // Escrow state, copy bill indicator (fourth byte)
        fprintf(stderr, "[DEBUG][BA] Escrow %02X\n", bill);
        TRMIT_CMD(STACK1);
        break;
    case STACKING:
        fprintf(stderr, "[DEBUG][BA] Stacking\n");
        TRMIT_CMD(STATUS_REQUEST);
        break;
    case VEND_VALID:
        indi.i6 = 1; // Valid Vend state (next is Stacked)
        fprintf(stderr, "[DEBUG][BA] Valid Vend\n");
        TRMIT_CMD(ACK);
        break;
    case STACKED:
        fprintf(stderr, "[DEBUG][BA] Stacked\n"); // Bill is stacked (state complete)
        if (indi.i6 == 1)                         // Make sure Vend Valid was set
        {
            indi.i6 = 0;
            indi.i7 = 1;
        }
        TRMIT_CMD(STATUS_REQUEST);
        break;
    case REJECTING:
        fprintf(stderr, "[DEBUG][BA] Rejecting\n");
        break;
    case RETURNING:
        break;
    case STACKER_FULL:
        break;
    case JAM_IN_ACCEPTOR:
        break;
    case SSR_INHIBIT_ACCEPTOR:
        fprintf(stderr, "[DEBUG][BA] SSR Inhibit Acceptor\n");
        TRMIT_CMD(STATUS_REQUEST);
        break;
    case CMD_INHIBIT_ACCEPTOR:
        fprintf(stderr, "[DEBUG][BA] CMD Inhibit Acceptor\n");
        TRMIT_CMD(SSR_INHIBIT_ACCEPTOR);
        break;
    case POWER_UP:
        fprintf(stderr, "[DEBUG][BA] Power Up\n");
        TRMIT_CMD(RESET_);
        break;
    case POWER_UP_WITH_BILL_IN_ACCEPTOR:
    case POWER_UP_WITH_BILL_IN_STACKER:

        break;
    case SSR_VERSION_INFORMATION:
        TRMIT_CMD(SSR_BOOT_VERSION_REQUEST);
        break;
    case SSR_EN_DIS_DENOMI:
        TRMIT_CMD(SSR_SECURITY_DENOMI);
        break;
    case SSR_SECURITY_DENOMI:
        TRMIT_CMD(SSR_OPTIONAL_FUNCTION);
        break;
    case SSR_OPTIONAL_FUNCTION:
        TRMIT_CMD(SSR_DIRECTION);
        break;
    case SSR_DIRECTION:
        TRMIT_CMD2(CMD_INHIBIT_ACCEPTOR, 6);
        break;
    case CMD_OPTIONAL_FUNCTION:
        TRMIT_CMD(SSR_EN_DIS_DENOMI);
        break;
    case SSR_BOOT_VERSION_INFORMATION:
        TRMIT_CMD(SSR_BILL_TABLE);
        break;
    case SSR_BILL_TABLE:
        TRMIT_CMD2(CMD_EN_DIS_DENOMI, 7);
        break;
    case CMD_EN_DIS_DENOMI:
        TRMIT_CMD2(CMD_SECURITY_DENOMI, 7);
        break;
    case CMD_SECURITY_DENOMI:
        TRMIT_CMD2(CMD_DIRECTION, 6);
        break;
    case CMD_DIRECTION:
        TRMIT_CMD2(CMD_OPTIONAL_FUNCTION, 7);
        break;
    default:
        TRMIT_CMD(STATUS_REQUEST);
        break;
    }
}
