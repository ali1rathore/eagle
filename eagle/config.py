# -*- coding: utf-8 -*-
import os, sys, json, site, importlib.metadata
from pathlib import Path
import platform
def get_version():
    HERE = os.path.dirname(__file__)
    with open(os.path.join(HERE,'__version__.py')) as f:
        return f.read().strip()

def get_commit_id():
    """If the package was installed using `pip install git+http://...`, this will return
    the git commit short SHA of this package. Otherwise it returns None
    """
    version = importlib.metadata.version(__package__)
    id = ''
    for prefix in [Path(site.getusersitepackages())] + [Path(s) for s in site.getsitepackages()]:
        path = prefix / f"{__package__}-{version}.dist-info" / 'direct_url.json'
        if path.exists():
            try:
                commit_info = json.loads(path.read_text())
                if 'vcs_info' in commit_info:
                    if 'requested_revision' in commit_info['vcs_info']:
                        id = commit_info['vcs_info']['requested_revision']
                    elif 'commit_id' in commit_info['vcs_info']:
                        id = commit_info['vcs_info']['commit_id'][:8]
            except:
                pass

            if id:
                break
    return id

def get_platform():
    """
    Get platform info, it will return platform
    (Darwin, x86, arm). Raises ValueError exception
    in case of unsupported platform.
    """
    opsys = os.uname().sysname
    if opsys == 'Darwin':
        return opsys
    elif opsys == 'Linux' or opsys == 'Android':
        machine = os.uname().machine
        if machine == 'x86_64':
            return 'x86'
        else:
            return 'arm'
    else:
        raise ValueError('Unsupported platform')

def get_mac_address():
    try:
        import getmac
        if get_platform() == 'arm':
            m = getmac.get_mac_address(interface="eth0")
        else:
            m = getmac.get_mac_address()
        return m or ''
    except:
        pass
    try:
        with open('/sys/class/net/eth0/address','r') as f:
            return f.read().strip()
    except Exception as e:
        return ""

def get_host_id():
    return get_mac_address().replace(':','')

def get_my_ip():
    local_ip = None

    try:
        import socket
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        local_ip = s.getsockname()[0]
    except:
        pass

    if local_ip:
        return local_ip


    try:
        import socket
        hostname = socket.gethostname()
        local_ip = socket.gethostbyname(hostname)
    except Exception as e:
        pass

    return local_ip

def get_hostname():
    return os.environ.get('HOSTNAME',platform.node())

def load_dotenv():
    """
    Load environment variables from .env file
    Checks the "EAGLE_ENV" env var for the path of the .env file.
    Defaults to using '.env'
    Works only if the 'python-dotenv' pip package is installed.
    """
    try:
        import dotenv
    except:
        return None
    envfile =  os.path.abspath(os.environ.get('EAGLE_ENV','.env'))
    if not os.path.exists(envfile):
        envfile = None
    if envfile:
        dotenv.load_dotenv(envfile)
    return envfile

ENV_FILE = load_dotenv()
VERSION = get_version()
COMMIT_ID = get_commit_id()
MAC_ADDRESS = get_mac_address()
HOST_ID = os.environ.get('EAGLE_HOST_ID') or get_host_id() or 'UNKOWN'
APP_NAME = f"eagle"
GITLAB_URL = 'https://gitlab.com/lakeshorelabs/eagle.git'
GITLAB_PROJECT_ID = 31613588
IP_ADDRESS = get_my_ip()
DATABASE = f'{APP_NAME}-services.db'
DATABASE_DIR = os.environ.get('DATABASE_DIR')
SQLALCHEMY_TRACK_MODIFICATIONS = False
FAVICON ='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqEAe_8aCqhvEUcVD-uxZDWHNW7zDZwtTWKlk1_i3iDg&s'# 'data:,' # Disable favicon
os.environ['XDG_CONFIG_DIRS'] = '/etc:/usr/local/etc'
INSTANCE_PATH = os.environ.get('EAGLE_INSTANCE_PATH')
TEMPLATE_FOLDER = os.environ.get('EAGLE_TEMPLATE_FOLDER',Path(__file__).parent/"templates")
HOST = os.environ.get('EAGLE_HOST','0.0.0.0')
PORT = os.environ.get('EAGLE_PORT','5000')
#PUBLIC_PORT = os.environ.get("PUBLIC_PORT", "80")
ROLE = os.environ.get('EAGLE_ROLE','ONBOARDING')
HOSTNAME = get_hostname()
SENTRY_TAG_KEYS = ['ROLE']

PIP_INSTALL_LOGFILE='/tmp/eagle-update.log'

SECRET_KEY = os.environ.get('SECRET_KEY')
ACCESS_POINT_SSID=f'{APP_NAME}-{VERSION}-{HOST_ID}'
ACCESS_POINT_PWD='1234567890'
ACCESS_POINT_IP='10.10.10.10'

FETCH_EVERY_S=7
UI_EVERY_S=7
TELEMETRY_EVERY_S=60

WIFI_SSID=None
WIFI_PWD=None
PLATFORM=get_platform()
PRINTER_NAME=os.environ.get('PRINTER_NAME','pos_printer')

SERVICES_DEBUG=False # dump stderr of processes

### >>> FAB
FAB_BASE_TEMPLATE = "base.html"
THEME_LOGOUT_ENDPOINT = "AuthDBView.logout"
THEME_LOGIN_ENDPOINT  = "AuthDBView.login"
ENABLE_ADMINLTE3 = False
AVAILABLE_THEMES = ["cerulean.css","amelia.css","cosmo.css","cyborg.css","flatly.css","journal.css","readable.css","simplex.css","slate.css","spacelab.css","united.css","yeti.css"]
APP_THEME = "cosmo.css"

ENABLE_BOOST=False
ENABLE_KIOSK=True
KIOSK_URL='/kiosk'
TIME_FORMAT = "%I:%M:%S %p %m-%d-%Y"
"""Time format used in parts of the app"""

XINITRC_TEMPLATE = """#!/usr/bin/env sh
xset -dpms
xset s off
xset s noblank

chromium-browser http://{{config.HOST}}:{{config.PORT}}{{config.KIOSK_URL}} \\
  --window-position=0,0 \\
  --incognito \\
  --kiosk \\
  --fullscreen \\
  --noerrdialogs \\
  --disable-translate \\
  --no-first-run \\
  --fast \\
  --fast-start \\
  --disable-infobars \\
  --disable-features=TranslateUI \\
  --disk-cache-dir=/dev/null \\
  --overscroll-history-navigation=0

"""
SYSTEMD_LINGER_DIR='/var/lib/systemd/linger'
SYSTEMD_USER=os.environ.get('EAGLE_USER','eagle')
SYSTEMD_SERVICES = {'eagle'}
SERVICES_LIST = {
    'cat': 'cat',
    'eagle': f'{sys.executable} -m eagle.server',
    'kiosk': 'startx -- -nocursor',
    'xterm': f'{sys.executable} -m eagle.extensions.terminal --host "0.0.0.0" --port 5055',

    'eagle_uba_pulse':'{app.root_path}/bin/{PLATFORM}/uba_pulse "{DATABASE}" 0',
    'eagle_uba_pulse_t':'{app.root_path}/bin/{PLATFORM}/uba_pulse_t "{DATABASE}" 0',
    'eagle_sense': '{app.root_path}/bin/{PLATFORM}/uba_pulse "{DATABASE}" 1',
    'eagle_uba_test':'{app.root_path}/bin/{PLATFORM}/uba_pulse "{DATABASE}" 2',

    'eagle_pog_cashout':'{app.root_path}/bin/pog_print.py "{DATABASE}"',
    'eagle_pog_sense': '{app.root_path}/bin/{PLATFORM}/uba_pulse "{DATABASE}" 3',
    'eagle_pog_cashout_test':'{app.root_path}/bin/pog_print.py "{DATABASE}" -',

    'eagle_banilla_cashout':'{app.root_path}/bin/banilla_print.py "{DATABASE}"',
    'eagle_banilla_cashout_test':'{app.root_path}/bin/banilla_print.py "{DATABASE}" -',

    'eagle_lol1_cashout':'{app.root_path}/bin/{PLATFORM}/lol1_print "{DATABASE}" 0',
    'eagle_lol1_cashout_test':'{app.root_path}/bin/{PLATFORM}/lol1_print "{DATABASE}" 2',

    'eagle_lol_cashout':'{app.root_path}/bin/{PLATFORM}/lol_print "{DATABASE}" 0',
    'eagle_lol_sense': '{app.root_path}/bin/{PLATFORM}/uba_pulse "{DATABASE}" 3',
    'eagle_lol_cashout_test':'{app.root_path}/bin/{PLATFORM}/lol_print "{DATABASE}" 2',

    'eagle_fish_cashout':'{app.root_path}/bin/{PLATFORM}/fish_print "{DATABASE}" 0',
    'eagle_fish_cashout_test':'{app.root_path}/bin/{PLATFORM}/fish_print "{DATABASE}" 2',

    'eagle_dummy_checkin':'{app.root_path}/bin/{PLATFORM}/test_insert "{DATABASE}"',
}

class Worker:
    XTERM = 'xterm'

class POS(Worker):
    """The Point-of-Sale"""

class ONBOARDING(Worker):
    pass

class POG(Worker):
    CASHIN = 'eagle_pog_sense'
    CASHOUT = 'eagle_pog_cashout'

class POG_TEST(Worker):
    CASHIN = 'eagle_uba_test'
    CASHOUT = 'eagle_pog_cashout_test'

class BAN(Worker):
    DELAY = .10
    CASHIN = 'eagle_uba_pulse'
    CASHOUT = 'eagle_banilla_cashout'

class TBAN(Worker):
    DELAY = .10
    CASHIN = 'eagle_uba_pulse_t'
    CASHOUT = 'eagle_banilla_cashout'


class BAN_TEST(Worker):
    CASHIN = 'eagle_uba_test'
    CASHOUT = 'eagle_banilla_cashout_test'

class LOL1(Worker):
    CASHIN = 'eagle_sense'
    CASHOUT = 'eagle_lol1_cashout'
class LOL1_TEST(Worker):
    CASHIN = 'eagle_uba_test'
    CASHOUT = 'eagle_lol1_cashout_test'

class LOL(Worker):
    CASHIN = 'eagle_lol_sense'
    CASHOUT =  'eagle_lol_cashout'
class LOL_TEST(Worker):
    CASHIN = 'eagle_uba_test'
    CASHOUT = 'eagle_lol_cashout_test'

class FIRELINK(Worker):
    DELAY = .10
    CASHIN = 'eagle_uba_pulse'
    CASHOUT = 'eagle_fish_cashout'
class FIRELINK_TEST(Worker):
    DELAY = .10
    CASHIN = 'eagle_uba_test'
    CASHOUT  = 'eagle_fish_cashout_test'

class RFISH(Worker):
    DELAY = .01
    CASHOUT = 'eagle_fish_cashout'
    ENABLE_REMOTE_CREDITS=True

class TEST(Worker):
    CASHIN = 'eagle_dummy_checkin'
    CASHOUT = 'cat'

ENABLE_REMOTE_CREDITS=False

AVAILABLE_ROLES = [k for k,v in locals().items() if isinstance(v,type) and issubclass(v,Worker) and v != Worker]
"""Roles available for this device"""

import flask
def init_app(app:flask.Flask,config_file=None,**overrides):

    if config_file:
        config_file = Path(config_file)
        if not config_file.exists():
            raise EnvironmentError(f"Configuration file {config_file} not found")

    app.config.from_object(__name__)

    app.env = overrides.get('ENV',app.env)
    app.debug = overrides.get('DEBUG',app.debug)

    if os.environ.get('FLASK_TESTING',0) in ['True','true','1','yes','TRUE','YES']:
        app.testing = (app.env != 'production')

    if app.env == 'production' and not app.testing:
        from platformdirs import PlatformDirs
        appdirs = PlatformDirs(appname=app.config['APP_NAME'],version=app.config['VERSION'])
        app.instance_path = app.config.root_path = str(appdirs.user_config_path / app.config['HOST_ID'])

        database = appdirs.user_data_path / app.config['HOST_ID'] / app.config['DATABASE']
        app.config['DATABASE'] = str(database)
        
        if not app.config.get('DATABASE_DIR'):
            app.config['DATABASE_DIR'] = str(appdirs.user_data_path / app.config['HOST_ID'])
        # The provision file should not depend on the HOST_ID 
        # or instance path in production.
        app.config['EAGLE_CONFIG'] = appdirs.user_config_path / "globals.py"
        app.config['SYSTEMD_WORKDIR'] = appdirs.user_config_path


    # Load the global configuation
    if 'EAGLE_CONFIG' in overrides:
        global_file = overrides['EAGLE_CONFIG']
    else:
        global_file = os.environ.get('EAGLE_CONFIG',Path(app.instance_path) / 'globals.py')
    global_config = app.config.setdefault('EAGLE_CONFIG',global_file)
    if global_config and app.config.from_pyfile(global_config ,silent=True):
        app.logger.info(f"Loaded global config from {global_config}")

    # Load the instance configuration
    if app.config.from_pyfile('config.py',silent=True):
        app.logger.debug(f'Loaded instance config {Path(app.instance_path)/"config.py"}')

    # Load the passed-in file config
    if config_file and app.config.from_pyfile(config_file):
        app.logger.debug(f"Loaded config_file from {config_file}")
   
    ROLE = os.environ.get('EAGLE_ROLE')
    if ROLE:
        app.config['ROLE'] = ROLE
    # Load any overrides into this functions
    if overrides:
        for k,v in overrides.items():
            app.config[k.upper()] = v

    if app.config.get('DATABASE_DIR') == None:
        app.config['DATABASE_DIR'] = str(Path(app.instance_path).absolute())

    if not Path(app.config['DATABASE_DIR']).is_absolute():
        app.config['DATABASE_DIR'] = str(Path(app.instance_path) / app.config['DATABASE_DIR'])

    db_dir = Path(app.config['DATABASE_DIR'])
    app.config['USERS_DB'] =        db_dir / f'{app.name}-users.db'
    app.config['TRANSACTIONS_DB'] = db_dir / f'{app.name}-transactions.db'
    app.config['MACHINES_DB'] =     db_dir / f'{app.name}-machines.db'
    app.config['REPORTS_DB'] =      db_dir / f'{app.name}-reports.db'
    app.config['CSTORES_DB'] =      db_dir / f'{app.name}-cstores.db'

    if not Path(app.config['DATABASE']).is_absolute():
        app.config['DATABASE'] = str(db_dir / app.config['DATABASE'])

    if 'SQLALCHEMY_DATABASE_URI' not in app.config:
        app.config['SQLALCHEMY_DATABASE_URI'] =f'sqlite:///{app.config["USERS_DB"]}?check_same_thread=False'
        app.config['SQLALCHEMY_BINDS'] = {
            'transactions':     f'sqlite:///{app.config["TRANSACTIONS_DB"]}?check_same_thread=False',
            'reports':          f'sqlite:///{app.config["REPORTS_DB"]}?check_same_thread=False',
            'machines':         f'sqlite:///{app.config["MACHINES_DB"]}?check_same_thread=False',
            'cstores':         f'sqlite:///{app.config["CSTORES_DB"]}?check_same_thread=False'
        }

    if not app.testing:
        # Enable roles ending with '_TEST' only in test mode
        available_roles = app.config['AVAILABLE_ROLES']
        app.config['AVAILABLE_ROLES'] = [r for r in available_roles if not r.endswith('TEST')]

    role = app.config.setdefault('ROLE','ONBOARDING')
    try:
        app.config.from_object(f'{__name__}.{role}')
    except Exception as e:
        app.logger.debug(f'Unable to find roles object eagle.config.{role}: {e}')
        return False
    else:
        app.logger.debug(f'Configured for role {role}')
    
    create_app_dirs(app)
    check_secret_key(app)

def check_secret_key(app):
    if not app.secret_key:
        import secrets
        app.logger.debug(f'SECRET_KEY not set so generated a random one.')
        app.secret_key = secrets.token_urlsafe(16)

def create_app_dirs(app):
    """
    Create the configuration and data directories
    """
    app.config['INSTANCE_PATH'] = app.instance_path
    # Create config and data dirs if they dont exist
    for f in [Path(app.instance_path), Path(app.config['DATABASE']).parent,Path(app.config['DATABASE_DIR'])]:
        if not f.exists():
            f.mkdir(parents=True,exist_ok=True)


DEFAULT_USERS = ['admin:admin:Admin','tech:tech:Tech,Owner,Clerk','clerk:clerk:Clerk','owner:owner:Owner,Clerk','accountant:accountant:Accountant,Owner']

FAB_ROLES = {
    "Tech": [
        ["MachineGeneralView", "can_list"],
        ["MachineGeneralView", "can_show"],
        ["MachineGeneralView", "menu_access"],
        ["Machines", "menu_access"],
        [".*", "menu_access"],
        [".*", "can_show"],
        [".*", "can_list"],
    ],
    "Clerk": [
        ["AddCashView","can_this_form_post"],
        ["AddCashView","can_this_form_get"]
    ],

    "Owner": [
        ["ReportsView", "can_list"],
        ["ReportsView", "can_show"],
        ["ReportsView", "menu_access"],
        ["ReportsView", "view"],
        ["ReportsView", "print"],
        ["ReportsView", "can_display"],
        ["Reports", "menu_access"],
        ["Summary", "menu_access"],
        ["TransactionGeneralView", "can_list"],
        ["TransactionGeneralView", "can_show"],
        ["TransactionGeneralView", "menu_access"],
        ["TransactionGeneralView", "print"],
        ["TransactionGeneralView", "view"],
        ["TransactionGeneralView", "can_display"],
        ["Transactions", "menu_access"],
    ],
    "Accountant": [
        ["ReplicaView", "can_list"],
        ["ReplicaView", "can_set"],
        ["CStoresView","can_list"],
        ["CStoresView","can_show"],
        ["CStoresView","can_edit"],
        ["Stores", "menu_access"],
    ],
    "Public": [
        ["TransactionGeneralView", "can_show"],
        ["TransactionGeneralView", "print"],
        ["TransactionGeneralView", "view"],
        ["CashoutView", "can_list"],
        ["CashoutView", "can_show"],
        ["CashoutView", "menu_access"],
        ["CashoutView", "can_display"],
        ["Cashouts", "menu_access"],
        ["Home","menu_access"],
        ["Back","menu_access"],
        ["Kiosk","menu_access"],
    ],
}