import click, flask
from blinker import signal
install_requested = signal('install-requested')

def add_install_command(app):
    @app.cli.command('install',help=f"Install {app.name}")
    def install_app():
        f"""Install this application ({app.name})"""

        install_requested.send(flask.current_app._get_current_object())

def init_app(app):
    add_install_command(app)
