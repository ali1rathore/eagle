import flask
import click
from . import lib

bp = flask.Blueprint('wifi',__name__,static_folder='static',template_folder='templates')

@bp.route('/')
def main():
    current_network = lib.is_wifi_connected()
    if current_network:
        msg = f"Currently connected to '{current_network}' <br />Select new network if desired:"
    else:
        msg = "Welcome! Please select the WiFi network:"

    return flask.render_template('wifi/index.html', ssids=lib.getssid(), message=msg)

@bp.route('/connect', methods=['POST'])
def connect():
    ssid = flask.request.form['ssid']
    password = flask.request.form['password']

    flask.current_app.logger.debug(f"Setting SSID {ssid}, password {password}")

    lib.connect_wifi(ssid=ssid,pwd=password)

    msg = f"Connecting to network '{ssid}'..."

    return flask.render_template('wifi/connecting.html', ssids=lib.getssid(), message=msg, redirect_interval=5, redirect_url=flask.url_for('.connect'))


@bp.route('/connect', methods=['GET'])
def check_connection():

    current_network = lib.is_wifi_connected()
    if current_network:
        msg = f"Connected to '{current_network}'"
        sleep = 60
        redir_url = flask.url_for('.main')
    else:
        msg = f"Connecting to network..."
        sleep = 5
        redir_url = flask.url_for('.connect')

    return flask.render_template('wifi/connecting.html', ssids=lib.getssid(), message=msg, redirect_interval=sleep, redirect_url=redir_url)

bp.cli.help = 'Commands for configuring the device WiFi'

@bp.cli.command('create')
@click.option('--ssid', help=f'Name of the wifi access point')
@click.option('--pwd', help=f'The wifi password')
@click.option('--ip', help=f'Which IP to run this app on')
def create(ssid=None,pwd=None,ip=None):
    """
    Create a new wifi access point
    """
    ssid = ssid or flask.current_app.config.get('ACCESS_POINT_SSID')
    pwd = pwd or flask.current_app.config.get('ACCESS_POINT_PWD')
    ip = ip or flask.current_app.config.get('ACCESS_POINT_IP',lib.AP_IP)
    if not all([ssid,pwd,ip]):
        click.echo(f"Please provide ssid, pwd, and ip")
        return
    lib.create_wifi(ssid=ssid,pwd=pwd,ip=ip)


@bp.cli.command('disable')
def disable():
    """
    Disable the wifi access point
    """
    lib.disable_access_point()


@bp.cli.command('install')
def install():
    """
    Install wifi dependancies
    """
    lib.install_deps()

@bp.cli.command('enable')
def enable():
    """
    Enable the wifi access point
    """
    lib.enable_access_point()

@bp.cli.command('disconnect')
def disconnect():
    """
    Disconnect device from wifi
    """
    raise NotImplementedError

@bp.cli.command('connect')
@click.option('--ssid', help=f'Name of the wifi access point')
@click.option('--pwd', help=f'The wifi password')
@click.option('--force', help=f'Connect even if wifi was not found',default=False)
def connect(ssid=None,pwd=None,force=False):
    """
    Connect device to a wifi
    """
    ssids = lib.getssid()

    ssid = ssid or flask.current_app.config.get('WIFI_SSID')
    pwd = pwd or flask.current_app.config.get('WIFI_PWD')

    if ssid not in ssids:
        click.echo(f"WiFi '{ssid}' was not found.  Choose from {ssids} ")
        if not force:
            return 1

    lib.connect_wifi(ssid=ssid,pwd=pwd)


@bp.cli.command('list')
def list():
    """
    List available wifi
    """
    click.echo(lib.getssid())


@bp.cli.command('status')
def status():
    """
    Print status of connected wifi
    """
    current_wifi = lib.is_wifi_connected()
    ap_enabled = lib.is_accept_point_enabled()
    print(f"Current Wifi: {current_wifi}. Access Point: {ap_enabled}")
