import flask
from blinker import signal

on_start = signal('on-start')
on_stop = signal('on-stop')

def init_app(app):
    pass
