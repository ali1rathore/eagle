#!/usr/bin/env python3
import argparse
from flask import Blueprint, Flask, render_template, current_app

import pty
import os
import subprocess
import select
import termios
import struct
import fcntl
import shlex

__version__ = "0.5.0.0"

bp = Blueprint("terminal",__name__, template_folder="templates")

@bp.route("/")
def index():
    return render_template("xterm.html")

def init_app(app:Flask,socketio=None):

    if app.config.setdefault('XTERM_ENABLED',False) != True:
        return

    url_prefix = app.config.setdefault('XTERM_URL_PREFIX', "/xterm")
    app.config["fd"] = None
    app.config["cmd"] = ["bash"]
    app.config["child_pid"] = None
    if not socketio:
        try:
            from flask_socketio import SocketIO
        except Exception as e:
            return
        socketio = SocketIO()
        socketio.init_app(app)
    init_socketio(app)

    app.register_blueprint(bp,url_prefix=url_prefix)


def set_winsize(fd, row, col, xpix=0, ypix=0):
    current_app.logger.debug("setting window size with termios")
    winsize = struct.pack("HHHH", row, col, xpix, ypix)
    fcntl.ioctl(fd, termios.TIOCSWINSZ, winsize)


def read_and_forward_pty_output_blocking(app):
    socketio = app.extensions.get('socketio')
    if not socketio:
        return
    import selectors
    sel = selectors.DefaultSelector()
    fd = app.config['fd']
    sel.register(fd, selectors.EVENT_READ)
    finished = False
    max_read_bytes = 1024 * 20

    with app.app_context():

        while not finished:
            try:
                for key, _ in sel.select():
                    data = os.read(key.fileobj, max_read_bytes).decode()
                    if not data:
                        finished = True
                    else:
                        output = data
                        socketio.emit("pty-output", {"output": output}, namespace="/pty")

            except Exception as e:
                app.logger.error(f"Error collecting outputs for: {e}") if app else print(f"Error collecting outputs for {app}: {e}")
        
        app.config['fd'] = None
        app.config["child_pid"] = None

        socketio.emit("pty-output", {"output": "Exiting"}, namespace="/pty")
        
def read_and_forward_pty_output(app):
    socketio = app.extensions.get('socketio')
    if not socketio:
        return

    max_read_bytes = 1024 * 20
    with app.app_context():
        while True:
            socketio.sleep(0.01)
            if app.config["fd"]:
                timeout_sec = 0
                (data_ready, _, _) = select.select([app.config["fd"]], [], [], timeout_sec)
                if data_ready:
                    output = os.read(app.config["fd"], max_read_bytes).decode()
                    socketio.emit("pty-output", {"output": output}, namespace="/pty")


def init_socketio(app,socketio=None):
    socketio = socketio or app.extensions.get('socketio')
    if not socketio:
        return

    @socketio.on("pty-input", namespace="/pty")
    def pty_input(data):
        """write to the child pty. The pty sees this as if you are typing in a real
        terminal.
        """
        if current_app.config["fd"]:
            current_app.logger.debug("received input from browser: %s" % data["input"])
            try:
                os.write(current_app.config["fd"], data["input"].encode())
            except Exception as e:
                current_app.logger.debug(f'Error writing to {current_app.config["fd"]}: {e}')
                raise

    @socketio.on("resize", namespace="/pty")
    def resize(data):
        if current_app.config["fd"]:
            current_app.logger.debug(f"Resizing window to {data['rows']}x{data['cols']}")
            set_winsize(current_app.config["fd"], data["rows"], data["cols"])

    
    @socketio.on("connect", namespace="/pty")
    def connect():
        """new client connected"""
        current_app.logger.debug("new client connected")
        if current_app.config["child_pid"]:
            # already started child process, don't start another
            return

        # create child process attached to a pty we can read from and write to
        (child_pid, fd) = pty.fork()
        if child_pid == 0:
            # this is the child process fork.
            # anything printed here will show up in the pty, including the output
            # of this subprocess
            subprocess.run(current_app.config["cmd"])
        else:
            # this is the parent process fork.
            # store child fd and pid
            current_app.config["fd"] = fd
            current_app.config["child_pid"] = child_pid
            set_winsize(fd, 50, 50)
            cmd = " ".join(shlex.quote(c) for c in current_app.config["cmd"])
            # logging/print statements must go after this because... I have no idea why
            # but if they come before the background task never starts
            socketio.start_background_task(read_and_forward_pty_output_blocking,current_app._get_current_object())

            current_app.logger.info(f"child pid is {child_pid}")
            current_app.logger.info(
                f"starting background task with command `{cmd}` to continously read "
                "and forward pty output to client"
            )
            current_app.logger.info("task started")


def main():
    parser = argparse.ArgumentParser(
        description=(
            "A fully functional terminal in your browser. "
            "https://github.com/cs01/pyxterm.js"
        ),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument("-p", "--port", default=5000, help="port to run server on")
    parser.add_argument(
        "--host",
        default="127.0.0.1",
        help="host to run server on (use 0.0.0.0 to allow access from other hosts)",
    )
    parser.add_argument("--debug", action="store_true", help="debug the server")
    parser.add_argument("--version", action="store_true", help="print version and exit")
    parser.add_argument(
        "--command", default="bash", help="Command to run in the terminal"
    )
    parser.add_argument(
        "--cmd-args",
        default="",
        help="arguments to pass to command (i.e. --cmd-args='arg1 arg2 --flag')",
    )
    args = parser.parse_args()
    if args.version:
        print(__version__)
        exit(0)
    from eagle import app, server
    app = app.create_app(
        XTERM_ENABLED=True,
        PORT=args.port,
        HOST=args.host
        )
    app.config["cmd"] = [args.command] + shlex.split(args.cmd_args)

    server.check_secret_key(app)
    green = "\033[92m"
    end = "\033[0m"
    log_format = green + "pyxtermjs > " + end + "%(levelname)s (%(funcName)s:%(lineno)s) %(message)s"
    app.logger.info(f"serving on http://127.0.0.1:{args.port}")

    socketio = app.extensions.get('socketio')
    if socketio:
        os.environ["FLASK_RUN_FROM_CLI"] = str(0)
        try:
            socketio.run(app, debug=args.debug, port=args.port, host=args.host,allow_unsafe_werkzeug=True,use_reloader=False)
        except:
            socketio.run(app, debug=args.debug, port=args.port, host=args.host,use_reloader=False)




if __name__ == "__main__":
    main()