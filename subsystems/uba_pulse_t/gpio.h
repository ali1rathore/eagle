#ifdef __arm__
#include <wiringPi.h>

void setup_wipi(void)
{
	const int pulse_gpio = 29;
	wiringPiSetup();
	pinMode(pulse_gpio, OUTPUT);
}
void write_pulses(int count)
{
	const int pulse_gpio = 29;
	// This should be parameterized. It's hardcoded at the moment
	// because the hardware is not changing from machine to machine

	for (; count > 0; count--)
	{
		digitalWrite(pulse_gpio, HIGH);
		delay(50);
		digitalWrite(pulse_gpio, LOW);
		delay(50);
	}
}
#else

void setup_wipi(void)
{
}
void write_pulses(int count)
{
}
#endif