import requests
import time
class TBRpcClient:

    def __init__(self,username,password,device_id=None,host='https://thingsboard.cloud',port=443) -> None:
        self._token = None
        self._refresh_token = None
        self._login_url = '/api/auth/login'
        self._rpc_url = '/api/rpc/twoway'
        self._devices_url = '/api/tenant/devices'
        self._device_telemetry_url = '/api/plugins/telemetry/DEVICE/{device_id}/values/timeseries?keys={keys}&startTs={start_ts}&endTs={end_ts}'

        self._url = f'{host}:{port}'
        self._username=username
        self._password=password
        self._device_id=device_id

    def url_for(self,path):
        url = f'{self._url}{path}'
        return url

    def get_telemetry(self,keys,device_id=None,start=None,end=None):
        headers = {
            'accept': 'application/json',
            # Already added when you pass json= but not when you pass data=
            # 'Content-Type': 'application/json',
            'X-Authorization': f'Bearer {self.token}',
        }
        device_id = device_id or self._device_id
        if not device_id:
            raise RuntimeError(f"Missing device_id for get_telemetry")    

        query = dict(
            keys=keys,
            device_id=device_id,
            start_ts=start or 0,
            end_ts=end or int(time.time() * 1000)
        )
        url = self.url_for(self._device_telemetry_url.format(**query))
        response = requests.get(url, headers=headers)
        data = {'error':response.status_code}
        if response.ok:
            data = response.json()

        return data

    def connect(self,username=None,password=None):

        headers = {
            'accept': 'application/json',
            # Already added when you pass json= but not when you pass data=
            # 'Content-Type': 'application/json',
        }

        json_data = {
            'username': username or self._username,
            'password': password or self._password,
        }
        response = requests.post(self.url_for(self._login_url), headers=headers, json=json_data)
        data = response.json()
        token = data['token']
        self._refresh_token = data['refreshToken']
        return token
    
    def get_device_id(self,device_name):

        headers = {
            'accept': 'application/json',
            'X-Authorization': f'Bearer {self.token}',
        }

        params = {
            'deviceName': device_name,
        }

        response = requests.get(self.url_for(self._devices_url), params=params, headers=headers)
        if response.ok:
            return response.json()
        
        return None

    @property
    def token(self):
        if not self._token:
            self._token = self.connect()
        return self._token

    def send_rpc(self,method,params,device_id=None):
        headers = {
            'accept': 'application/json',
            # Already added when you pass json= but not when you pass data=
            # 'Content-Type': 'application/json',
            'X-Authorization': f'Bearer {self.token}',
        }

        json_data = {
            'method': method,
            'params': params,
            'persistent': False,
            'timeout': 5000,
        }
        data = {}
        device_id = device_id or self._device_id
        if not device_id:
            raise RuntimeError(f"Missing device_id for RPC")            
        url = self.url_for(self._rpc_url+'/'+device_id)
        response = requests.post(url, headers=headers, json=json_data)
        data = {'error':response.status_code}
        if response.ok:
            data = response.json()

        return data

class REPL:

    def __init__(self,client,device_id) -> None:
        self.client = client
        self.device_id = device_id

    def send_command(self,cmd):
        params = {'command':cmd}
        response = self.client.send_rpc(device_id=self.device_id,method='sendCommand',params=params)
        output = error = ''
        if response.get('ok'):
            response = self.client.send_rpc(device_id=self.device_id,method='getCommandStatus',params={})
        else:
            error = response


        if response.get('done'):
            data = response.get('data')
            for d in data:
                output += d.get('stdout','') if d.get('stdout') is not None else ''
                error += d.get('stderr','') if d.get('stderr') is not None else ''
                
        return output, error

    def start(self):
        response = self.client.send_rpc(device_id=self.device_id,method='getTermInfo',params={})
        print("<<< " + str(response))
        while True:
            output = error = None
            try:
                command_line = input('>>> ')
                if not command_line:
                    continue
                output,error = self.send_command(command_line)
            except Exception as e:
                error = f"[ERROR]: {e}"
            print(str(output))
            if error:
                print('!!! ' + str(error))

def main():
    from flask import Config
    from eagle.config import get_host_id
    import sys
    device_name = get_host_id()
    if len(sys.argv) > 1:
        device_name = sys.argv[1]

    config = Config('instance')
    if not config.from_envvar('EAGLE_CONFIG',silent=True):
        if not config.from_pyfile('globals.py',silent=True):
            print("EAGLE_CONFIG not set")

    client = TBRpcClient(username=config['TB_API_USERNAME'],password=config['TB_API_PASSWORD'],host=config['TB_API_URL'])
    device = client.get_device_id(device_name=device_name)
    if device:
        device_id = device['id']['id']
        repl = REPL(client,device_id=device_id)
        repl.start()

if __name__ == '__main__':
    main()