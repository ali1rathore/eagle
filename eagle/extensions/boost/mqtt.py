from paho.mqtt import client as mqtt 
from logging import Logger
import queue, json
class MQTTQueue:
    def __init__(self,queue_name,host='127.0.0.1',port=1883) -> None:
        self._queue_name = queue_name
        self._queue = None
        self._client = None
        self._host = host
        self._port = port
        self.logger = Logger(queue_name)
    
    @property
    def client(self):
        if not self._client:
            client = mqtt.Client()
            client.on_connect = self._on_connect
            client.on_message = self._on_message
            client.on_disconnect = self._on_socket_close
            client.on_socket_close = self._on_socket_close
            self._client = client
            self._client.connect(self._host, self._port, 60*10)
            self._shedual_task()
        return self._client

    # noinspection DuplicatedCode
    def _shedual_task(self):
        import flask
        self.client.loop_start()  # 保持连接

    def _on_socket_close(self, client, userdata, socket):
        self.logger.critical(f'{client, userdata, socket}')
        self._shedual_task()

    # noinspection PyPep8Naming
    def _on_disconnect(self, client, userdata, reasonCode, properties):
        self.logger.critical(f'{client, userdata, reasonCode, properties}')

    def _on_connect(self, client, userdata, flags, rc):
        self.logger.info(f'Connected MQTT client, {client, userdata, flags, rc}')

    # noinspection PyUnusedLocal
    def _on_message(self, client, userdata, msg):
        # print(msg.topic + " " + str(msg.payload))
        payload = json.loads(msg.payload)
        self.queue.put(payload)

    @property
    def queue(self):
        if self._queue == None:
            self._queue = queue.Queue()
        return self._queue

    def put(self,msg,**kwargs):
        payload = {'pqid':1,'data':msg}
        resp = self.client.publish(self._queue_name,payload=json.dumps(payload),qos=2,retain=False)
        return resp

    def get(self,*args,**kwargs):
        self.client.subscribe(self._queue_name, qos=2)  # on message 是异把消息丢到线程池，本身不可能失败。
        return self.queue.get()

    def ack(self,*args,**kwargs):
        pass

    def nack(self,*args,**kwargs):
        pass

    def ack_failed(self,*args,**kwargs):
        pass

    def task_done(self,*args,**kwargs):
        pass