from eagle.app import create_app

def test_different_instances(tmp_path):

    ban = create_app(ROLE='BAN',instance_path=tmp_path/'ban',TESTING=True)
    lol = create_app(ROLE='LOL',instance_path=tmp_path/'lol',TESTING=True)
    assert lol.instance_path != ban.instance_path

    assert lol.config['ROLE']=='LOL1'


def test_different_instances2(tmp_path):

    ban = create_app(ROLE='BAN',instance_path=tmp_path/'ban',TESTING=True)
    lol = create_app(ROLE='LOL',instance_path=tmp_path/'lol',TESTING=True)
    assert lol.instance_path != ban.instance_path
    assert lol.config['ROLE']=='LOL1'

