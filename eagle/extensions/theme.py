from flask_theme_adminlte3 import theme, menu

def init_app(app):
    theme.init_app(app)
    menu.init_app(app)
