
# This script enables the KIOSK mode

# Install all required x11 and misc packages
echo Installing required packages
sudo apt-get update
sudo apt-get install --no-install-recommends xserver-xorg-video-all \
  xserver-xorg-input-all xserver-xorg-core xinit x11-xserver-utils \
  chromium-browser unclutter -y

if [ $? -eq 0 ] 
then 
  echo "Successfully installed packages" 
else 
  echo "Could not install packages" >&2
  exit 255 
fi

