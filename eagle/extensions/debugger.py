

def init_app(app):
    if not app.config.get('SECRET_KEY'):
        app.logger.debug(f'Disabling debugger extension {__name__} since "SECRET_KEY"')
        return
    try:
        from flask_debugtoolbar import DebugToolbarExtension
        debug_toolbar = DebugToolbarExtension()
    except:
        app.logger.debug(f'Disabling debugger extension {__name__} since "flask_debugtoolbar" is missing')
        return

    enabled = app.config.setdefault('DEBUG_TB_ENABLED', app.env == 'development')
    if not enabled:
        return
    #app.config['DEBUG_TB_HOSTS'] = ['127.0.0.1','localhost','0.0.0.0']
    app.config['DEBUG_TB_PROFILER_ENABLED'] = False
    app.config['DEBUG_TB_TEMPLATE_EDITOR_ENABLED'] = True
    app.config['DEBUG_TB_INTERCEPT_REDIRECTS'] = False

    debug_toolbar.init_app(app)
