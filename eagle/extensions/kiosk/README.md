kiosk-mode
==================

This extension implements KIOSK mode. To enable this extension define ``ENABLE_KIOSK`` in the config. Currently it will only work correctly with ``framebuffer`` devices. The monitor needs to be attached to the device before booting for the externsion to work. This extension uses X11 and for now it will only work on Raspberry pi and other arm based devices that work with X11.

This extension creates the .xinitrc file and installs a ``systemd`` user services ``kiosk`` that can be used to start/stop kiosk. The extension also automaticaly start kiosk when ``eagle`` is started.

To stop kiosk:

````bash
systemd --user stop kiosk
````

The ``.xinitrc`` generator uses a ``jinja2`` template string ``XINITRC_TEMPLATE``, which is defined in ``config.py`` and can be customized for further needs. 

Features
==================

* Auto-detect monitor resolution
* Auto start google chrome fullscreen with flask URL
* Incorporates Vrtual Keyboard that can be popped up/down using ``/toggle_keyboard`` route. 


Limitations
==================

* Only framebuffer devices
* Only single framebuffer ``/dev/fbv0`` is detected.
* Requires ``/sys`` filesystem with user read permissions
* Virtual keyboard requires change in HTML templates.



\<TODO add instruction for using virtual keyboard\>
