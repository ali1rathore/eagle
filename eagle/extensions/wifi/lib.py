import subprocess
import logging
import time
import pathlib
import re
import os,shutil
logger = logging.Logger(__name__)

SCAN_RETRIES = 10
AP_IP='10.10.10.10'
AP_INTERFACE='uap0'

WPA_TEMPLATE = """country=US
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
network={{
    ssid="{ssid}"
    {pwd}
}}"""

DHCP_HOOK_TEMPLATE= """
if [ -z "$wpa_supplicant_conf" ]; then
	for x in \
		/etc/wpa_supplicant/wpa_supplicant-"$interface".conf \
		/etc/wpa_supplicant/wpa_supplicant.conf \
		/etc/wpa_supplicant-"$interface".conf \
		/etc/wpa_supplicant.conf \
	; do
		if [ -s "$x" ]; then
			wpa_supplicant_conf="$x"
			break
		fi
	done
fi
: ${wpa_supplicant_conf:=/etc/wpa_supplicant.conf}

if [ "$ifwireless" = "1" ] && \
    type wpa_supplicant >/dev/null 2>&1 && \
    type wpa_cli >/dev/null 2>&1
then
	if [ "$reason" = "IPV4LL" ]; then
		wpa_supplicant -B -iwlan0 -f/var/log/wpa_supplicant.log -c/etc/wpa_supplicant/wpa_supplicant.conf
	fi
fi
"""

HOSTAPD_TEMPLATE = """interface={name}
ssid={ssid}
hw_mode=g
channel=6
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase={pwd}
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
""" 

AP_TEMPLATE = """allow-hotplug {name}
auto {name}
iface {name} inet static
    address {ip}
    netmask {netmask}
"""

DNSMASQ_TEMPLATE = """interface=lo,{name}
no-dhcp-interface=lo,wlan0
dhcp-range={start},{end},24h
"""

WIRELESS_RULES_TEMPLATE = '''ACTION=="add", SUBSYSTEM=="ieee80211", KERNEL=="phy0", RUN+="/sbin/iw phy %k interface add {name} type __ap"'''

def getssid(scan_retries=SCAN_RETRIES):
    if not shutil.which('iw'):
        return []
    ssid_list = []

    for i in range(scan_retries):
        try:
            get_ssid_list = subprocess.check_output(('sudo','iw', 'dev', 'wlan0', 'scan', 'ap-force'))
            ssids = get_ssid_list.splitlines()
            break
        except FileNotFoundError as e:
            logger.error(f"Error while trying to get WiFi list: {e}.")
            ssids = ['-- wifi extension not supported on this platform ("iw" command is missing) --']
            break
        except subprocess.CalledProcessError as e:
            print(f"Error while trying to get WiFi list: {e}. Will retry (#{i})")
            time.sleep(1)

        if i == scan_retries - 1:
            ssids = ['-- not available; please refresh --']
    
    for s in ssids:
        try:
            s = s.strip().decode('utf-8')
        except AttributeError:
            logger.error("Exception while trying to decode SSID scan output; leaving as is")

        if s.startswith("SSID"):
            a = s.split(": ")
            try:
                ssid_list.append(a[1])
            except:
                pass

    ssid_list = sorted(list(set(ssid_list)))
    return ssid_list

def is_wifi_connected():
    try:
        result = subprocess.check_output(['iwconfig', 'wlan0'])
    except FileNotFoundError:
        return None
    except Exception as e:
        logging.warn(f"Error checking for wifi: {e}")

    matches = re.findall(r'\"(.+?)\"', result.split(b'\n')[0].decode('utf-8'))
    if len(matches) > 0:
        return matches[0]
    else:
        return None

def backup_and_overwrite(filename,content,extension='bck',replace_existing=False):
    """
    Copy `filename` to `filename.bck` if it doesn't exist or if `replace_existing` is True.
    """
    
    # Backup the file
    if pathlib.Path(filename).exists() and (replace_existing or not pathlib.Path(f"{filename}.{extension}").exists()):
        subprocess.Popen(f"sudo cp {filename} {filename}.{extension}".split()).wait()

    # Write the new file
    fname = os.path.basename(filename)
    with open(f'/tmp/{fname}', 'w') as f:
        f.write(content)

    subprocess.Popen(f"sudo cp /tmp/{fname} {filename}".split()).wait()


def set_wireless_rules(name=AP_INTERFACE):
    filename = '/etc/udev/rules.d/90-wireless.rules'
    content = WIRELESS_RULES_TEMPLATE.format(name=name)
    backup_and_overwrite(filename=filename,content=content)

def set_dhcp_hooks():
    filename = '/lib/dhcpcd/dhcpcd-hooks/10-wpa_supplicant'
    content = DHCP_HOOK_TEMPLATE
    backup_and_overwrite(filename=filename,content=content)

def connect_wifi(ssid,pwd):
    if not shutil.which('ifconfig') or not shutil.which('wpa_cli'):
        return False

    filename = '/etc/wpa_supplicant/wpa_supplicant.conf'
    if pwd:
        pwd = 'psk="' + pwd + '"'
    else:
        pwd = "key_mgmt=NONE"

    content = WPA_TEMPLATE.format(ssid=ssid, pwd=pwd)
    backup_and_overwrite(filename=filename,content=content)

    # restart wifi
    subprocess.Popen("sudo wpa_cli -i wlan0 reconfigure".split()).wait()
    subprocess.Popen("sudo ifconfig wlan0 down".split()).wait()
    subprocess.Popen("sudo ifconfig wlan0 up".split()).wait()


def install_deps():
    if not shutil.which('apt-get'):
        return False
        
    subprocess.Popen(f'sudo apt-get install -y dnsmasq hostapd'.split()).wait()

def stop_dnsmasq_hostapd():
    if not shutil.which('systemctl'):
        return False
    subprocess.Popen(f'sudo systemctl stop dnsmasq'.split()).wait()
    subprocess.Popen(f'sudo systemctl stop hostapd'.split()).wait()

def set_dnsmasq(ip:str=AP_IP,name=AP_INTERFACE):
    filename = '/etc/dnsmasq.conf'
    first,second,third,forth = ip.split('.')
    forth_plus_one = str(int(forth) + 1)
    ap_dhcp_start =  '.'.join([first,second,third,forth_plus_one])
    ap_dhcp_end = '.'.join([first,second,third,'200'])
    
    content = DNSMASQ_TEMPLATE.format(name=name,start=ap_dhcp_start,end=ap_dhcp_end)
    backup_and_overwrite(filename=filename,content=content)

def reload_dhcpc():
    if not shutil.which('systemctl'):
        return False
    subprocess.Popen(f'sudo systemctl daemon-reload'.split()).wait()
    subprocess.Popen(f'sudo systemctl restart dhcpcd'.split()).wait()

def create_interface(ip=AP_IP,netmask='255.255.255.0',name=AP_INTERFACE):
    filename = '/etc/network/interfaces.d/ap'
    content = AP_TEMPLATE.format(name=name,ip=ip,netmask=netmask)
    backup_and_overwrite(filename=filename,content=content)

def is_accept_point_enabled():
    if not shutil.which('systemctl'):
        return False
    process = subprocess.run('sudo systemctl status hostapd'.split(),capture_output=True)
    stdout = process.stdout.decode()
    logging.debug(f"Output of 'sudo systemctl status hostapd': {stdout}")
    if 'Active: active (running)' in stdout:
        return True
    return False

def create_access_point(ssid,pwd,name=AP_INTERFACE):
    filename = '/etc/hostapd/hostapd.conf'
    content = HOSTAPD_TEMPLATE.format(ssid=ssid, pwd=pwd,name=name)

    backup_and_overwrite(filename,content)

    process = subprocess.run('cat /etc/default/hostapd'.split(),capture_output=True)
    content, err = process.stdout, process.stderr
    if not err and content:
        content = content.decode()
        if 'DAEMON_CONF="/etc/hostapd/hostapd.conf"' not in content:
            content += '\nDAEMON_CONF="/etc/hostapd/hostapd.conf"'
        
            with open('/tmp/hostapd.new', 'w') as f:
                f.write(content)
            subprocess.Popen(f"sudo cp /tmp/hostapd.new /etc/default/hostapd".split()).wait()

def start_ap():

    if not shutil.which('systemctl'):
        return False
    subprocess.Popen(f"sudo systemctl unmask hostapd".split()).wait()
    subprocess.Popen(f"sudo systemctl enable hostapd".split()).wait()
    subprocess.Popen(f"sudo systemctl daemon-reload".split()).wait()
    subprocess.Popen(f"sudo systemctl start hostapd".split()).wait()
    subprocess.Popen(f"sudo systemctl start dnsmasq".split()).wait()

def disable_access_point():

    if not shutil.which('systemctl'):
        return False
    subprocess.Popen(f"sudo systemctl stop hostapd".split()).wait()
    subprocess.Popen(f"sudo systemctl disable hostapd".split()).wait()


def enable_access_point():
    if not shutil.which('systemctl'):
        return False
    subprocess.Popen(f"sudo systemctl enable hostapd".split()).wait()
    subprocess.Popen(f"sudo systemctl start hostapd".split()).wait()

def get_MAC():
    try:
        with open('/sys/class/net/eth0/address','r') as f:
            return f.read().strip()
    except Exception as e:
        return "Unknown"

def create_wifi(ssid,pwd,ip=AP_IP):
    if not shutil.which('systemctl'):
        return False
    stop_dnsmasq_hostapd()
    set_dnsmasq()
    set_wireless_rules()
    set_dhcp_hooks()
    create_interface(ip)
    create_access_point(ssid,pwd)
    start_ap()
    reload_dhcpc()

if __name__ == '__main__':
    is_accept_point_enabled()