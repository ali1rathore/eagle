import flask
from time import sleep
try:
    import RPi.GPIO as GPIO
except:
    GPIO = None

PULSE_PIN = 40

def send_pulses(amount: int, delay):

    denom = int(amount/100)
    if not GPIO:
        return 0
        
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(PULSE_PIN,GPIO.OUT)
    npulses = 0
    for ct in range(denom):
        GPIO.output(PULSE_PIN,1)
        sleep(delay)
        GPIO.output(PULSE_PIN,0)
        sleep(delay)
        npulses += 1
    return npulses


if __name__ == '__main__':
    from eagle import config
    import sys
    amount = sys.argv[1]
    if len(sys.argv) > 2:
        delay = sys.argv[2]
    else:
        delay = config.FIRELINK.DELAY
    pulses = send_pulses(int(amount),float(delay))
    print(f"Send {pulses} pulses with delay {delay}")