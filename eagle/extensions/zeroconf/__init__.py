from zeroconf import ServiceBrowser, Zeroconf, ServiceInfo, EventLoopBlocked
from blinker import Namespace
import flask
from time import sleep
import socket
from eagle.config import get_my_ip
zeroconf_events = Namespace()
service_added = zeroconf_events.signal('service-added')
service_removed = zeroconf_events.signal('service-removed')

TYPE = "_{app.name}._http._tcp.local."

def svs_type(app=None):
    if not app:
        app = flask.current_app
    return TYPE.format(app=app)

def get_ip_by_mac(mac):
    """use arp to get a machine's IP address using its MAC address

    Args:
        mac (str): the MAC address of the machine

    Returns:
        ip: str or None
    """
    from pathlib import Path
    ip = None
    try:
        rows = Path('/proc/net/arp').read_text().split('\n')[1:-1]
        cols = [r.split() for r in rows]
        ip_to_mac = {c[0]:c[3] for c in cols}
        ip = ip_to_mac.get(mac)
    except:
        pass
    return ip
    
class MyListener:

    def __init__(self,app:flask.Flask) -> None:
        self._app: flask.Flask = app

    def remove_service(self, zeroconf, type, name):
        info = zeroconf.get_service_info(type, name)
        with self._app.app_context():
            service_removed.send(self._app,type_=type,name=name)
        self._app.logger.debug("Service %s removed" % (name,))

    def update_service(self,zeroconf,type,name):
        self.add_service(zeroconf,type,name)

    def add_service(self, zeroconf, type, name):
        info = zeroconf.get_service_info(type, name)
        if info is None:
            return flask.Response('Service Unavailable', 503)
        addresses = []
        properties = {x.decode():y.decode() for x,y in info.properties.items()}
        mac = properties.get('mac')
        for addr in info.addresses:
            address = socket.inet_ntoa(addr)
            if address.startswith('127.0.0.') and mac:
                ip = get_ip_by_mac(mac)
                if ip:
                    address = ip
                    self._app.logger.debug(f'Found {mac} at {ip} using ARP instead of {address}')
            addresses.append(address)
        port = info.port
        service = dict(type_=type,name=name,port=port,addresses=addresses,properties=properties,server=info.server)

        with self._app.app_context():
            service_added.send(self._app,**service)
    
class FlaskZeroConf:
    def __init__(self) -> None:
        self._zeroconf = None #Zeroconf()
        self._listener = None
        self._browser = None

    def start_app(self,app):
        my_ip = get_my_ip()
        while not my_ip or my_ip.startswith('127.0.0'):
            app.logger.warn(f'Zeroconf start_app got bad ip={my_ip}.  Sleeping for 30s until real IP is found.')
            sleep(30)
            my_ip = get_my_ip()

        app.logger.debug(f'Zeroconf got actual ip={my_ip}.  Will start zeroxonf')
        self._zeroconf = Zeroconf()
        
    def start_discovery(self,app):
        self._listener = MyListener(app)
        self._browser = ServiceBrowser(self._zeroconf, svs_type(app=app), self._listener)

    def init_app(self,app):
        self._app = app
        app.extensions['zeroconf'] = self

    def stop(self):
        if self._browser:
            self._browser.cancel()
        if self._zeroconf:
            self._zeroconf.close()

bp = flask.Blueprint('zeroconf',__name__,template_folder='templates')

@bp.route('/discovery')
def discovery():
    zc = flask.current_app.extensions.get('zeroconf')
    browser = ServiceBrowser(zc._zerconf,svs_type(),zc._listener)
    import time
    time.sleep(4)
    browser.cancel()
    browser.join()
    return 'services found'


def start_app(app):
    """
    Register this app so that is listens for discovery requests
    """

    zeroconf = app.extensions.get('zeroconf')
    if not zeroconf:
        return

    zeroconf.start_app(app)

def start_discovery(app):

    zeroconf = app.extensions.get('zeroconf')
    if not zeroconf:
        return

    zeroconf.start_discovery(app)
def start_register(app,ip,properties=None):
    zeroconf = app.extensions.get('zeroconf')
    if not zeroconf:
        return

    my_ip = ip()
    while not my_ip or my_ip.startswith('127.0.0'):
        app.logger.warn(f'Zeroconf got bad ip={my_ip}.  Sleeping for 30s until real IP is found.')
        sleep(30)
        my_ip = ip()
    
    app.logger.debug(f'Zeroconf got actual ip={my_ip}.  Registering with this IP address')
    app.config['IP_ADDRESS'] = my_ip
    properties = properties or {}
    properties = properties(app) if callable(properties) else properties

    port = int(app.config['PORT'])
    server = app.config['HOST_ID']
    name = app.config['HOST_ID'] + '.' + svs_type(app=app)
    info = ServiceInfo(type_=svs_type(app=app),
                name=name,
                server=server,
                port=port, weight=0, priority=0,
                addresses=[my_ip],
                properties=properties)
    #app.logger.debug(f"Registering zeroconf service {info}")

    try:
        zeroconf._zeroconf.unregister_service(info)
        zeroconf._zeroconf.register_service(info)
    except EventLoopBlocked as e:
        app.logger.warn(f'Failed registering zeroconf service : {e}, trying again in 3 seconds')
        sleep(3)
        start_register(app,ip,properties=properties)
    except Exception as e:
        app.logger.warning(f'Failed registering zeroconf service : {e}')

def stop_app(app,*args,**kwargs):
    zeroconf = app.extensions.get('zeroconf')
    if not zeroconf:
        return 
    zeroconf.stop()

def init_app(app):
    if not app.config.get('EAGLE_ENABLE_ZEROCONF',app.testing or app.env=='production'):
        return

    zeroconf = FlaskZeroConf()
    zeroconf.init_app(app)
    app.register_blueprint(bp)